
// OnestoreCodeDlgDemo.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

#include "OneStoreCode/EventMgr/CEventMgr.h"

// COnestoreCodeDlgDemoApp:
// See OnestoreCodeDlgDemo.cpp for the implementation of this class
//

class COnestoreCodeDlgDemoApp : public CWinApp
{
public:
	COnestoreCodeDlgDemoApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
  int memberfun1(int);

	DECLARE_MESSAGE_MAP()

  CEventMgr<void(int)>  m_EventMgrVoid;
  CEventMgr<int()>  m_EventMgr0;
  CEventMgr<int(int)> m_EventMgr1;
  CEventMgr<int(int, char)> m_EventMgr2;
  CEventMgr<int(int, int, char)> m_EventMgr3;
  CEventMgr<int(int, int, char, int)> m_EventMgr4;
  CEventMgr<int(int, int, char, int, char)> m_EventMgr5;
  CEventMgr<int(int, int, char, int, char, int)> m_EventMgr6;
};

extern COnestoreCodeDlgDemoApp theApp;