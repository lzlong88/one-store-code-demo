// OnestoreClientDlg.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "OnestoreClientDlg.h"
#include "define.h"
#include "IPC/ProcMsgQueueMgr.h"
#include "IPC/ByteBuffer.h"
#include "local_onestore_protocol.h"
#include "cmdparser/CmdParser.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
CProcMsgQueueMgr g_ProcMsgQueueMgr;
HWND    g_hWnd = NULL;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void auth_token_CmdSlot(const CmdParserMgrPtr &pCmdParserMgr,
  const CmdLineNodePtr &pCmdLineNode);
void show_login_view_CmdSlot(const CmdParserMgrPtr &pCmdParserMgr,
  const CmdLineNodePtr &pCmdLineNode);
void show_product_detail_CmdSlot(const CmdParserMgrPtr &pCmdParserMgr,
  const CmdLineNodePtr &pCmdLineNode);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    DWORD dwPid = ::GetCurrentProcessId();
    DWORD dwSid = 0;
    TCHAR szMutexName[MAX_PATH] = { 0 };

    ::ProcessIdToSessionId(dwPid, &dwSid);
    ::swprintf_s(szMutexName, MAX_PATH, _T("%s_%u"), ONESTORECLINE_MUTEX_NAME, dwSid);
    HANDLE hMutex = ::CreateMutexW(NULL, FALSE, szMutexName);

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_ONESTORECLIENTDLG, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    LPWSTR lpszCmdLine = GetCommandLine();
    if (lpszCmdLine && std::wstring(lpszCmdLine).find(L"-newcmdparser") != std::wstring::npos) {
      CmdParserMgr CmdParser;
      CmdParser.AddParseSlotCmdNode(
        ArgsParserNode(L"-auth_token",
          FunctorSubscriber(&auth_token_CmdSlot)));

      CmdParser.AddParseSlotCmdNode(
        ArgsParserNode(
          L"-show_login_view",
          FunctorSubscriber(&show_login_view_CmdSlot)));

      CmdParser.AddParseSlotCmdNode(
        ArgsParserNode(
          L"-show_product_detail",
          FunctorSubscriber(&show_product_detail_CmdSlot)));
      CmdParser.DoParseCmdLine();
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_ONESTORECLIENTDLG));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ONESTORECLIENTDLG));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_ONESTORECLIENTDLG);
    wcex.lpszClassName  = CLIENT_CLASS_NAME/*szWindowClass*/;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(CLIENT_CLASS_NAME/*szWindowClass*/, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   g_hWnd = hWnd;
   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

LRESULT DealWindowPro(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  switch (uMsg)
  {
    case WNDMSG_ONESTORE_AUTHTOKEN:
    {
      ByteBuffer bytebuffer;
      bytebuffer << static_cast<uint32_t>(EmProtocolMsgType::auth_token);  // auth_token protocol
      bytebuffer << "KHDIE8790324SDFGEAECGFHJUPLOOIJIOHSHDLSOLJDUHISYEWOKD";
      bytebuffer << "18";
      g_ProcMsgQueueMgr.WriteByteBuffer2Host(msgqueuedefaultserverid, bytebuffer);  // 发送给模拟器
      break;
    }
    case WNDMSG_ONESTORE_SHOWLOGINVIEW:
    {
      //MarketWebDelegate::NotifyOneStoreToLogin();
      ByteBuffer bytebuffer;
      bytebuffer << static_cast<uint32_t>(EmProtocolMsgType::show_login_view);  // show_login_view protocol
      g_ProcMsgQueueMgr.WriteByteBuffer2Host(msgqueuedefaultserverid, bytebuffer);  // 发送给模拟器
      break;
    }
    case WNDMSG_ONESTORE_NOTIFYAUTH:
    {
      ByteBuffer bytebuffer;
      bytebuffer << static_cast<uint32_t>(EmProtocolMsgType::notify_auth);  // notify_auth protocol
      bytebuffer << (uint32_t)wParam;
      g_ProcMsgQueueMgr.WriteByteBuffer2Host(msgqueuedefaultserverid, bytebuffer);
      break;
    }    case WM_COPYDATA:    {      COPYDATASTRUCT *pCopyData = (COPYDATASTRUCT *)lParam;
      if (!pCopyData)
        break;
      if (pCopyData->dwData == WNDMSG_ONESTORE_SHOWPRODETAIL)
      {
        ByteBuffer bytebuffer;
        typedef struct {
          WCHAR cls_type[2];
          WCHAR cls_value[MAX_PATH];
        } *MsgPtr;        MsgPtr msgptr = static_cast<MsgPtr>(pCopyData->lpData);        std::wstring producttype = msgptr->cls_type;        std::wstring productvalue = msgptr->cls_value;  
        /// product_detail protocol
        bytebuffer << static_cast<uint32_t>(EmProtocolMsgType::product_detail);
        /// 发送给模拟器
        g_ProcMsgQueueMgr.WriteByteBuffer2Host(msgqueuedefaultserverid, bytebuffer);        break;
      }    }
  }
  return S_OK;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    DealWindowPro(hWnd, message, wParam, lParam);
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


// -auth_token cmd
void auth_token_CmdSlot(const CmdParserMgrPtr &pCmdParserMgr,
  const CmdLineNodePtr &pCmdLineNode) {
  PostMessage(g_hWnd, WNDMSG_ONESTORE_AUTHTOKEN, 0, 0);
}

// -show_login_view cmd
void show_login_view_CmdSlot(const CmdParserMgrPtr &pCmdParserMgr,
  const CmdLineNodePtr &pCmdLineNode) {
  PostMessage(g_hWnd, WNDMSG_ONESTORE_SHOWLOGINVIEW, 0, 0);
}

// -show_product_detail cmd
void show_product_detail_CmdSlot(
  const CmdParserMgrPtr &pCmdParserMgr, const CmdLineNodePtr &pCmdLineNode) {
  std::wstring strOST_Req_cls_type = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  std::wstring m_strOST_Req_cls_value = pCmdParserMgr->GetSecondInputParamStr(pCmdLineNode);

  struct
  {
    WCHAR cls_type[2];
    WCHAR cls_value[MAX_PATH];
  } msg = { { 0 },{ 0 } };
  memcpy(msg.cls_type, strOST_Req_cls_type.c_str(), 1 * sizeof(WCHAR));
  memcpy(msg.cls_value, m_strOST_Req_cls_value.c_str(), m_strOST_Req_cls_value.size() * sizeof(WCHAR));
  COPYDATASTRUCT copyData = { 0 };
  copyData.cbData = sizeof(msg);
  copyData.lpData = &msg;
  copyData.dwData = WNDMSG_ONESTORE_SHOWPRODETAIL;
  DWORD dwSendResult = 0;

  SendMessageTimeout((HWND)g_hWnd, WM_COPYDATA, NULL, (LPARAM)&copyData, SMTO_ABORTIFHUNG, 5000, &dwSendResult);
}