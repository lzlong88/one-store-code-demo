# **OnestoreCodeDlgDemo项目简介**

OnestoreCodeDlgDemo是安卓模拟器内置种子app（seedapp）与模拟器进行通信的一个模块，除了进程间通信模块部分代码借鉴了前公司代码，其它代码都是本人所编写，代码已经隐去关系到公司的所有具体业务的所有私有业务信息。代码仅供学习以及本人工作成果展示所用，未经授权请勿传播。





## **程序部分预览：**
![主界面截图](https://gitee.com/lzlong88/one-store-code-demo/raw/master/pic_res/previewpng.png)

![使用截图](https://gitee.com/lzlong88/one-store-code-demo/raw/master/pic_res/previewgif.gif)

## seedapp与模拟器、模拟器与市场的通信流程图

![流程图](https://gitee.com/lzlong88/one-store-code-demo/raw/master/pic_res/communicate.png)

## **该项目是从onestore引擎侧与onestore通信相关协议部分抽出的部分代码，主要类封装包含：**


1. local_onestore_protocol类实现协议类的封装。
2. CRegMgr/CregHelperT类实现类工厂管理类实现类对象的注册和创建。
3. CMapT和CVecT类作为父类的封装，间接实现父类对map和vector类的父类接口封装和实现。
4. StlLockHelper类实现对map/set/vector/list常用stl对象的加锁保护封装。
5. ProcMsgQueue实现IPC进程间通信共享内存消息队列的封装。
6. CmdParser类实现对cmd命令行的解析。
7. FunctionSubscriber类实现对函数调用的绑定。
8. EventMgr类是事件跟函数的封装。
9. MemLeak类实现对CRT堆对内存开辟和释放的监测，监控内存泄漏。

**工程代码结构如下**：（工程sln目前用vs2015编译）

![codepng](https://gitee.com/lzlong88/one-store-code-demo/raw/master/pic_res/code.png)


**开源协议**

需要遵守什么开源协议：

用于个人，团体，公司进行相应的交流和学习，如果你能从这个库中拿到你需要的代码，这也是OK的。

**关于**

笔者QQ：754059099[天涯行客] 英文名：zeronliao