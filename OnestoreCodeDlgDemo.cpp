
// OnestoreCodeDlgDemo.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "OnestoreCodeDlgDemo.h"
#include "OnestoreCodeDlgDemoDlg.h"

#include "OneStoreCode/CmdParser/CmdParser.h"
#include "OneStoreCode/memleak/MemLeak.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define TESTEVENTID 1065

// COnestoreCodeDlgDemoApp

BEGIN_MESSAGE_MAP(COnestoreCodeDlgDemoApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// COnestoreCodeDlgDemoApp construction

COnestoreCodeDlgDemoApp::COnestoreCodeDlgDemoApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only COnestoreCodeDlgDemoApp object

#ifdef _DEBUG
class globalApp {
public:
  globalApp() {
    CMemLeak::Instance().Init();
    ptheApp = new COnestoreCodeDlgDemoApp;
  }
  ~globalApp()
  {
    delete ptheApp;
    CMemLeak::Instance().Uninit();
  }
  COnestoreCodeDlgDemoApp* ptheApp;
}theapp;
#else
COnestoreCodeDlgDemoApp theApp;
#endif


void g_DoCmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode) 
{
  std::wstring strOST_Req_cls_type = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  std::wstring strOST_Req_cls_value = pCmdParserMgr->GetSecondInputParamStr(pCmdLineNode);
  if (!strOST_Req_cls_type.empty() && !strOST_Req_cls_value.empty()) {
    //ApiNotifyWindow::SendMessageToShowProductDetail(strOST_Req_cls_type, strOST_Req_cls_value);
  }
}

// -startpkg
void startpkg_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  do
  {
    std::wstring strStartPkgName = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);  // apk name
    if (strStartPkgName.empty())
      break;

    if (pCmdParserMgr->GetCmdLineNode(L"-Customization"))
    {
      //ApiNotifyWindow::SendMessageToStartCustomizationApk(strStartPkgName);
      break;
    }
    if (pCmdParserMgr->GetCmdLineNode(L"-pcgame"))
    {
      //pc_game::GameMan19::Instance()->LaunchGame(minilib::Utils::toUtf8(strStartPkgName.c_str()), "");
      break;
    }
    //ApiNotifyWindow::SendMessageToStartApk(strStartPkgName);
  } while (FALSE);
}

// -pkg
void pkg_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strInstallPkgName = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  std::wstring strCmdInstallPkgSupplyId = pCmdParserMgr->GetFirstInputParamStr(L"-apksupplyid");
  DWORD dwCmdInstallPkgSupplyId = strCmdInstallPkgSupplyId.empty() ? 0 : _wtoi(strCmdInstallPkgSupplyId.c_str());
  std::wstring strCmdInstallPkgArea = pCmdParserMgr->GetFirstInputParamStr(L"-apkarea");
  std::wstring strCmdInstallPkgPayId = pCmdParserMgr->GetFirstInputParamStr(L"-apkpayid");
  std::wstring strDisplayName = pCmdParserMgr->GetFirstInputParamStr(L"-displayname");
  BOOL bCreateDesktopIcon = pCmdParserMgr->GetCmdLineNode(L"-createdesktopicon") ? TRUE : FALSE;
  if (!strInstallPkgName.empty()) {
    //ApiNotifyWindow::SendMessageToInstallApk(strInstallPkgName, dwCmdInstallPkgSupplyId, strCmdInstallPkgArea, strCmdInstallPkgPayId, strDisplayName, bCreateDesktopIcon);
  }
 }

// -jumpgift
void jumpgift_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strJumpGiftPkgName = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  // 打开指定礼包页面
  if (!strJumpGiftPkgName.empty())
  {
    //ApiNotifyWindow::SendMessageToJumpGift(strJumpGiftPkgName);
  }
}

// -jumpvipgift
void jumpvipgift_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strJumpVipGiftPkgName = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  std::wstring strJumpVipGiftType = pCmdParserMgr->GetSecondInputParamStr(pCmdLineNode);
  //ApiNotifyWindow::SendMessageToJumpVipGift(strJumpVipGiftPkgName, strJumpVipGiftType);
}

//L"-syncapk"
void syncapk_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  //ApiNotifyWindow::SendMessageToRefreshMyGame();
}

//L"-msgwnd"
void msgwnd_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strHMsgWnd = pCmdParserMgr->GetFirstInputParamStr(L"-msgwnd");
  if (!strHMsgWnd.empty()) {
    HWND hMsgWnd = (HWND)(DWORD_PTR)_wtoi(strHMsgWnd.c_str());
    //ApiNotifyWindow::SendMessageToUpdateMsgWnd(hMsgWnd);
  }
}

// -uninstall
void uninstall_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strUnInstallApk = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  if (!strUnInstallApk.empty()) {
    //ApiNotifyWindow::SendMessageToUnInstallApk(strUnInstallApk);
  }
}

// "-noui"
void noui_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  //ApiNotifyWindow::SendMessageToNotifyNoUi(true);
}

// -launchtray
void launchtray_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  //ApiNotifyWindow::SendMessageToNotifyNoUi(true);
}

// -from
void from_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strFromsourceApk = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  if (!strFromsourceApk.empty()) {
    //CAppManager::Instance()->SetCurLaunchSource(CAtlString(strFromsourceApk.c_str()));
  }
}

// -gamequit
void gamequit_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  //ApiNotifyWindow::SendMessageToNotifyGameQuit();
}

// -external_request_logon
void external_request_logon_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strProcessId = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  std::wstring strAction = pCmdParserMgr->GetSecondInputParamStr(pCmdLineNode);

  //校验参数必须是十进制数字
  /*  if (isDecimalNum(strProcessId.c_str()) && isDecimalNum(strAction.c_str())) {
  DWORD dwProcessId = (DWORD)_tstoi(strProcessId.c_str());
  int action = (int)_tstoi(strAction.c_str());
  ApiNotifyWindow::SendMessageToRequestLogon(dwProcessId, action);
  } */
}

// -launchvoice
void launchvoice_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  /*CommandWrapper command;

  if ((i + 3) < nCnt)
  {
  CCommandProv::ParseCommand(lpszCmdLine, command);
  }

  //Android emulator通知appmarket拉起TVoice
  ApiNotifyWindow::PostMessageToLaunchTVoice(command.strCammand.GetString(), command.strParam.GetString());
  */
}

// -url
void url_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strOpenUrl = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  std::wstring strOpenType = pCmdParserMgr->GetFirstInputParamStr(L"-opentype");
  if (!strOpenType.empty() && 0 == _wtoi(strOpenType.c_str())) {  // nOpenType == 0
    int nWidth = _wtoi(pCmdParserMgr->GetFirstInputParamStr(L"-browserwidth").c_str());
    int nHeight = _wtoi(pCmdParserMgr->GetFirstInputParamStr(L"-browserheight").c_str());
    //ApiNotifyWindow::SendMessageToPopUrl(strOpenUrl, nWidth, nHeight);
  }
}

// -feedback
void feedback_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  //ApiNotifyWindow::PostMessageToLaunchFeedback();
}

// -openpkurl
void openpkurl_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  // ApiNotifyWindow::PostMessageToLaunchPKUrl();
}

// -auth_token
void auth_token_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  // ApiNotifyWindow::PostMessageToSendAuthToken();
}

// -show_login_view
void show_login_view_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  //ApiNotifyWindow::PostMessageToShowLoginView();
}

// -show_product_detail
void show_product_detail_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strOST_Req_cls_type = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  std::wstring strOST_Req_cls_value = pCmdParserMgr->GetSecondInputParamStr(pCmdLineNode);
  if (!strOST_Req_cls_type.empty() && !strOST_Req_cls_value.empty()) {
    //ApiNotifyWindow::SendMessageToShowProductDetail(strOST_Req_cls_type, strOST_Req_cls_value);
  }
}

// -settingcenter
void settingcenter_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  //SettingCenterTab settingCenterTab = SettingCenterTab::Basic;
  //settingCenterTab = (SettingCenterTab)_wtoi(pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode).c_str());
  //ApiNotifyWindow::PostMessageToLaunchSettingCenter(settingCenterTab);
}

// -creategamedesktopicon
void creategamedesktopicon_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strIconPkgName = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  std::wstring strDisplayName = pCmdParserMgr->GetSecondInputParamStr(pCmdLineNode);
  if (!strIconPkgName.empty() && !strDisplayName.empty())
  {
    //CAppManager::Instance()->CreateSelfShortCutToDesktop(strDisplayName.c_str(), iconPkgName.c_str(), strDesktopUrl.c_str());
  }
}

// -loginplatformkey
void loginplatformkey_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strplatformKey = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  if (!strplatformKey.empty()) {
    //ApiNotifyWindow::SendMessageToPlatformKey(strplatformKey);
  }
}

// -launchvoicemini
void launchvoicemini_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring asideWindow = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  //ApiNotifyWindow::SendVoiceMiniUICmd((HWND)std::stoul(asideWindow));
}

// -localpkg
void localpkg_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const CmdLineNodePtr& pCmdLineNode)
{
  std::wstring strCmdLocalPkgPath = pCmdParserMgr->GetFirstInputParamStr(pCmdLineNode);
  if (!strCmdLocalPkgPath.empty()) {
    //ApiNotifyWindow::SendMessageToInstallLocalApk(strCmdLocalPkgPath);
  }
}

LRESULT MsgHandleSlot(HWND hWnd, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
  return DefWindowProc(hWnd, uiMsg, wParam, lParam);
}

void voidfun(int ii)
{
  int i = ii;
}

int fun0()
{
  int jj = 0;
  int ii = jj;
  return 9;
}

int fun1(int param)
{
  int jj = param;
  return jj;
}

int COnestoreCodeDlgDemoApp::memberfun1(int i)
{
  return i;
}

int fun2(int i, char c)
{
  int ii = i;
  return ii;
}

int fun3(int i, int j, char c)
{
  int ii = i;
  return ii;
}

int fun4(int i, int j, char c, int r)
{
  int ii = i;
  return ii;
}

int fun5(int i, int j, char c, int r, char cc)
{
  int ii = i;
  return ii;
}

int fun6(int i, int j, char c, int r, char cc, int jj)
{
  int ii = i;
  return ii;
}

void constFunc(const int& i, LPCTSTR p, char& c)
{
  int ii = i;
  c = 'q';
}

BOOL COnestoreCodeDlgDemoApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

  char* pTestMemLeak = new char[108];

//  m_winMsgHandlerMgr.RegisterMsg(TESTEVENTID, FunctorSubscriber(&MsgHandleSlot).Clone());
  //m_winMsgHandlerMgr.UnRegisterMsg(TESTEVENTID, FunctorSubscriber(&MsgHandleSlot).Clone());
//  m_winMsgHandlerMgr.ProcessWinMsg(0, TESTEVENTID, 0, 0);

 // CRetTool<void> ret = CRetTool<void>();//voidfun(88)
//  bool b = CRetTool<voidfun>();
//  m_EventMgrVoid.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&voidfun));
//  m_EventMgrVoid.FireEvent(TESTEVENTID, 75);

  {
    CEventMgr<int()>* pEventMgrCopy;
    CEventMgr<void(const int&, LPCTSTR, char&)>* pEventMgrCopy2;
    {
      CEventMgr<int()> EventMgr0;
      EventMgr0.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun0));
      pEventMgrCopy = EVMRTTI_CAST<CEventMgr<int()>*>(EventMgr0.Clone());

      CEventMgr<void(const int&, LPCTSTR, char&)> EventMgr3;
      EventMgr3.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&constFunc));
      pEventMgrCopy2 = EVMRTTI_CAST<CEventMgr<void(const int&, LPCTSTR, char&)>*>(EventMgr3.Clone());
    }
    pEventMgrCopy->FireEvent(TESTEVENTID);
    char cc;
    pEventMgrCopy2->FireEvent(TESTEVENTID, 23, L"TestStr", cc);
    delete pEventMgrCopy;
    delete pEventMgrCopy2;
  }

  m_EventMgr0.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun0));
  m_EventMgr0.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun0));// twice subc but no use
  //m_EventMgr0.UnSubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun0));
  m_EventMgr0.FireEvent(TESTEVENTID);

  m_EventMgr1.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun1));
  //m_EventMgr1.UnSubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun1));
  m_EventMgr1.FireEvent(TESTEVENTID, 45);

  m_EventMgr1.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&COnestoreCodeDlgDemoApp::memberfun1, this));
  //m_EventMgr1.UnSubscribeEvent(TESTEVENTID, FunctorSubscriber(&COnestoreCodeDlgDemoApp::memberfun1, this));
  m_EventMgr1.FireEvent(TESTEVENTID, 37);

  m_EventMgr2.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun2));
  //m_EventMgr2.UnSubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun2));
  m_EventMgr2.FireEvent(TESTEVENTID, 65, 'e');

  m_EventMgr3.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun3));
  //m_EventMgr3.UnSubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun3));
  m_EventMgr3.FireEvent(TESTEVENTID, 15, 32, 'h');

  m_EventMgr4.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun4));
  //m_EventMgr4.UnSubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun4));
  m_EventMgr4.FireEvent(TESTEVENTID, 17, 22, 'd', 98);

  m_EventMgr5.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun5));
  //m_EventMgr5.UnSubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun5));
  m_EventMgr5.FireEvent(TESTEVENTID, 15, 32, 'w', 23, 58);

  m_EventMgr6.SubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun6));
  //m_EventMgr6.UnSubscribeEvent(TESTEVENTID, FunctorSubscriber(&fun6));
  m_EventMgr6.FireEvent(TESTEVENTID, 55, 31, 'f', 32, 'd', 94);

  LPWSTR lpszCmdLine = GetCommandLine();

  CmdParserMgr CmdParser;
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-startpkg", FunctorSubscriber(&startpkg_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-pkg", FunctorSubscriber(&pkg_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-jumpgift", FunctorSubscriber(&jumpgift_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-jumpvipgift", FunctorSubscriber(&jumpvipgift_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-syncapk", FunctorSubscriber(&syncapk_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-msgwnd", FunctorSubscriber(&msgwnd_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-uninstall", FunctorSubscriber(&uninstall_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-noui", FunctorSubscriber(&noui_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-launchtray", FunctorSubscriber(&launchtray_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-from", FunctorSubscriber(&from_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-gamequit", FunctorSubscriber(&gamequit_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-external_request_logon", FunctorSubscriber(&external_request_logon_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-launchvoice", FunctorSubscriber(&launchvoice_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-url", FunctorSubscriber(&url_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-feedback", FunctorSubscriber(&feedback_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-openpkurl", FunctorSubscriber(&openpkurl_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-auth_token", FunctorSubscriber(&auth_token_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-show_login_view", FunctorSubscriber(&show_login_view_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-show_product_detail", FunctorSubscriber(&show_product_detail_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-settingcenter", FunctorSubscriber(&settingcenter_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-creategamedesktopicon", FunctorSubscriber(&creategamedesktopicon_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-loginplatformkey", FunctorSubscriber(&loginplatformkey_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-launchvoicemini", FunctorSubscriber(&launchvoicemini_cmdAction)));
  CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-localpkg", FunctorSubscriber(&localpkg_cmdAction)));
  CmdParser.DoParseCmdLine();

  if (!CmdParser.GetCmdLineNode(L"-launchservice")) {
    bool bIsNoUi = CmdParser.GetCmdLineNode(L"-noui") || CmdParser.GetCmdLineNode(L"-launchtray");
    //ApiNotifyWindow::PostMessageToRunAsNormal(!bIsNoUi);
  }

	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Activate "Windows Native" visual manager for enabling themes in MFC controls
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	COnestoreCodeDlgDemoDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
		TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
	}

	// Delete the shell manager created above.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

#ifndef _AFXDLL
	ControlBarCleanUp();
#endif

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

