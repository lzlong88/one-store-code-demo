
// OnestoreCodeDlgDemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "OnestoreCodeDlgDemo.h"
#include "OnestoreCodeDlgDemoDlg.h"
#include "afxdialogex.h"

#include "OneStoreCode/FunctionSubscriber.h"
#include "OneStoreCode/local_onestore_protocol.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef _DEBUG
#define  _USEMEMLEAK
#endif

#ifdef _USEMEMLEAK_
#include "OneStoreCode/memleak/MemLeak.h"
class  MemLeakCollect
{
public:
  MemLeakCollect() {
    CMemLeak::Instance().Init();
  }
  ~MemLeakCollect() {
    CMemLeak::Instance().Uninit();
  }
private:
}memleak;
#endif // !_USEMEMLEAK


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// COnestoreCodeDlgDemoDlg dialog



COnestoreCodeDlgDemoDlg::COnestoreCodeDlgDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_ONESTORECODEDLGDEMO_DIALOG, pParent), m_onestore(this)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void COnestoreCodeDlgDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(COnestoreCodeDlgDemoDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()

  ON_BN_CLICKED(IDC_BUTTON1, &COnestoreCodeDlgDemoDlg::OnBnClickedButton1)
  ON_BN_CLICKED(IDC_BUTTON2, &COnestoreCodeDlgDemoDlg::OnBnClickedButton2)
  ON_BN_CLICKED(IDC_BUTTON3, &COnestoreCodeDlgDemoDlg::OnBnClickedButton3)
  ON_BN_CLICKED(IDC_BUTTON4, &COnestoreCodeDlgDemoDlg::OnBnClickedButton4)
  ON_BN_CLICKED(IDC_BUTTON5, &COnestoreCodeDlgDemoDlg::OnBnClickedButton5)
  ON_BN_CLICKED(IDC_BUTTON6, &COnestoreCodeDlgDemoDlg::OnBnClickedButton6)
  ON_BN_CLICKED(IDC_BUTTON7, &COnestoreCodeDlgDemoDlg::OnBnClickedButton7)
  ON_BN_CLICKED(IDC_BUTTON8, &COnestoreCodeDlgDemoDlg::OnBnClickedButton8)
  ON_BN_CLICKED(IDC_BUTTON9, &COnestoreCodeDlgDemoDlg::OnBnClickedButton9)
END_MESSAGE_MAP()


// COnestoreCodeDlgDemoDlg message handlers

int normfun0()
{
  int ii = 0;
  return ii;
}

int normfun1(int i)
{
  int ii = i;
  return ii;
}

int normfun2(int i, double db)
{
  int ii = i;
  double d = db;
  return ii;
}

int normfun3(int i, double db, char f)
{
  int ii = i;
  double d = db;
  char ff = f;
  return ii;
}

int normfun4(int i, double db, char f, int ij)
{
  int ii = i;
  double d = db;
  char ff = f;
  return ii;
}

int normfun5(int i, double db, char f, int ij, double di)
{
  int ii = i;
  double d = db;
  char ff = f;
  return ii;
}

int normfun6(int i, double db, char f, int ij, double di, int ik)
{
  int ii = i;
  double d = db;
  char ff = f;
  return ii;
}

int COnestoreCodeDlgDemoDlg::memberfun0()
{
  int ii = 0;
  return ii;
}

int COnestoreCodeDlgDemoDlg::memberfun1(int i)
{
  int ii = i;
  return ii;
}

int COnestoreCodeDlgDemoDlg::memberfun2(int i, double db)
{
  int ii = i;
  double d = db;
  return ii;
}

int COnestoreCodeDlgDemoDlg::memberfun3(int i, double db, char f)
{
  int ii = i;
  double d = db;
  char ff = f;
  return ii;
}

int COnestoreCodeDlgDemoDlg::memberfun4(int i, double db, char f, int ij)
{
  int ii = i;
  double d = db;
  char ff = f;
  return ii;
}

int COnestoreCodeDlgDemoDlg::memberfun5(int i, double db, char f, int ij, double di)
{
  int ii = i;
  double d = db;
  char ff = f;
  return ii;
}

int COnestoreCodeDlgDemoDlg::memberfun6(int i, double db, char f, int ij, double di, int ik)
{
  int ii = i;
  double d = db;
  char ff = f;
  return ii;
}

void voidNorFunc(int j)
{
  int ii = j;
  void(0);
  return void(0);
}

int constFunc(const int& i)
{
  int jj = i;
  return jj;
}

BOOL COnestoreCodeDlgDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

  WCHAR szPath[MAX_PATH] = { 0 };
  GetModuleFileNameW(GetModuleHandle(NULL), szPath, MAX_PATH);
  PathRemoveFileSpec(szPath);
  wcscat_s(szPath, L"\\OnestoreMFCClientDlg.exe");

  ((CEdit*)GetDlgItem(IDC_EDIT2))->SetWindowText(szPath);
  ((CEdit*)GetDlgItem(IDC_EDIT1))->SetWindowText(L"com.android.launcherex");
  
  FunctorDelegateBase<void(int)>& VoidNorFuncSlot = FunctorSubscriber(&voidNorFunc);
  VoidNorFuncSlot(34);
  FunctorDelegateBase<int(const int&)>& constNorFuncSlot = FunctorSubscriber(&constFunc);
  constNorFuncSlot(54);

  FunctorDelegateBase<int()>* pNorFunc0 = FunctorSubscriber(&normfun0).Clone();
  FunctorDelegateBase<int(int)>* pNorFunc1 = FunctorSubscriber(&normfun1).Clone();
  FunctorDelegateBase<int(int, double)>* pNorFunc2 = FunctorSubscriber(&normfun2).Clone();
  FunctorDelegateBase<int(int, double, char)>* pNorFunc3 = FunctorSubscriber(&normfun3).Clone();
  FunctorDelegateBase<int(int, double, char, int)>* pNorFunc4 = FunctorSubscriber(&normfun4).Clone();
  FunctorDelegateBase<int(int, double, char, int, double)>* pNorFunc5 = FunctorSubscriber(&normfun5).Clone();
  FunctorDelegateBase<int(int, double, char, int, double, int)>* pNorFunc6 = FunctorSubscriber(&normfun6).Clone();
  FunctorDelegateBase<int(int)>& NorFunc11 = FunctorSubscriber(&normfun1);

  pNorFunc0->operator()();
  pNorFunc1->operator()(5);
  NorFunc11(34);
  (*pNorFunc2)(4, 6.3);
  (*pNorFunc3)(7, 9.3, 'd');
  (*pNorFunc4)(6, 1.9, 'j', 2);
  (*pNorFunc5)(8, 4.4, 'w', 42, 8.5);
  (*pNorFunc6)(1, 8.3, 's', 121, 5.6, 54);

  delete pNorFunc0;
  delete pNorFunc1;
  delete pNorFunc2;
  delete pNorFunc3;
  delete pNorFunc4;
  delete pNorFunc5;
  delete pNorFunc6;


  FunctorDelegateBase<int()>* pMemberFunc0 = FunctorSubscriber(&COnestoreCodeDlgDemoDlg::memberfun0, this).Clone();
  FunctorDelegateBase<int(int)>* pMemberFunc1 = FunctorSubscriber(&COnestoreCodeDlgDemoDlg::memberfun1, this).Clone();
  FunctorDelegateBase<int(int, double)>* pMemberFunc2 = FunctorSubscriber(&COnestoreCodeDlgDemoDlg::memberfun2, this).Clone();
  FunctorDelegateBase<int(int, double, char)>* pMemberFunc3 = FunctorSubscriber(&COnestoreCodeDlgDemoDlg::memberfun3, this).Clone();
  FunctorDelegateBase<int(int, double, char, int)>* pMemberFunc4 = FunctorSubscriber(&COnestoreCodeDlgDemoDlg::memberfun4, this).Clone();
  FunctorDelegateBase<int(int, double, char, int, double di)>* pMemberFunc5 = FunctorSubscriber(&COnestoreCodeDlgDemoDlg::memberfun5, this).Clone();
  FunctorDelegateBase<int(int, double, char, int, double di, int ik)>* pMemberFunc6 = FunctorSubscriber(&COnestoreCodeDlgDemoDlg::memberfun6, this).Clone();
  FunctorDelegateBase<int(int)>& pMemberFunc11 = FunctorSubscriber(&COnestoreCodeDlgDemoDlg::memberfun1, this);

  pMemberFunc0->operator()();
  pMemberFunc1->operator()(5);
  pMemberFunc11(32);
  (*pMemberFunc2)(4, 6.3);
  (*pMemberFunc3)(7, 9.3, 'd');
  (*pMemberFunc4)(6, 1.9, 'j', 2);
  (*pMemberFunc5)(8, 4.4, 'w', 42, 8.5);
  (*pMemberFunc6)(1, 8.3, 's', 121, 5.6, 54);

  delete pMemberFunc0;
  delete pMemberFunc1;
  delete pMemberFunc2;
  delete pMemberFunc3;
  delete pMemberFunc4;
  delete pMemberFunc5;
  delete pMemberFunc6;


	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void COnestoreCodeDlgDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void COnestoreCodeDlgDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR COnestoreCodeDlgDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void COnestoreCodeDlgDemoDlg::writeSendBuf(const std::string& str)
{
  CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_SEND);
  CString strEdit;
  pEdit->GetWindowTextW(strEdit);
  strEdit += L"\r\n";
  strEdit += CA2W(str.c_str(), CP_ACP);
  pEdit->SetWindowTextW(strEdit);
}

void COnestoreCodeDlgDemoDlg::writeSendReceive(const std::string& str)
{
  CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_RECEIVE);

  CString strEdit;
  pEdit->GetWindowTextW(strEdit);
  strEdit += L"\r\n";
  strEdit += CA2W(str.c_str(), CP_ACP);
  pEdit->SetWindowTextW(strEdit);
}

// service info
void COnestoreCodeDlgDemoDlg::OnBnClickedButton1()
{
  char lpszbuf[100] = { 0x10, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x00, 0x7b, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x73, // ........{ "req_type" : "s
    0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x22, 0x20, 0x7d, 0x00, 0x00, 0x00, 0x00, 0x00 };
  
  m_onestore.ProcessOnestoreData(lpszbuf, 100);
  writeSendBuf(lpszbuf+8);
}
// auth token
void COnestoreCodeDlgDemoDlg::OnBnClickedButton2()
{
  char lpszbuf[100] = { 0x11, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00, 0x00, 0x7b, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x6f//  ....&...{ "req_type" : "o
    , 0x6e, 0x65, 0x73, 0x74, 0x6f, 0x72, 0x65, 0x5f, 0x61, 0x75, 0x74, 0x68, 0x5f, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x22, 0x20, 0x7d, 0x00, 0x00, 0x00, 0x00 }; //  nestore_auth_token" }....
  m_onestore.ProcessOnestoreData(lpszbuf, 100);
  writeSendBuf(lpszbuf + 8);
}
// client id
void COnestoreCodeDlgDemoDlg::OnBnClickedButton3()
{
  char lpszbuf[100] = { 0x12, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0x00, 0x7b, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x63 //  ........{ "req_type" : "c
    , 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x22, 0x20, 0x7d, 0x00, 0x00, 0x00, 0x00 };
  m_onestore.ProcessOnestoreData(lpszbuf, 100);
  writeSendBuf(lpszbuf + 8);
}
//update seed app
void COnestoreCodeDlgDemoDlg::OnBnClickedButton4()
{
  char lpszbuf[150] = { 0x13, 0x00, 0x00, 0x00, 0x65, 0x00, 0x00, 0x00, 0x7b, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x75 //  ....e...{ "req_type" : "u
    , 0x70, 0x64, 0x61, 0x74, 0x65, 0x5f, 0x73, 0x65, 0x65, 0x64, 0x61, 0x70, 0x70, 0x22, 0x2c, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x63, 0x6c, 0x73, 0x5f //  pdate_seedapp", "req_cls_
    , 0x74, 0x79, 0x70, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x32, 0x22, 0x2c, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x63, 0x6c, 0x73, 0x5f, 0x76, 0x61, 0x6c  //type" : "2", "req_cls_val
    , 0x75, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x63, 0x6f, 0x6d, 0x2e, 0x73, 0x6b, 0x74, 0x2e, 0x73, 0x6b, 0x61, 0x66, 0x2e, 0x4f, 0x41, 0x30, 0x30, 0x30  // ue" : "com.skt.skaf.OA000
    , 0x31, 0x38, 0x32, 0x38, 0x32, 0x22, 0x20, 0x20, 0x7d, 0x00, 0x00, 0x00 };
  m_onestore.ProcessOnestoreData(lpszbuf, 150);
  writeSendBuf(lpszbuf + 8);
}
// product detail
void COnestoreCodeDlgDemoDlg::OnBnClickedButton5()
{
  char lpszbuf[150] = { 0x20, 0x00, 0x00, 0x00, 0x63, 0x00, 0x00, 0x00, 0x7b, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x70 //   ...c...{ "req_type" : "p
    , 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x64, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x22, 0x2c, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x63, 0x6c, 0x73, 0x5f //  roduct_detail", "req_cls_
    , 0x74, 0x79, 0x70, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x32, 0x22, 0x2c, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x63, 0x6c, 0x73, 0x5f, 0x76, 0x61, 0x6c //  type" : "2", "req_cls_val
    , 0x75, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x63, 0x6f, 0x6d, 0x2e, 0x6f, 0x6e, 0x65, 0x73, 0x74, 0x6f, 0x72, 0x65, 0x2e, 0x65, 0x78, 0x61, 0x6d, 0x70 // ue" : "com.onestore.examp
    , 0x6c, 0x65, 0x31, 0x22, 0x20, 0x20, 0x7d, 0x00, 0x00, 0x00, 0x00 };
  m_onestore.ProcessOnestoreData(lpszbuf, 150);
  writeSendBuf(lpszbuf + 8);
}
// error popup
void COnestoreCodeDlgDemoDlg::OnBnClickedButton6()
{
  char lpszbuf[200] = { 0x21, 0x00, 0x00, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x7b, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x73 //  !... | ...{ "req_type" : "s
    , 0x68, 0x6f, 0x77, 0x5f, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x22, 0x2c, 0x20, 0x22, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x54, 0x69//  how_error", "title" : "Ti
    , 0x74, 0x6c, 0x65, 0x20, 0x54, 0x65, 0x73, 0x74, 0x22, 0x2c, 0x20, 0x22, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x73 // tle Test", "message" : "s
    , 0x61, 0x6d, 0x70, 0x6c, 0x65, 0x20, 0x64, 0x61, 0x74, 0x61, 0x2e, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x20 // ample data. this message
    , 0x6d, 0x75, 0x73, 0x74, 0x20, 0x62, 0x65, 0x20, 0x73, 0x68, 0x6f, 0x77, 0x20, 0x69, 0x6e, 0x20, 0x70, 0x6f, 0x70, 0x75, 0x70, 0x20, 0x64, 0x69, 0x61 //  must be show in popup dia
    , 0x6c, 0x6f, 0x67, 0x2e, 0x22, 0x20, 0x7d, 0x00, 0x00, 0x00 };
  m_onestore.ProcessOnestoreData(lpszbuf, 200);
  writeSendBuf(lpszbuf + 8);
}
// show login view
void COnestoreCodeDlgDemoDlg::OnBnClickedButton7()
{
  char lpszbuf[100] = { 0x30, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00, 0x7b, 0x20, 0x22, 0x72, 0x65, 0x71, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x22, 0x20, 0x3a, 0x20, 0x22, 0x73 // 0..."...{ "req_type" : "s
    , 0x68, 0x6f, 0x77, 0x5f, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x5f, 0x76, 0x69, 0x65, 0x77, 0x22, 0x20, 0x7d, 0x00, 0x00, 0x00, 0x00, 0x00 }; // 00 00 00  how_login_view" }........
  m_onestore.ProcessOnestoreData(lpszbuf, 100);
  writeSendBuf(lpszbuf + 8);
}

CString COnestoreCodeDlgDemoDlg::getdlgproductdetail()
{
  CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT1);
  CString strText;
  if (pEdit )
  {
    pEdit->GetWindowTextW(strText);
  }
  return strText;
}

void COnestoreCodeDlgDemoDlg::OnBnClickedButton8()
{
  CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_SEND);
  pEdit->SetWindowTextW(L"");
}

void COnestoreCodeDlgDemoDlg::OnBnClickedButton9()
{
  CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_RECEIVE);
  pEdit->SetWindowTextW(L"");
}