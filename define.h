#pragma once

#define CLIENT_CLASS_NAME _T("{345B0DF4-4FD7-AF13-3DKD87D7A}")

#define ONESTORECLINE_MUTEX_NAME                                                 \
  _T("{KJG778C-DEAE-4a37-DFE4-JDKSL12E273846}_APPMARKET")

#define WNDMSG_ONESTORE_AUTHTOKEN  WM_USER + 1000 + 4
#define WNDMSG_ONESTORE_SHOWLOGINVIEW  WM_USER + 1000 + 5
#define WNDMSG_ONESTORE_NOTIFYAUTH WM_USER + 1000 + 6
#define WNDMSG_ONESTORE_SHOWPRODETAIL  WM_USER + 1000 + 7
