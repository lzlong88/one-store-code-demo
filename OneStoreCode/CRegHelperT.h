//-------------------------------------------------------
// Copyright (c) tencent
// All rights reserved.
//
// File Name: CRegHelperT.h
// File Des: 类工厂注册类
// File Summary:
// Cur Version: 1.0
// Author:
// Create Data:2021-5-22
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron		2021-5-22 	1.0
//-------------------------------------------------------
#pragma once
#include <wtypes.h>

class IRegister;
typedef IRegister *IRegisterPtr;

class IRegister {
public:
  virtual ~IRegister() {}
  virtual PVOID NewObj() = 0;            // 创建自身对象(NewObj)
  virtual IRegisterPtr Clone() = 0;      // 复制自身
  virtual int GetRegType() = 0;          // 根据注册ID取得注册类型
  virtual LPCWSTR GetRegItemName() = 0;  // 根据注册名取得注册类型
};

template <class T> class CRegHelperT : public IRegister {
public:
  // 默认构造函数是必须的，不然无法初始化对象
  CRegHelperT() {}
  ~CRegHelperT() {}

public:
  virtual PVOID NewObj() { return new T; }

  virtual IRegisterPtr Clone() { return new CRegHelperT<T>; }

  virtual int GetRegType() {
    /// 注意，此处使用的是静态GetRegType
    return T::GetRegType();
  }

  /// 注意，此处使用的是静态GetRegItemName
  virtual LPCWSTR GetRegItemName() { return T::GetRegItemName(); }
};
