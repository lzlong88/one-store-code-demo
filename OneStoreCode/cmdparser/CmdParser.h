//-------------------------------------------------------
// Copyright (c) tencent
// All rights reserved.
//
// File Name: CmdParser.h
// File Des: 命令行解析封装类
// File Summary:
// Cur Version: 1.0
// Author:zeron
// 用法：1.void startpkg_cmdAction(const CmdParserMgrPtr& pCmdParserMgr, const
// CmdLineNodePtr& pCmdLineNode);
//          CmdParserMgr CmdParser;
//      加入命令行处理回调：CmdParser.AddParseSlotCmdNode(ArgsParserNode(L"-startpkg",
//                                    FunctorSubscriber(&startpkg_cmdAction)));
//      2.执行解析：CmdParser.DoParseCmdLine();
// Create Data:2021-6-30
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron		2021-6-30 	1.0
//-------------------------------------------------------
#pragma once
#include "../CVecT.h"
#include "../EventMgr/CEventMgr.h"
#include "../FunctionSubscriber.h"

#define NEWCMDPARSER L" -newcmdparser "
#define CMD_DECLARENODE_CLASS(cmd)                                             \
  LPCWSTR getArgsPtr() const override { return cmd; }

class CmdParserMgr;
typedef CmdParserMgr *CmdParserMgrPtr;

class CmdNodeBase;
typedef CmdNodeBase *CmdNodeBasePtr;

class ArgsParserNode;
typedef ArgsParserNode *ArgsParserNodePtr;

class CmdLineNode;
typedef CmdLineNode *CmdLineNodePtr;

class CmdNodeBase {
public:
  CmdNodeBase();
  CmdNodeBase(LPCWSTR lpszCmd);
  virtual CmdNodeBase *Clone() const = 0;
  virtual ~CmdNodeBase() = 0;
  virtual LPCWSTR getArgsPtr() const;

private:
  std::wstring m_strArgs;
};

class ArgsParserNode : public CmdNodeBase {
  typedef FunctorDelegateBase<void(const CmdParserMgrPtr &,
                                   const CmdLineNodePtr &)> *FunctorSlotPtr;

#define FUNCEVENT 100
  typedef FunctorDelegateBase<void(const CmdParserMgrPtr &,
                                   const CmdLineNodePtr &)>
      CmdFunctionSlot;
  typedef CEventMgr<void(const CmdParserMgrPtr &, const CmdLineNodePtr &)>
      CEventMgrT;

public:
  ArgsParserNode();
  ArgsParserNode(LPCWSTR lpszArgs);
  ArgsParserNode(LPCWSTR lpszArgs, const CmdFunctionSlot &slot);
  ArgsParserNode(LPCWSTR lpszArgs, CEventMgrT *pEventMgr);
  virtual ~ArgsParserNode();

  virtual CmdNodeBasePtr Clone() const;
  virtual void DoCmdAction(const CmdParserMgrPtr &pCmdParserMgr,
                           const CmdLineNodePtr &pCmdLineNode);
  void SetFunctionSlot(const CmdFunctionSlot &slot);

private:
  CEventMgr<void(const CmdParserMgrPtr&, const CmdLineNodePtr&)> *m_pEventMgr;
};

class CmdParserItem : public CVecT<CmdNodeBasePtr> {
public:
  CmdParserItem();
  virtual ~CmdParserItem();

public:
  void PreVecObjRemove(const CmdNodeBasePtr &obj) override;
  bool EqualVecObj(const CmdNodeBasePtr &objsrc,
                   const CmdNodeBasePtr &objdest) const override;
};

class CmdLineNode : public CmdNodeBase {
public:
  CmdLineNode(LPCWSTR lpszArgs);
  virtual ~CmdLineNode();
  virtual CmdNodeBasePtr Clone() const;
  bool IsAsAParam();
  void MarkAsAParam();
  CmdLineNode *m_nextnodeptr;

private:
  bool m_bMarkAsAParam;
};

class CmdParserMgr : public CmdParserItem {
public:
  CmdParserMgr(LPCWSTR lpCmdLine = NULL);
  ~CmdParserMgr();

  void AddParseSlotCmdNode(const CmdNodeBase &obj);
  void DoParseCmdLine();

  LPCWSTR GetParserCmdLine() const;
  virtual CmdLineNodePtr GetCmdLineNode(LPCWSTR lpszArgs);

  std::wstring GetNodeBaseString(const CmdNodeBasePtr &pNodeBase);

  virtual CmdNodeBasePtr GetFirstInputParam(const CmdLineNodePtr &pNodeBase);
  virtual std::wstring GetFirstInputParamStr(const CmdLineNodePtr &pNodeBase);
  virtual std::wstring GetFirstInputParamStr(LPCWSTR lpszAgrs);

  virtual CmdNodeBasePtr GetSecondInputParam(const CmdLineNodePtr &pNodeBase);
  virtual std::wstring GetSecondInputParamStr(const CmdLineNodePtr &pNodeBase);
  virtual std::wstring GetSecondInputParamStr(LPCWSTR lpszAgrs);

  virtual CmdNodeBasePtr GetThirdInputParam(const CmdLineNodePtr &pNodeBase);
  virtual std::wstring GetThirdInputParamStr(const CmdLineNodePtr &pNodeBase);
  virtual std::wstring GetThirdInputParamStr(LPCWSTR lpszAgrs);

  virtual CmdNodeBasePtr GetXThInputParam(const CmdLineNodePtr &pNodeBase,
                                          int x);
  virtual std::wstring GetXThInputParamStr(const CmdLineNodePtr &pNodeBase,
                                           int x);
  virtual std::wstring GetXThInputParamStr(LPCWSTR lpszAgrs, int x);

protected:
  void AddNewCmdLineNodeToListTail(const CmdLineNodePtr &pNewNode);
  void FreeCmdLineNodeList();

private:
  CmdLineNodePtr m_CmdLineListCurPtr;
  CmdLineNodePtr m_CmdLineListHeader;
  CmdLineNodePtr m_CmdLineListTailer;
  LPCWSTR m_lpszCmdLine;
};
