#include "stdafx.h"
#include "CmdParser.h"
#include <shellapi.h>
#include "../DeclareDefine.h"

CmdNodeBase::CmdNodeBase() {}

CmdNodeBase::CmdNodeBase(LPCWSTR lpszArgs) {
  m_strArgs = IsValidString(lpszArgs) ? lpszArgs : L"";
}

CmdNodeBase::~CmdNodeBase() {}

LPCWSTR CmdNodeBase::getArgsPtr() const { return m_strArgs.c_str(); }

ArgsParserNode::ArgsParserNode() { m_pEventMgr = new CEventMgrT(); }

ArgsParserNode::ArgsParserNode(LPCWSTR lpszArgs) : CmdNodeBase(lpszArgs) {
  m_pEventMgr = new CEventMgrT();
}

ArgsParserNode::ArgsParserNode(LPCWSTR lpszArgs, const CmdFunctionSlot &slot)
    : CmdNodeBase(lpszArgs) {
  m_pEventMgr = new CEventMgrT();
  m_pEventMgr->SubscribeEvent(FUNCEVENT, slot);
}

ArgsParserNode::ArgsParserNode(LPCWSTR lpszArgs, CEventMgrT *pEventMgr)
    : CmdNodeBase(lpszArgs) {
  m_pEventMgr = EVMRTTI_CAST<
      CEventMgr<void(const CmdParserMgrPtr &, const CmdLineNodePtr &)> *>(
      pEventMgr->Clone());
}

ArgsParserNode::~ArgsParserNode() {
  if (m_pEventMgr) {
    delete m_pEventMgr;
    m_pEventMgr = NULL;
  }
}

CmdNodeBasePtr ArgsParserNode::Clone() const {
  return new ArgsParserNode(getArgsPtr(), m_pEventMgr);
}

void ArgsParserNode::DoCmdAction(const CmdParserMgrPtr &pCmdParserMgr,
                                 const CmdLineNodePtr &pCmdLineNode) {
  if (m_pEventMgr) m_pEventMgr->FireEvent(FUNCEVENT, pCmdParserMgr, pCmdLineNode);
}

void ArgsParserNode::SetFunctionSlot(const CmdFunctionSlot &slot) {
  if (m_pEventMgr) m_pEventMgr->SubscribeEvent(FUNCEVENT, slot);
}

CmdParserItem::CmdParserItem() {}

CmdParserItem::~CmdParserItem() { RemoveAll(); }

void CmdParserItem::PreVecObjRemove(const CmdNodeBasePtr &obj) {
  if (obj) {
    delete obj;
  }
}

bool CmdParserItem::EqualVecObj(const CmdNodeBasePtr &objsrc,
                                const CmdNodeBasePtr &objdest) const {
  return (0 == _wcsicmp(objsrc->getArgsPtr(), objdest->getArgsPtr()));
}

CmdLineNode::CmdLineNode(LPCWSTR lpszArgs)
    : CmdNodeBase(lpszArgs), m_nextnodeptr(NULL), m_bMarkAsAParam(false) {}

CmdLineNode::~CmdLineNode() { m_nextnodeptr = NULL; }

CmdNodeBasePtr CmdLineNode::Clone() const {
  return new CmdLineNode(getArgsPtr());
}

bool CmdLineNode::IsAsAParam() { return m_bMarkAsAParam; }

void CmdLineNode::MarkAsAParam() { m_bMarkAsAParam = true; }

CmdParserMgr::CmdParserMgr(LPCWSTR lpszCmdLine)
    : m_CmdLineListHeader(NULL), m_CmdLineListTailer(NULL),
      m_CmdLineListCurPtr(NULL), m_lpszCmdLine(NULL) {
  do {
    if (!lpszCmdLine) {
      lpszCmdLine = GetCommandLine();
    }
    if (lpszCmdLine == NULL) {
      break;
    }

    int nCnt = 0;
    LPWSTR *lpszArgs = CommandLineToArgvW(lpszCmdLine, &nCnt);
    for (int i = 0; i < nCnt; ++i) {
      AddNewCmdLineNodeToListTail(new CmdLineNode(lpszArgs[i]));
    }
    if (NULL != lpszArgs) {
      LocalFree(lpszArgs);
    }
  } while (FALSE);
  m_lpszCmdLine = lpszCmdLine;
}

CmdParserMgr::~CmdParserMgr() {
  FreeCmdLineNodeList();
  m_lpszCmdLine = NULL;
}

void CmdParserMgr::AddParseSlotCmdNode(const CmdNodeBase &obj) {
  AddObj(obj.Clone());
}

void CmdParserMgr::DoParseCmdLine() {
  m_CmdLineListCurPtr = m_CmdLineListHeader;
  while (m_CmdLineListCurPtr) {
    CmdLineNodePtr CurNodePtr = m_CmdLineListCurPtr;
    m_CmdLineListCurPtr = m_CmdLineListCurPtr->m_nextnodeptr;
    if (CurNodePtr->IsAsAParam()) {
      /// 标记为参数 参数跳过分析
      continue;
    }

    int id = FindObj(CurNodePtr);
    if (id == -1) {
      // not find
      continue;
    }
    ArgsParserNodePtr pParserNode = static_cast<ArgsParserNodePtr>(GetObj(id));
    if (pParserNode) {
      pParserNode->DoCmdAction(this, CurNodePtr);
    }
  }
}

LPCWSTR CmdParserMgr::GetParserCmdLine() const { return m_lpszCmdLine; }

CmdLineNodePtr CmdParserMgr::GetCmdLineNode(LPCWSTR lpszArgs) {
  if (!IsValidString(lpszArgs)) {
    return NULL;
  }
  CmdLineNodePtr pCmdLineNode = m_CmdLineListHeader;
  while (pCmdLineNode) {
    if (0 == _wcsicmp(pCmdLineNode->getArgsPtr(), lpszArgs)) {
      return pCmdLineNode;
    }
    pCmdLineNode = pCmdLineNode->m_nextnodeptr;
  }
  return NULL;
}

std::wstring
CmdParserMgr::GetNodeBaseString(const CmdNodeBasePtr &pNodeBasePtr) {
  if (!pNodeBasePtr) {
    return std::wstring(L"");
  }

  return IsValidString(pNodeBasePtr->getArgsPtr())
             ? std::wstring(pNodeBasePtr->getArgsPtr())
             : std::wstring(L"");
}

CmdNodeBasePtr
CmdParserMgr::GetFirstInputParam(const CmdLineNodePtr &pNodeBase) {
  return GetXThInputParam(pNodeBase, 1);
}

std::wstring
CmdParserMgr::GetFirstInputParamStr(const CmdLineNodePtr &pNodeBase) {
  return GetNodeBaseString(GetFirstInputParam(pNodeBase));
}

std::wstring CmdParserMgr::GetFirstInputParamStr(LPCWSTR lpszAgrs) {
  return GetFirstInputParamStr(GetCmdLineNode(lpszAgrs));
}

CmdNodeBasePtr
CmdParserMgr::GetSecondInputParam(const CmdLineNodePtr &pNodeBase) {
  return GetXThInputParam(pNodeBase, 2);
}

std::wstring
CmdParserMgr::GetSecondInputParamStr(const CmdLineNodePtr &pNodeBase) {
  return GetNodeBaseString(GetSecondInputParam(pNodeBase));
}

std::wstring CmdParserMgr::GetSecondInputParamStr(LPCWSTR lpszAgrs) {
  return GetSecondInputParamStr(GetCmdLineNode(lpszAgrs));
}

CmdNodeBasePtr
CmdParserMgr::GetThirdInputParam(const CmdLineNodePtr &pNodeBase) {
  return GetXThInputParam(pNodeBase, 3);
}

std::wstring
CmdParserMgr::GetThirdInputParamStr(const CmdLineNodePtr &pNodeBase) {
  return GetNodeBaseString(GetThirdInputParam(pNodeBase));
}

std::wstring CmdParserMgr::GetThirdInputParamStr(LPCWSTR lpszAgrs) {
  return GetThirdInputParamStr(GetCmdLineNode(lpszAgrs));
}

CmdNodeBasePtr CmdParserMgr::GetXThInputParam(const CmdLineNodePtr &pNodeBase,
                                              int x) {
  CmdLineNodePtr pCurNode = pNodeBase;
  while (x-- > 0) {
    if (!pCurNode) {
      break;
    }

    pCurNode = pCurNode->m_nextnodeptr;
    if (x == 0 && pCurNode) {
      pCurNode->MarkAsAParam();
      return pCurNode;
    }
  }
  return NULL;
}

std::wstring CmdParserMgr::GetXThInputParamStr(const CmdLineNodePtr &pNodeBase,
                                               int x) {
  return GetNodeBaseString(GetXThInputParam(pNodeBase, x));
}

std::wstring CmdParserMgr::GetXThInputParamStr(LPCWSTR lpszAgrs, int x) {
  return GetXThInputParamStr(GetCmdLineNode(lpszAgrs), x);
}

void CmdParserMgr::AddNewCmdLineNodeToListTail(const CmdLineNodePtr &pNewNode) {
  if (!pNewNode) {
    return;
  }

  if (!m_CmdLineListHeader) {
    m_CmdLineListHeader = pNewNode;
  }

  if (!m_CmdLineListTailer) {
    m_CmdLineListTailer = pNewNode;
  } else {
    m_CmdLineListTailer->m_nextnodeptr = pNewNode;
    m_CmdLineListTailer = pNewNode;
  }
}

void CmdParserMgr::FreeCmdLineNodeList() {
  while (m_CmdLineListHeader) {
    CmdLineNode *next = m_CmdLineListHeader->m_nextnodeptr;
    delete m_CmdLineListHeader;
    m_CmdLineListHeader = next;
  }
  m_CmdLineListHeader = NULL;
  m_CmdLineListTailer = NULL;
}
