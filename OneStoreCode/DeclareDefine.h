//-------------------------------------------------------
// Copyright tencent lnc
// All rights reserved.
//
// File Name: declaredefine.h
// File Des: 常用声明文件
// File Summary:
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron 	2021-6-25 	1.0
//-------------------------------------------------------
#pragma once

#define DECLARE_REG_CLASS(theclass, RegItemName, RegType)                      \
public:                                                                        \
  static int GetRegType() { return RegType; }                                  \
  static LPCWSTR GetRegItemName() { return RegItemName; }

#define __STR(x) #x
#define _STR(x) __STR(x)
#define __WSTR(x) L##x
#define _WSTR(x) __WSTR(x)

#define Enum2Str(x) _STR(x)
#define Enum2WStr(x) _WSTR(Enum2Str(x))

#define SAFEDELEPTR(x)                                                         \
  if (x) {                                                                     \
    delete x;                                                                  \
    x = NULL;                                                                  \
  }

#ifndef IsValidString
#define IsValidString(x) ((x) && (x)[0])
#endif

#ifndef SAFE_STR
#define SAFE_STR(a) IsValidString(a) ? a : L""
#endif