#include "stdafx.h"
#include "local_onestore_protocol.h"
//#include "log.h"

BOOL OST_Request_JSDataBase::FromRequestStr(const std::string &strRequest) {
  BOOL bRet = TRUE;
  try {
    nlohmann::json jsonObj = nlohmann::json::parse(strRequest);
    AssignJsonVal(jsonObj);
  } catch (const std::exception &/*err*/) {
    //LOG_ERROR_PRINTF(_T("%S: parse json data catch except, data: %s \n"),
    //                 __FUNCTION__, CA2W(strRequest.c_str(), CP_ACP));
    //LOG_ERROR_PRINTF(_T("%S: parse json data catch except, error:%s \n"),
    //                 __FUNCTION__, CA2W(err.what(), CP_ACP));
    bRet = FALSE;
  }
  return bRet;
}

void OST_Request_JSDataBase::AssignJsonVal(const nlohmann::json &jsonObj) {
  *this = jsonObj;
}
