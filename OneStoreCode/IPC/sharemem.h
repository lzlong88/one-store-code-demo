#pragma once

#include <string>
#include <windows.h>

/*
共享内存的简单封装。非线程安全
*/
class CShareMem {
public:
  CShareMem();
  ~CShareMem();

public:
  static void set_sa_everyone_priv(SECURITY_DESCRIPTOR &sd,
                                   SECURITY_ATTRIBUTES &sa);
  HRESULT Create(std::wstring const &strName, DWORD dwSize);
  HRESULT Open(std::wstring const &);

  HRESULT Write(BYTE const *pbData, DWORD dwLength, DWORD dwOffset = 0);
  HRESULT Read(BYTE *pbData, DWORD dwLength, DWORD dwOffset = 0);

private:
  HANDLE m_hFileMap;
  BYTE *m_pView;
};
