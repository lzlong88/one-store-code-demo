#include "stdafx.h"
#include "ProcMsgQueue.h"
#include "sharemem.h"
#include <assert.h>
#include <objbase.h>

namespace {
class CMutexLock {
public:
  CMutexLock(HANDLE h) {
    m_hMutex = h;
    ::WaitForSingleObject(h, INFINITE);
  }

  ~CMutexLock() { ::ReleaseMutex(m_hMutex); }

private:
  HANDLE m_hMutex;
};
}  // namespace

#define TOTALSIZE_OFFSET (0)
#define SEMOPHORE_OFFSET (sizeof(DWORD))
#define MUTEX_OFFSET (SEMOPHORE_OFFSET + sizeof(GUID))
#define MSG_COUNT_OFFSET (MUTEX_OFFSET + sizeof(GUID))
#define FRONT_POS_OFFSET (MSG_COUNT_OFFSET + sizeof(DWORD))
#define TAIL_POS_OFFSET (FRONT_POS_OFFSET + sizeof(DWORD))
#define MSG_BEGIN_OFFSET (TAIL_POS_OFFSET + sizeof(DWORD))

CProcMsgQueue::CProcMsgQueue() : m_pMem(NULL), m_hMutex(0), m_hSemophore(0) {}

CProcMsgQueue::~CProcMsgQueue() { this->Close(); }

HRESULT CProcMsgQueue::Create(std::wstring const &strName,
                              DWORD dwQueueSize /* = 64*1024  */) {
  if (m_pMem) {
    return S_FALSE;
  }

  m_pMem = new CShareMem;
  if (m_pMem == NULL) {
    assert(false);
    return E_OUTOFMEMORY;
  }

  HRESULT hr = m_pMem->Create(strName, dwQueueSize);
  if (FAILED(hr)) {
    assert(false);
    delete m_pMem;
    m_pMem = NULL;
    return hr;
  }

  if (hr == S_OK) {
    // 必须先建立Mutex，并获取锁，并等到共享内存一切初始化完毕后释放锁
    GUID g = {0};
    CoCreateGuid(&g);
    TCHAR buf[MAX_PATH] = {0};
    StringFromGUID2(g, buf, MAX_PATH);

    SECURITY_DESCRIPTOR sd;
    SECURITY_ATTRIBUTES sa;
    CShareMem::set_sa_everyone_priv(sd, sa);

    m_hMutex = ::CreateMutexW(&sa, TRUE, buf);
    // 此时应该可以肯定，这个mutex是我创建的
    if (m_hMutex == NULL || ::GetLastError() == ERROR_ALREADY_EXISTS) {
      assert(false);
      delete m_pMem;
      m_pMem = NULL;
      if (NULL != m_hMutex) {
        ::CloseHandle(m_hMutex);
      }
      m_hMutex = NULL;
      return E_FAIL;
    }

    // 初始化，semophore name,mutex name，front pos，tail pos,msg count
    GUID semophoreGUID = {0};
    CoCreateGuid(&semophoreGUID);
    StringFromGUID2(semophoreGUID, buf, MAX_PATH);

    SECURITY_DESCRIPTOR sd_;
    SECURITY_ATTRIBUTES sa_;
    CShareMem::set_sa_everyone_priv(sd_, sa_);
    m_hSemophore = ::CreateSemaphore(&sa_, 0, 4096, buf);

    if (m_hSemophore == NULL || ::GetLastError() == ERROR_ALREADY_EXISTS) {
      assert(false);
      delete m_pMem;
      m_pMem = NULL;
      ::CloseHandle(m_hMutex);
      m_hMutex = NULL;
      if (NULL != m_hSemophore) {
        ::CloseHandle(m_hSemophore);
      }
      m_hSemophore = NULL;
      return E_FAIL;
    }

    SetMutexName(g);
    SetSemophoreName(semophoreGUID);
    SetMsgCount(0);
    SetFrontPos(MSG_BEGIN_OFFSET);
    SetTailPos(MSG_BEGIN_OFFSET);
    // 初始化完毕，现在可以释放Mutex
    ::ReleaseMutex(m_hMutex);
  } else {
    // 打开Mutex
    std::wstring strMutex;
    GetMutexName(strMutex);
    m_hMutex =
        ::OpenMutex(MUTEX_MODIFY_STATE | SYNCHRONIZE, FALSE, strMutex.c_str());
    if (m_hMutex == NULL) {
      assert(false);
      delete m_pMem;
      m_pMem = NULL;
      return E_FAIL;
    }

    std::wstring strSemo;
    GetSemophoreName(strSemo);
    m_hSemophore = ::OpenSemaphore(SEMAPHORE_MODIFY_STATE | SYNCHRONIZE, FALSE,
                                   strSemo.c_str());
    if (m_hSemophore == NULL) {
      assert(false);
      delete m_pMem;
      m_pMem = NULL;
      ::CloseHandle(m_hMutex);
      m_hMutex = NULL;
      return E_FAIL;
    }
  }
  return S_OK;
}

HRESULT CProcMsgQueue::Open(std::wstring const &strName) {
  if (m_pMem) {
    return S_FALSE;
  }

  m_pMem = new CShareMem;
  if (m_pMem == NULL) {
    assert(false);
    return E_OUTOFMEMORY;
  }

  HRESULT hr = m_pMem->Open(strName);
  if (FAILED(hr)) {
    assert(false);
    delete m_pMem;
    m_pMem = NULL;
    return hr;
  }

  // 打开Mutex
  std::wstring strMutex;
  GetMutexName(strMutex);
  m_hMutex =
      ::OpenMutex(MUTEX_MODIFY_STATE | SYNCHRONIZE, FALSE, strMutex.c_str());
  if (m_hMutex == NULL) {
    assert(false);
    delete m_pMem;
    m_pMem = NULL;
    return E_FAIL;
  }

  std::wstring strSemo;
  GetSemophoreName(strSemo);
  m_hSemophore = ::OpenSemaphore(SEMAPHORE_MODIFY_STATE | SYNCHRONIZE, FALSE,
                                 strSemo.c_str());
  if (m_hSemophore == NULL) {
    assert(false);
    delete m_pMem;
    m_pMem = NULL;
    ::CloseHandle(m_hMutex);
    m_hMutex = NULL;
    return E_FAIL;
  }

  return S_OK;
}

void CProcMsgQueue::Close() {
  if (m_pMem) {
    delete m_pMem;
    m_pMem = NULL;
  }
  if (m_hMutex) {
    ::CloseHandle(m_hMutex);
    m_hMutex = NULL;
  }
  if (m_hSemophore) {
    ::CloseHandle(m_hSemophore);
    m_hSemophore = NULL;
  }
}

/*
入队的过程：
1、把元素放到tail pos所在的位置。
2、tail pos指向下一个空的位置。
*/
HRESULT CProcMsgQueue::EnQueue(PMsgInternal const &msg) {
  // 需要先加锁
  CMutexLock lock(m_hMutex);

  DWORD dwCount = GetMsgCount();
  DWORD dwFront = GetFrontPos();
  DWORD dwTail = GetTailPos();

  // 1、判断队列是否满。
  if (dwTail == dwFront && dwCount > 0) {
    assert(false);
    return E_OUTOFMEMORY;
  }

  // 2、判断余下的空间能否容纳msg
  DWORD dwTotalSize = GetTotalSize();
  DWORD dwLeft = 0;
  if (dwTail >= dwFront)
  {
    // 尾指针在头指针前面
    dwLeft = dwFront + dwTotalSize - dwTail - MSG_BEGIN_OFFSET;
  } else {
    dwLeft = dwFront - dwTail;
  }

  if (dwLeft < msg.dwSize) 
  {
    /// 长度不够
    assert(false);
    return E_OUTOFMEMORY;
  }

  if (!m_pMem) {
    assert(false);
    return E_POINTER;
  }

  // 3、ok，现在可以入队了
  // a、元素数目加1
  dwCount++;
  SetMsgCount(dwCount);

  // b、将msg写入 注意，这里要处理消息可能要写到队列头的情况
  if (msg.dwSize + dwTail > dwTotalSize)
  {
    // 要越过边界，需要写到队列的最前面
    DWORD dwFirst = dwTotalSize - dwTail;
    DWORD dwSecond = msg.dwSize - dwFirst;
    BYTE const *pData = (BYTE const *)&msg;
    m_pMem->Write(pData, dwFirst, dwTail);
    m_pMem->Write(pData + dwFirst, dwSecond, MSG_BEGIN_OFFSET);

    // 不可能写到dwFront处
    assert(dwSecond + MSG_BEGIN_OFFSET < dwFront);
  } else {
    m_pMem->Write((BYTE const *)&msg, msg.dwSize, dwTail);
  }

  // c、更新tail pos
  dwTail += msg.dwSize;
  if (dwTail >= dwTotalSize) {
    dwTail -= dwTotalSize;
    dwTail += MSG_BEGIN_OFFSET;
  }
  SetTailPos(dwTail);

  // 通知接收方，到了一个消息
  ::ReleaseSemaphore(m_hSemophore, 1, NULL);
  return S_OK;
}

HRESULT CProcMsgQueue::DeQueue(PMsgInternal *&pmsg,
                               DWORD dwWaitMS /* = INFINITE */) {
  pmsg = NULL;

  // 如果队列中无消息，将阻塞
  HANDLE handles[2];
  handles[0] = m_hSemophore;
  DWORD nCount = 1;

  DWORD dwRet = ::WaitForMultipleObjects(nCount, handles, FALSE, dwWaitMS);
  {
    if (!m_pMem) {
      assert(false);
      Sleep(1);
      return E_POINTER;
    }

    // 如果有消息了，先加锁，然后取消息
    CMutexLock lock(m_hMutex);

    // 1、如果队列为空
    DWORD dwCount = GetMsgCount();
    if (dwCount <= 0) {
      // 没有取到消息，就用一个空消息代替。
      pmsg = reinterpret_cast<PMsgInternal *>(new char[sizeof(PMsgInternal)]);
      pmsg->dwSize = sizeof(PMsgInternal);
      pmsg->dwSrcPid = 0;
      return E_FAIL;
    }

    // 2、得到消息的长度。
    DWORD dwFront = GetFrontPos();
    DWORD dwTotalSize = GetTotalSize();
    DWORD dwSize = GetMsgSize(dwFront, dwTotalSize);

    if (dwSize < sizeof(DWORD) * 3) {
      assert(false);
      return E_FAIL;
    }

    // 3、给消息分配空间
    pmsg = reinterpret_cast<PMsgInternal *>(new char[dwSize]);
    if (pmsg == NULL) {
      assert(false);
      return E_OUTOFMEMORY;
    }

    // 4、读取消息 注意，这里要判断消息是否连续。
    // a、判断消息是否连续
    if (dwFront + dwSize > dwTotalSize)
    {
      // 消息被分开了
      DWORD dwFirst = dwTotalSize - dwFront;
      DWORD dwSecond = dwSize - dwFirst;
      BYTE *pData = (BYTE *)pmsg;

      m_pMem->Read(pData, dwFirst, dwFront);
      m_pMem->Read(pData + dwFirst, dwSecond, MSG_BEGIN_OFFSET);
    } else {
      m_pMem->Read((BYTE *)pmsg, dwSize, dwFront);
    }

    // 5、front pos更新
    dwFront += dwSize;
    if (dwFront >= dwTotalSize) {
      dwFront -= dwTotalSize;
      dwFront += MSG_BEGIN_OFFSET;
    }
    SetFrontPos(dwFront);

    // 6、更新msg count
    dwCount--;
    dwCount = (dwCount == DWORD(-1)) ? 0 : dwCount;
    SetMsgCount(dwCount);
  }

  return S_OK;
}

DWORD CProcMsgQueue::GetMsgSize(DWORD MsgPos, DWORD TotalSize) const {
  DWORD dwSize = 0;
  if (MsgPos + sizeof(DWORD) > TotalSize)
  {
    // 长度DWORD被分开在队列尾部与队列前部
    BYTE *ptr = reinterpret_cast<BYTE *>(&dwSize);
    int nFirst = TotalSize - MsgPos;
    int nSecond = sizeof(DWORD) - nFirst;
    m_pMem->Read(ptr, nFirst, MsgPos);
    m_pMem->Read(ptr + nFirst, nSecond, MSG_BEGIN_OFFSET);
  } else {
    m_pMem->Read((BYTE *)&dwSize, sizeof(DWORD), MsgPos);
  }

  return dwSize;
}

bool CProcMsgQueue::IsEmpty() const { return GetMsgCount() == 0; }

DWORD CProcMsgQueue::GetTotalSize() const {
  if (m_pMem == NULL) {
    assert(false);
    return 0;
  }

  DWORD dwCount = 0;
  m_pMem->Read((BYTE *)&dwCount, sizeof(DWORD), TOTALSIZE_OFFSET);

  return dwCount;
}

DWORD CProcMsgQueue::GetMsgCount() const {
  if (m_pMem == NULL) {
    assert(false);
    return 0;
  }

  DWORD dwCount = 0;
  m_pMem->Read((BYTE *)&dwCount, sizeof(DWORD), MSG_COUNT_OFFSET);

  return dwCount;
}

DWORD CProcMsgQueue::GetFrontPos() const {
  if (m_pMem == NULL) {
    assert(false);
    return 0;
  }

  DWORD dwCount = 0;
  m_pMem->Read((BYTE *)&dwCount, sizeof(DWORD), FRONT_POS_OFFSET);

  return dwCount;
}

DWORD CProcMsgQueue::GetTailPos() const {
  if (m_pMem == NULL) {
    assert(false);
    return 0;
  }

  DWORD dwCount = 0;
  m_pMem->Read((BYTE *)&dwCount, sizeof(DWORD), TAIL_POS_OFFSET);

  return dwCount;
}

HRESULT CProcMsgQueue::GetMutexName(std::wstring &strMutexName) const {
  if (m_pMem == NULL) {
    assert(false);
    return S_FALSE;
  }

  GUID g = {0};
  HRESULT hr = m_pMem->Read((BYTE *)&g, sizeof(GUID), MUTEX_OFFSET);

  TCHAR buf[MAX_PATH] = {0};
  StringFromGUID2(g, buf, MAX_PATH);

  strMutexName = std::wstring(buf);

  return hr;
}

HRESULT CProcMsgQueue::GetSemophoreName(std::wstring &SemoName) const {
  if (m_pMem == NULL) {
    assert(false);
    return S_FALSE;
  }

  GUID g = {0};
  HRESULT hr = m_pMem->Read((BYTE *)&g, sizeof(GUID), SEMOPHORE_OFFSET);

  TCHAR buf[MAX_PATH] = {0};
  StringFromGUID2(g, buf, MAX_PATH);

  SemoName = std::wstring(buf);

  return hr;
}

HRESULT CProcMsgQueue::SetFrontPos(DWORD dw) {
  if (m_pMem == NULL) {
    assert(false);
    return S_FALSE;
  }

  return m_pMem->Write((BYTE const *)&dw, sizeof(DWORD), FRONT_POS_OFFSET);
}

HRESULT CProcMsgQueue::SetMsgCount(DWORD dw) {
  if (m_pMem == NULL) {
    assert(false);
    return S_FALSE;
  }

  return m_pMem->Write((BYTE const *)&dw, sizeof(DWORD), MSG_COUNT_OFFSET);
  ;
}

HRESULT CProcMsgQueue::SetTailPos(DWORD dw) {
  if (m_pMem == NULL) {
    assert(false);
    return S_FALSE;
  }

  return m_pMem->Write((BYTE const *)&dw, sizeof(DWORD), TAIL_POS_OFFSET);
}

HRESULT CProcMsgQueue::SetMutexName(GUID const &g) {
  if (m_pMem == NULL) {
    assert(false);
    return S_FALSE;
  }

  return m_pMem->Write((BYTE const *)&g, sizeof(GUID), MUTEX_OFFSET);
}

HRESULT CProcMsgQueue::SetSemophoreName(GUID const &g) {
  if (m_pMem == NULL) {
    assert(false);
    return S_FALSE;
  }

  return m_pMem->Write((BYTE const *)&g, sizeof(GUID), SEMOPHORE_OFFSET);
}

void CProcMsgQueue::Signal() {
  // 信号量增大，DeQueue肯定返回
  if (m_hSemophore)
    ::ReleaseSemaphore(m_hSemophore, 1, NULL);
}
