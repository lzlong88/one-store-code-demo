#include "stdafx.h"
#include "ProcMsgQueueMgr.h"
#include "ByteBuffer.h"
#include "ProcMsgQueue.h"
#include <process.h>

CProcMsgQueueMgr::CProcMsgQueueMgr()
    : m_strHostNamePrefix(procmsgqueuedefaulthostnameprefix) {}

CProcMsgQueueMgr::~CProcMsgQueueMgr() {
  ClientAllDisconnect();
  StopAllHost();
}

void CProcMsgQueueMgr::SetHostNamePrefix(const std::wstring &strNamePrefix) {
  m_strHostNamePrefix = strNamePrefix;
}

HRESULT
CProcMsgQueueMgr::CreateNewHostById(DWORD dwHostId,
                                    IMsgQueueHandler *pMsgQueueHandler) {
  std::wstring strHostName = GetHostNameById(dwHostId);
  return CreateNewHostByName(strHostName, pMsgQueueHandler);
}

HRESULT
CProcMsgQueueMgr::CreateNewHostByName(const std::wstring &strHostName,
                                      IMsgQueueHandler *pMsgQueueHandler) {
  HRESULT hRet = S_FALSE;
  do {
    MutexAutoLockObj(m_CurHostList);
    if (m_CurHostList.end() != m_CurHostList.find(strHostName)) {
      // 已经存在了
      break;
    }
    struct ThreadPARAM *pParam = new ThreadPARAM;
    pParam->pThis = this;
    pParam->strHostName = strHostName;
    pParam->pMsgQueueHandler = pMsgQueueHandler;
    HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &HostListenThread,
                                            (void *)pParam, 0, NULL);
    MutexAutoLockObj2(m_CurHostHThread);
    m_CurHostHThread[strHostName] = hThread;
    hRet = S_OK;
  } while (FALSE);
  return hRet;
}

std::wstring CProcMsgQueueMgr::GetHostNameById(DWORD dwHostId) {
  wchar_t szHostId[32] = {0};
  _itow_s(dwHostId, szHostId, 10);
  std::wstring strHostName = m_strHostNamePrefix + std::wstring(szHostId);
  return strHostName;
}

unsigned int __stdcall CProcMsgQueueMgr::HostListenThread(PVOID pVoid) {
  ThreadPARAM *pParam = (ThreadPARAM *)pVoid;
  CProcMsgQueueMgr *pThis = pParam->pThis;
  std::wstring strHostName = pParam->strHostName;
  IMsgQueueHandler *pMsgQueueHandler = pParam->pMsgQueueHandler;
  delete pParam;

  CProcMsgQueue *pReadQueue = new CProcMsgQueue;
  HostQueue *pHostQueue = new HostQueue;
  {
    MutexAutoLockObj(pThis->m_CurHostList);
    pThis->m_CurHostList[strHostName] = pHostQueue;
  }
  pHostQueue->bRun = true;
  pHostQueue->pMsgQueue = NULL;

  // 创建共享读内存
  while (pHostQueue->bRun) {
    if (S_OK == pReadQueue->Create(strHostName))
      break;
    Sleep(200);
  }
  pHostQueue->pMsgQueue = pReadQueue;

  // 消息处理
  HRESULT hr;
  PMsgInternal *pMsg;
  while (pHostQueue->bRun) {
    hr = pReadQueue->DeQueue(pMsg, INFINITE);
    if (FAILED(hr)) {
      if (pMsg)
        delete[](char *) pMsg;
      continue;
    }

    if (!pMsgQueueHandler) {
      delete[](char *) pMsg;
      break;
    }
    ByteBuffer bytebuff((uint8_t *)pMsg->buf,
                        (uint32_t)(pMsg->dwSize - sizeof(DWORD) * 3));

    DWORD dwMsgType = bytebuff.get<DWORD>();
    if (bytebuff.underflow()) {
      delete[](char *) pMsg;
      continue;
    }

    if (!pThis->HandleMsgQueueFirst(bytebuff, dwMsgType, pMsg->dwSrcPid)) {
      pMsgQueueHandler->HandleMsgQueue(bytebuff, dwMsgType, pMsg->dwSrcPid);
    }
    delete[](char *) pMsg;
  }

  pHostQueue->pMsgQueue = NULL;
  delete pHostQueue;
  pThis->m_CurHostList.erase(strHostName);
  pReadQueue->Close();
  delete pReadQueue;
  return 0;
}

BOOL CProcMsgQueueMgr::HandleMsgQueueFirst(ByteBuffer &bytebuffer,
                                           DWORD dwMsgType, DWORD clientId) {
  if (dwMsgType ==
      MsgQueue_CLIENT_CONNECTED) {
    // 远程客户端连接了本地服务，附带了远程服务名称，连上对方即可
    std::wstring strRemoteHostName;
    bytebuffer >> strRemoteHostName;
    Connect2HostByName(strRemoteHostName);
    return TRUE;
  } else if (
      dwMsgType ==
      MsgQueue_HOST_CLOSED) {
    // 对方关闭了服务，附带了远程服务名称，要把跟对方的连接断开
    std::wstring strRemoteHostName;
    bytebuffer >> strRemoteHostName;
    ClientDisconnectByName(strRemoteHostName);
    return TRUE;
  }
  return FALSE;
}

void CProcMsgQueueMgr::StopAllHost() {
  std::vector<std::wstring> vtHostList;
  {
    MutexAutoLockObj(m_CurHostList);
    for (std::map<std::wstring, HostQueue *>::iterator it =
             m_CurHostList.begin();
         it != m_CurHostList.end(); it++) {
      vtHostList.push_back(it->first);
    }
  }
  for (size_t id = 0; id < vtHostList.size(); id++) {
    StopHostByName(vtHostList[id]);
  }
}

void CProcMsgQueueMgr::StopHostById(DWORD dwHostId) {
  std::wstring strHostName = GetHostNameById(dwHostId);
  StopHostByName(strHostName);
}

void CProcMsgQueueMgr::StopHostByName(const std::wstring &strHostName) {
  do {
    MutexAutoLockObj(m_CurHostList);
    std::map<std::wstring, HostQueue *>::iterator it =
        m_CurHostList.find(strHostName);
    if (it == m_CurHostList.end()) {
      break;
    }
    if (!it->second) {
      break;
    }

    ByteBuffer bytebuffer;
    bytebuffer.put(MsgQueue_HOST_CLOSED);
    // 附带服务器名称
    bytebuffer << it->first;
    WriteByteBuffer2Host(bytebuffer);

    it->second->bRun = false;
    if (!it->second->pMsgQueue) {
      break;
    }
    it->second->pMsgQueue->Signal();
  } while (FALSE);

  do {
    HANDLE hThread = NULL;
    {
      MutexAutoLockObj(m_CurHostHThread);
      std::map<std::wstring, HANDLE>::iterator it =
          m_CurHostHThread.find(strHostName);
      if (it == m_CurHostHThread.end()) {
        break;
      }
      hThread = it->second;
    }
    if (!hThread) {
      break;
    }
    if (WAIT_TIMEOUT == WaitForSingleObject(hThread, 500)) {
      TerminateThread(hThread, -1);
      /// 强制擦除
      m_CurHostList.erase(strHostName);
    }
    CloseHandle(hThread);
    m_CurHostHThread.erase(strHostName);
  } while (FALSE);
}

CProcMsgQueue *CProcMsgQueueMgr::Connect2HostById(DWORD dwHostId) {
  std::wstring strHostName = GetHostNameById(dwHostId);
  return Connect2HostByName(strHostName);
}

CProcMsgQueue *
CProcMsgQueueMgr::Connect2HostByName(const std::wstring &strHostName) {
  CProcMsgQueue *pMsgQueue = NULL;
  do {
    if (strHostName.empty()) {
      break;
    }
    MutexAutoLockObj(m_ConHostList);
    if (m_ConHostList.end() != m_ConHostList.find(strHostName)) {
      pMsgQueue = m_ConHostList[strHostName];
      break;
    }

    CProcMsgQueue *pWriteMsgQueue = new CProcMsgQueue;
    if (NULL == pWriteMsgQueue) {
      break;
    }
    if (S_OK != pWriteMsgQueue->Open(strHostName)) {
      delete pWriteMsgQueue;
      break;
    }
    m_ConHostList[strHostName] = pWriteMsgQueue;

    /// 把当前host名称发给对方，等待对方连接过来
    MutexAutoLockObj2(m_CurHostList);
    for (std::map<std::wstring, HostQueue *>::iterator it =
             m_CurHostList.begin();
         it != m_CurHostList.end(); it++) {
      ByteBuffer bytebuffer;
      bytebuffer.put(MsgQueue_CLIENT_CONNECTED);
      bytebuffer << it->first;
      WriteByteBuffer2Host(m_ConHostList[strHostName], bytebuffer);
    }
    pMsgQueue = pWriteMsgQueue;
  } while (FALSE);
  return pMsgQueue;
}

HRESULT CProcMsgQueueMgr::WriteByteBuffer2Host(CProcMsgQueue *pWriteQueue,
                                               ByteBuffer &bytebuffer,
                                               DWORD dwClinetId) {
  if (!pWriteQueue)
    return S_FALSE;

  int len = bytebuffer.size();
  /// 构造数据
  char *pTemp = new char[len + 32];

  PMsgInternal *pMsg = (PMsgInternal *)pTemp;
  pMsg->dwSize = len + sizeof(DWORD) * 3;
  pMsg->dwSrcPid = dwClinetId;
  if (len > 0) {
    memcpy(pMsg->buf, &bytebuffer.getBuffer()[0], len);
  }

  // 写数据
  HRESULT hr = pWriteQueue->EnQueue(*pMsg);

  // 返回
  delete[] pTemp;
  return (hr == S_OK) ? true : false;
}

HRESULT CProcMsgQueueMgr::WriteByteBuffer2Host(DWORD dwHostId,
                                               ByteBuffer &bytebuffer,
                                               DWORD dwClinetId) {
  CProcMsgQueue *pWriteQueue = Connect2HostById(dwHostId);
  return WriteByteBuffer2Host(pWriteQueue, bytebuffer, dwClinetId);
}

HRESULT CProcMsgQueueMgr::WriteByteBuffer2Host(ByteBuffer &bytebuffer,
                                               DWORD dwClinetId) {
  if (m_ConHostList.empty()) {
    return E_FAIL;
  }
  MutexAutoLockObj(m_ConHostList);
  for (std::map<std::wstring, CProcMsgQueue *>::iterator it =
           m_ConHostList.begin();
       it != m_ConHostList.end(); it++) {
    WriteByteBuffer2Host(it->second, bytebuffer, dwClinetId);
  }
  return S_OK;
}

HRESULT CProcMsgQueueMgr::ClientDisconnectById(DWORD dwHostId) {
  std::wstring strHostName = GetHostNameById(dwHostId);
  return ClientDisconnectByName(strHostName);
}

HRESULT
CProcMsgQueueMgr::ClientDisconnectByName(const std::wstring &strHostName) {
  MutexAutoLockObj(m_ConHostList);
  if (m_ConHostList.find(strHostName) != m_ConHostList.end()) {
    m_ConHostList[strHostName]->Close();
    CProcMsgQueue *pMsgQueue = m_ConHostList[strHostName];
    m_ConHostList.erase(strHostName);
    delete pMsgQueue;
  }
  return S_OK;
}

HRESULT CProcMsgQueueMgr::ClientAllDisconnect() {
  std::vector<std::wstring> vtConHostList;
  MutexAutoLockObj(m_ConHostList);
  for (std::map<std::wstring, CProcMsgQueue *>::iterator it =
           m_ConHostList.begin();
       it != m_ConHostList.end(); it++) {
    vtConHostList.push_back(it->first);
  }
  for (size_t id = 0; id < vtConHostList.size(); id++) {
    ClientDisconnectByName(vtConHostList[id]);
  }
  return S_OK;
}
