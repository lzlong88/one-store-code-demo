//-------------------------------------------------------
// Copyright (c) tencent cop
// All rights reserved.
//
// File Name: ProcMsgAqueueMgr.h
// File Des: 实现简单的共享内存进程间通信管理类
// File Summary:
// Cur Version: 1.0
// Author:
// Create Data:2021-5-28
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron		2021-5-28 	1.0
//-------------------------------------------------------

#pragma once
#include "StlLockHelper.h"
#include <wtypes.h>
#include <xstring>

enum MsgQueue_type {
  MsgQueue_CLIENT_CONNECTED = 0xFFFFFFE0,
  MsgQueue_HOST_CLOSED,
};

#define procmsgqueuedefaulthostnameprefix                                      \
  L"Local\\serverservice_ds34_34fe_dw23_"
#define msgqueuedefaultserverid 12234

class ByteBuffer;
class IMsgQueueHandler {
public:
  virtual void HandleMsgQueue(ByteBuffer &bytebuffer, DWORD dwMsgType,
                              DWORD clientId) = 0;
};

class CProcMsgQueue;
class CProcMsgQueueMgr {
  struct ThreadPARAM {
    CProcMsgQueueMgr *pThis;
    std::wstring strHostName;
    IMsgQueueHandler *pMsgQueueHandler;
  };

  struct HostQueue {
    CProcMsgQueue *pMsgQueue;
    bool bRun;
  };

public:
  CProcMsgQueueMgr();
  ~CProcMsgQueueMgr();

  void SetHostNamePrefix(const std::wstring &strNamePrefix);

  /// 创建监听Host
  HRESULT CreateNewHostById(DWORD dwHostId,
                            IMsgQueueHandler *pMsgQueueHandler);
  /// 创建监听Host
  HRESULT
  CreateNewHostByName(const std::wstring &strHostName,
                      IMsgQueueHandler *pMsgQueueHandler);

  void StopAllHost();
  void StopHostById(DWORD dwHostId);
  void StopHostByName(const std::wstring &strHostName);

  /// 连接到Host
  CProcMsgQueue *Connect2HostById(DWORD dwHostId);
  /// 连接到Host
  CProcMsgQueue *
  Connect2HostByName(const std::wstring &strHostName);
  /// 写数据
  HRESULT
  WriteByteBuffer2Host(CProcMsgQueue *pWriteQueue, ByteBuffer &,
                       DWORD dwClinetId = GetCurrentProcessId());
  /// 写数据
  HRESULT
  WriteByteBuffer2Host(DWORD dwHostId, ByteBuffer &,
                       DWORD dwClinetId = GetCurrentProcessId());
  /// 往所有连接发送数据
  HRESULT WriteByteBuffer2Host(
      ByteBuffer &,
      DWORD dwClinetId = GetCurrentProcessId());

  HRESULT ClientDisconnectById(DWORD dwHostId);
  HRESULT ClientDisconnectByName(const std::wstring &strHostName);
  HRESULT ClientAllDisconnect();

protected:
  std::wstring GetHostNameById(DWORD dwHostId);
  BOOL HandleMsgQueueFirst(ByteBuffer &bytebuffer, DWORD dwMsgType,
                           DWORD clientId);
  static unsigned int __stdcall HostListenThread(PVOID pVoid);

private:
  std::wstring m_strHostNamePrefix;

  // 当前Host列表
  CStlLockHelper<std::map<std::wstring, HostQueue *>>
      m_CurHostList;
  // 当前Host列表对应线程句柄
  CStlLockHelper<std::map<std::wstring, HANDLE>>
      m_CurHostHThread;
  // 正在连接的host列表
  CStlLockHelper<std::map<std::wstring, CProcMsgQueue *>>
      m_ConHostList;
};
