//-------------------------------------------------------
// Copyright (c) tencent Inc
// All rights reserved.
//
// File Name: StlLockHelper.h
// File Des: 实现简单的stl自动锁保护的封装
// File Summary:
// 使用方式：CStlLockHelper<std::set<int>> setlock;
// 使用方式：CStlLockHelper<std::vector<int>> vtlock;
// 使用方式：CStlLockHelper<std::map<int, int>> maplock;
// 使用方式：CStlLockHelper<std::list<int>> listlock;
// 脱离作用域后外部可以使用MutexAutoLockObj(maplock)进行锁保护
// 脱离作用域后外部可以使用MutexAutoLockObjX(maplock,2)进行锁保护
// Cur Version: 1.0
// Author: zeron
// Create Data:2021-5-28
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron		2021-6-9 	1.0
//-------------------------------------------------------

#pragma once
#include "MutexLock.h"
#include <list>
#include <map>
#include <set>
#include <vector>

#define CONNECT2OBJ(obj1, obj2) obj1##obj2
#define CONNECT3OBJ(obj1, obj2, obj3) CONNECT2OBJ(obj1, obj2)##obj3
#define CONNECT4OBJ(obj1, obj2, obj3, obj4) CONNECT3OBJ(obj1, obj2, obj3)##obj4
#define CONNECT5OBJ(obj1, obj2, obj3, obj4, obj5)                              \
  CONNECT4OBJ(obj1, obj2, obj3, obj4)##obj5
#define CONNECT6OBJ(obj1, obj2, obj3, obj4, obj5, obj6)                        \
  CONNECT5OBJ(obj1, obj2, obj3, obj4, obj5)##obj6

#define MutexAutoLockObj(obj) Mutex::Lock mutexlock((obj).RefMutex());
#define MutexAutoLockObj2(obj) Mutex::Lock mutexlock2((obj).RefMutex());
#define MutexAutoLockObjX(obj, x)                                              \
  Mutex::Lock CONNECT2OBJ(mutexlock, x)((obj).RefMutex());

// 0 个参数的函数定义
#define STLDEFINESAFEFUNCTION0(RET, FUNC)                                      \
  RET FUNC() {                                                                 \
    Mutex::Lock lock(m_mutex);                                                 \
    return CONNECT2OBJ(val., FUNC());                                          \
  }

#define STLDEFINESAFEFUNCTION0_CONST(RET, FUNC)                                \
  RET FUNC() const {                                                           \
    Mutex::Lock lock(m_mutex);                                                 \
    return CONNECT3OBJ(val., FUNC, ());                                        \
  }

// 1 个参数的函数定义
#define STLDEFINESAFEFUNCTION1(RET, FUNC, TYPE, PARAM)                         \
  RET FUNC(TYPE PARAM) {                                                       \
    Mutex::Lock lock(m_mutex);                                                 \
    return CONNECT2OBJ(val., FUNC(PARAM));                                     \
  }

#define STLDEFINESAFEFUNCTION1_CONST(RET, FUNC, TYPE, PARAM)                   \
  RET FUNC(TYPE PARAM) const {                                                 \
    Mutex::Lock lock(m_mutex);                                                 \
    return CONNECT2OBJ(val., FUNC(PARAM));                                     \
  }

template <class T> class CStlLockHelperBase {
public:
  typedef CStlLockHelperBase type;
  // 默认构造函数是必须的，不然无法初始化对象
  CStlLockHelperBase() {}
  ~CStlLockHelperBase() {}

  typedef typename T::iterator iterator;
  typedef typename T::reverse_iterator reverse_iterator;
  typedef typename T::const_reverse_iterator const_reverse_iterator;
  typedef typename T::const_iterator const_iterator;
  typedef typename T::size_type size_type;

  // 返回向量头指针，指向第一个元素iterator begin()
  STLDEFINESAFEFUNCTION0(iterator, begin);

  // const_iterator begin() const
  STLDEFINESAFEFUNCTION0_CONST(const_iterator, begin);

  // 反向迭代器，指向最后一个元素reverse_iterator rbegin()
  STLDEFINESAFEFUNCTION0(reverse_iterator, rbegin);

  // iterator for beginning of reversed:const_reverse_iterator rbegin() const
  STLDEFINESAFEFUNCTION0_CONST(const_reverse_iterator, rbegin);

  // 返回向量尾指针，指向向量最后一个元素的下一个位置iterator end()
  STLDEFINESAFEFUNCTION0(iterator, end);

  // const_iterator end() const
  STLDEFINESAFEFUNCTION0_CONST(const_iterator, end);

  // 反向迭代器，指向第一个元素之前的位置reverse_iterator rend()
  STLDEFINESAFEFUNCTION0(reverse_iterator, rend);

  // const_reverse_iterator rend() const
  STLDEFINESAFEFUNCTION0_CONST(const_reverse_iterator, rend);

  // return iterator for beginning of nonmutable sequence
  // const_iterator cbegin() const _NOEXCEPT
  STLDEFINESAFEFUNCTION0_CONST(const_iterator, cbegin);

  // return iterator for end of nonmutable sequence
  // const_iterator cend() const _NOEXCEPT
  STLDEFINESAFEFUNCTION0_CONST(const_iterator, cend);

  // return iterator for beginning of reversed nonmutable sequence
  // const_reverse_iterator crbegin() const _NOEXCEPT
  STLDEFINESAFEFUNCTION0_CONST(const_reverse_iterator, crbegin);

  // return iterator for end of reversed nonmutable sequence
  // const_reverse_iterator crend() const _NOEXCEPT
  STLDEFINESAFEFUNCTION0_CONST(const_reverse_iterator, crend);

  // return length of sequence
  // size_type size() const _NOEXCEPT
  STLDEFINESAFEFUNCTION0_CONST(size_type, size);

  // return maximum possible length of sequence
  // size_type max_size() const _NOEXCEPT
  STLDEFINESAFEFUNCTION0_CONST(size_type, max_size);

  // 判断向量是否为空，若为空，则向量中无元素 bool empty() const
  STLDEFINESAFEFUNCTION0_CONST(bool, empty);

  // 清空向量中所有元素void clear()
  STLDEFINESAFEFUNCTION0(void, clear);

  // 返回当前向量所能容纳的最大元素值int capacity() const
  STLDEFINESAFEFUNCTION0_CONST(int, capacity);

  void lock() { m_mutex.lock(); }

  void unlock() { m_mutex.unlock(); }

  T &RefValue() { return val; }

  const Mutex &RefMutex() const { return m_mutex; }

protected:
  T val;
  Mutex m_mutex;
};

template <typename E> class CStlLockHelper;

// 半特化vector保护类
template <typename _Ty>
class CStlLockHelper<std::vector<_Ty>>
    : public CStlLockHelperBase<std::vector<_Ty>> {
public:
  typedef CStlLockHelper type;
  // 默认构造函数是必须的，不然无法初始化对象
  CStlLockHelper() {}
  ~CStlLockHelper() {}

  typedef typename CStlLockHelperBase::iterator iterator;
  typedef typename CStlLockHelperBase::reverse_iterator reverse_iterator;
  typedef typename CStlLockHelperBase::const_reverse_iterator
      const_reverse_iterator;
  typedef typename CStlLockHelperBase::const_iterator const_iterator;
  typedef typename CStlLockHelperBase::size_type size_type;

  typedef typename std::vector<_Ty>::value_type value_type;
  typedef typename std::vector<_Ty>::difference_type difference_type;
  typedef typename std::vector<_Ty>::pointer pointer;
  typedef typename std::vector<_Ty>::const_pointer const_pointer;
  typedef typename std::vector<_Ty>::reference reference;
  typedef typename std::vector<_Ty>::const_reference const_reference;
  typedef typename std::vector<_Ty>::_Myt _Myt;

  CStlLockHelper(size_type _Count) : val(_Count) {}
  CStlLockHelper(size_type _Count, const value_type &_Val)
      : val(_Count, _Val) {}
  CStlLockHelper(_Myt &&_Right) : val(_Right) {}

  _Myt &operator=(_Myt &&_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right;
    return val;
  }

  CStlLockHelper &operator=(CStlLockHelper &&_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right.val;
    return *this;
  }

  _Myt &operator=(const _Myt &_Right) {
    // assign _Right
    Mutex::Lock lock(m_mutex);
    val = _Right;
    return val;
  }

  CStlLockHelper &operator=(const CStlLockHelper &_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right.val;
    return *this;
  }

  void swap(_Myt &_Right) {
    Mutex::Lock lock(m_mutex);
    val.swap(_Right);
  }

  void swap(CStlLockHelper &_Right) {
    Mutex::Lock lock(m_mutex);
    val.swap(_Right.val);
  }

  // insert by moving into element at end
  template <class... _Valty> void emplace_back(_Valty &&..._Val) {
    Mutex::Lock lock(m_mutex);
    val.emplace_back(_Val...);
  }

  // insert by moving _Val at _Where
  template <class... _Valty>
  iterator emplace(const_iterator _Where, _Valty &&..._Val) {
    Mutex::Lock lock(m_mutex);
    val.emplace(_Where, _Val...);
  }
  // 删除向量中迭代器指向元素iterator erase(const_iterator _Where)
  STLDEFINESAFEFUNCTION1(iterator, erase, const_iterator, _Where);

  // 删除向量中[first, last)中元素iterator erase(iterator first, iterator last)
  iterator erase(const_iterator _First_arg, const_iterator _Last_arg) {
    Mutex::Lock lock(m_mutex);
    return val.erase(_First_arg, _Last_arg);
  }

  template <class _Iter> void _Construct(_Iter _First, _Iter _Last) {
    Mutex::Lock lock(m_mutex);
    val._Construct(_First, _Last);
  }

  void _Construct_n(size_type _Count, const value_type *_Pval) {
    Mutex::Lock lock(m_mutex);
    val._Construct_n(_Count, _Pval);
  }

  // determine new minimum length of allocated storage
  void reserve(size_type _Count) {
    Mutex::Lock lock(m_mutex);
    val.reserve(_Count);
  }

  // return current length of allocated storage
  size_type capacity() const {
    Mutex::Lock lock(m_mutex);
    val.capacity();
  }

  // return iterator for end of nonmutable sequence
  const_iterator cend() const {
    Mutex::Lock lock(m_mutex);
    return val.cend();
  }

  // return iterator for beginning of reversed nonmutable sequence
  const_reverse_iterator crbegin() const {
    Mutex::Lock lock(m_mutex);
    return val.crbegin();
  }

  // return iterator for end of reversed nonmutable sequence
  const_reverse_iterator crend() const {
    Mutex::Lock lock(m_mutex);
    return val.crend();
  }

  void resize(size_type _Newsize) {
    Mutex::Lock lock(m_mutex);
    val.resize(_Newsize);
  }

  void resize(size_type _Newsize, const value_type &_Val) {
    Mutex::Lock lock(m_mutex);
    val.resize(_Newsize, _Val);
  }

  const_reference at(size_type _Pos) const {
    Mutex::Lock lock(m_mutex);
    return val.at(_Pos);
  }

  reference at(size_type _Pos) {
    Mutex::Lock lock(m_mutex);
    return val.at(_Pos);
  }

  reference operator[](size_type _Pos) {
    Mutex::Lock lock(m_mutex);
    return val.operator[](_Pos);
  }

  const_reference operator[](size_type _Pos) const {
    Mutex::Lock lock(m_mutex);
    return val.operator[](_Pos);
  }

  // return first element of mutable sequence
  reference front() {
    Mutex::Lock lock(m_mutex);
    return val.front();
  }

  const_reference front() const {
    Mutex::Lock lock(m_mutex);
    return val.front();
  }

  reference back() {
    Mutex::Lock lock(m_mutex);
    return val.back();
  }

  const_reference back() const {
    Mutex::Lock lock(m_mutex);
    return val.back();
  }

  // 向量尾部增加一个元素X insert by moving into element at end
  void push_back(const value_type &_Val) {
    Mutex::Lock lock(m_mutex);
    val.push_back(_Val);
  }

  // 删除向量中最后一个元素 erase element at end
  void pop_back() {
    Mutex::Lock lock(m_mutex);
    val.pop_back();
  }

  void assign(size_type _Count, const value_type &_Val) {
    Mutex::Lock lock(m_mutex);
    val.assign(_Count, _Val);
  }

  // 向量中迭代器指向元素前增加一个元素x iterator insert(const_iterator _Where,
  // const _Ty& _Val)
  iterator insert(const_iterator _Where, const _Ty &_Val) {
    // insert by moving _Val at _Where
    Mutex::Lock lock(m_mutex);
    return val.insert(_Where, _Val);
  }

  // 向量中迭代器指向元素前增加n个相同的元素x
  iterator insert(const_iterator _Where, size_type _Count, const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_Where, _Count, _Val);
  }

  void _Reallocate(size_type _Count) {
    Mutex::Lock lock(m_mutex);
    val._Reallocate(_Count);
  }

  void _Reserve(size_type _Count) {
    Mutex::Lock lock(m_mutex);
    val._Reserve(_Count);
  }
};

// 半特化set保护类
template <typename _Kty>
class CStlLockHelper<std::set<_Kty>>
    : public CStlLockHelperBase<std::set<_Kty>> {
public:
  typedef CStlLockHelper type;
  // 默认构造函数是必须的，不然无法初始化对象
  CStlLockHelper() {}
  ~CStlLockHelper() {}

  typedef typename CStlLockHelperBase::iterator iterator;
  typedef typename CStlLockHelperBase::reverse_iterator reverse_iterator;
  typedef typename CStlLockHelperBase::const_reverse_iterator
      const_reverse_iterator;
  typedef typename CStlLockHelperBase::const_iterator const_iterator;

  typedef typename std::set<_Kty>::_Myt _Myt;
  typedef typename std::set<_Kty>::key_type key_type;
  typedef typename std::set<_Kty>::_Pairib _Pairib;
  typedef typename std::set<_Kty>::key_compare key_compare;
  typedef typename std::set<_Kty>::value_compare value_compare;
  typedef typename std::set<_Kty>::allocator_type allocator_type;
  typedef typename std::set<_Kty>::size_type size_type;
  typedef typename std::set<_Kty>::difference_type difference_type;
  typedef typename std::set<_Kty>::pointer pointer;
  typedef typename std::set<_Kty>::const_pointer const_pointer;
  typedef typename std::set<_Kty>::reference reference;
  typedef typename std::set<_Kty>::const_reference const_reference;
  typedef typename std::set<_Kty>::value_type value_type;

  CStlLockHelper(_Myt &&_Right) : val(_Right) {}

  _Myt &operator=(_Myt &&_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right;
    return val;
  }

  CStlLockHelper &operator=(CStlLockHelper &&_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right.val;
    return *this;
  }

  CStlLockHelper &operator=(const CStlLockHelper &_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right.val;
    return *this;
  }

  _Myt &operator=(const _Myt &_Right) {
    // assign _Right
    Mutex::Lock lock(m_mutex);
    val = _Right;
    return val;
  }

  _Pairib insert(value_type &&_Val) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_Val);
  }

  iterator insert(const_iterator _Where, value_type &&_Val) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_Where, _Val);
  }

  void swap(_Myt &_Right) {
    Mutex::Lock lock(m_mutex);
    val.swap(_Right);
  }

  _Pairib insert(const value_type &_Val) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_Val);
  }

  iterator insert(const_iterator _Where, const value_type &_Val) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_Where, _Val);
  }

  template <class _Iter> void insert(_Iter _First, _Iter _Last) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_First, _Last);
  }

  template <class... _Valty> _Pairib emplace(_Valty &&..._Val) {
    Mutex::Lock lock(m_mutex);
    return val.emplace(_Val...);
  }

  // erase and count all that match _Keyval
  size_type erase(const key_type &_Keyval) {
    Mutex::Lock lock(m_mutex);
    return val.erase(_Keyval);
  }

  // erase [_First, _Last)
  iterator erase(const_iterator _First, const_iterator _Last) {
    Mutex::Lock lock(m_mutex);
    return val.erase(_First, _Last);
  }

  // find an element in mutable sequence that matches _Keyval
  iterator find(const key_type &_Keyval) {
    Mutex::Lock lock(m_mutex);
    return val.find(_Keyval);
  }

  // find an element in nonmutable sequence that matches _Keyval
  const_iterator find(const key_type &_Keyval) const {
    Mutex::Lock lock(m_mutex);
    return val.find(_Keyval);
  }
};

// 偏特化list保护类
template <typename _Ty>
class CStlLockHelper<std::list<_Ty>>
    : public CStlLockHelperBase<std::list<_Ty>> {
public:
  typedef CStlLockHelper type;
  // 默认构造函数是必须的，不然无法初始化对象
  CStlLockHelper() {}
  ~CStlLockHelper() {}

  typedef typename CStlLockHelperBase::iterator iterator;
  typedef typename CStlLockHelperBase::reverse_iterator reverse_iterator;
  typedef typename CStlLockHelperBase::const_reverse_iterator
      const_reverse_iterator;
  typedef typename CStlLockHelperBase::const_iterator const_iterator;

  typedef typename std::list<_Ty>::_Myt _Myt;
  typedef typename std::list<_Ty>::size_type size_type;
  typedef typename std::list<_Ty>::difference_type difference_type;
  typedef typename std::list<_Ty>::pointer pointer;
  typedef typename std::list<_Ty>::const_pointer const_pointer;
  typedef typename std::list<_Ty>::reference reference;
  typedef typename std::list<_Ty>::const_reference const_reference;
  typedef typename std::list<_Ty>::value_type value_type;
  typedef typename std::list<_Ty>::_Unchecked_const_iterator
      _Unchecked_const_iterator;
  typedef typename std::list<_Ty>::_Unchecked_iterator _Unchecked_iterator;

  CStlLockHelper(_Myt &&_Right) : val(_Right) {}

  CStlLockHelper(size_type _Count) : val(_Count) {}

  CStlLockHelper(size_type _Count, const _Ty &_Val) : val(_Count, _Val) {}

  _Myt &operator=(_Myt &&_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right;
    return val;
  }

  CStlLockHelper &operator=(CStlLockHelper &&_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right.val;
    return *this;
  }

  CStlLockHelper &operator=(const CStlLockHelper &_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right.val;
    return *this;
  }

  _Myt &operator=(const _Myt &_Right) {
    // assign _Right
    Mutex::Lock lock(m_mutex);
    val = _Right;
    return val;
  }

  template <class _Iter> void _Construct(_Iter _First, _Iter _Last) {
    Mutex::Lock lock(m_mutex);
    val._Construct(_First, _Last);
  }

  void _Construct_n(size_type _Count, const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    val._Construct(_Count, _Val);
  }

  void push_front(_Ty &&_Val) {
    Mutex::Lock lock(m_mutex);
    val.push_front(_Val);
  }

  void push_back(_Ty &&_Val) {
    Mutex::Lock lock(m_mutex);
    val.push_back(_Val);
  }

  iterator insert(const_iterator _Where, _Ty &&_Val) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_Where, _Val);
  }

  template <class... _Valty> void emplace_front(_Valty &&..._Val) {
    Mutex::Lock lock(m_mutex);
    val.emplace_front(_Val...);
  }

  template <class... _Valty> void emplace_back(_Valty &&..._Val) {
    Mutex::Lock lock(m_mutex);
    val.emplace_back(_Val...);
  }

  template <class... _Valty>
  iterator emplace(const_iterator _Where, _Valty &&..._Val) {
    Mutex::Lock lock(m_mutex);
    return val.emplace(_Where, _Val...);
  }

  template <class... _Valty>
  void _Insert(_Unchecked_const_iterator _Where, _Valty &&..._Val) {
    Mutex::Lock lock(m_mutex);
    val._Insert(_Where, _Val...);
  }

  void resize(size_type _Newsize) {
    Mutex::Lock lock(m_mutex);
    val.resize(_Newsize);
  }

  void resize(size_type _Newsize, const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    val.resize(_Newsize, _Val);
  }

  reference front() {
    Mutex::Lock lock(m_mutex);
    return val.front();
  }

  const_reference back() const {
    Mutex::Lock lock(m_mutex);
    return val.back();
  }

  void push_front(const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    val.push_front(_Val);
  }

  void pop_front() {
    Mutex::Lock lock(m_mutex);
    val.pop_front();
  }

  void push_back(const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    val.push_back(_Val);
  }

  void pop_back() {
    Mutex::Lock lock(m_mutex);
    val.pop_back();
  }

  void assign(size_type _Count, const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    val.assign(_Count, _Val);
  }

  iterator insert(const_iterator _Where, const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    return val.assign(_Where, _Val);
  }

  iterator insert(const_iterator _Where, size_type _Count, const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_Where, _Count, _Val);
  }

  iterator erase(const_iterator _Where) {
    Mutex::Lock lock(m_mutex);
    return val.erase(_Where);
  }

  iterator erase(const_iterator _First, const_iterator _Last) {
    Mutex::Lock lock(m_mutex);
    return val.erase(_First, _Last);
  }

  void swap(_Myt &_Right) {
    Mutex::Lock lock(m_mutex);
    val.swap(_Right);
  }

  // splice all of _Right at _Where
  void splice(const_iterator _Where, _Myt &_Right) {
    Mutex::Lock lock(m_mutex);
    val.splice(_Where, _Right);
  }

  // splice all of _Right at _Where
  void splice(const_iterator _Where, _Myt &&_Right) {
    Mutex::Lock lock(m_mutex);
    val.splice(_Where, _Right);
  }

  // splice _Right [_First, _First + 1) at _Where
  void splice(const_iterator _Where, _Myt &_Right, const_iterator _First) {
    Mutex::Lock lock(m_mutex);
    val.splice(_Where, _Right, _First);
  }

  // splice _Right [_First, _First + 1) at _Where
  void splice(const_iterator _Where, _Myt &&_Right, const_iterator _First) {
    Mutex::Lock lock(m_mutex);
    val.splice(_Where, _Right, _First);
  }

  // splice _Right [_First, _Last) at _Where
  void splice(const_iterator _Where, _Myt &_Right, const_iterator _First,
              const_iterator _Last) {
    Mutex::Lock lock(m_mutex);
    val.splice(_Where, _Right, _First, _Last);
  }

  // splice _Right [_First, _Last) at _Where
  void splice(const_iterator _Where, _Myt &&_Right, const_iterator _First,
              const_iterator _Last) {
    Mutex::Lock lock(m_mutex);
    val.splice(_Where, _Right, _First, _Last);
  }

  // erase each element matching _Val
  void remove(const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    val.remove(_Val);
  }

  // erase each element satisfying _Pred
  template <class _Pr1> void remove_if(_Pr1 _Pred) {
    Mutex::Lock lock(m_mutex);
    val.remove_if(_Pred);
  }

  // erase each element satisfying _Pred
  template <class _Pr1> void _Remove_if(_Pr1 &_Pred) {
    Mutex::Lock lock(m_mutex);
    val.remove_if(_Pred);
  }

  // erase each element matching previous
  void unique() {
    Mutex::Lock lock(m_mutex);
    val.unique();
  }

  // merge in elements from _Right, both ordered by operator<
  void merge(_Myt &_Right) {
    Mutex::Lock lock(m_mutex);
    val.merge(_Right);
  }

  // merge in elements from _Right, both ordered by operator<
  void merge(_Myt &&_Right) {
    Mutex::Lock lock(m_mutex);
    val.merge(_Right);
  }

  // merge in elements from _Right, both ordered by _Pred
  template <class _Pr2> void merge(_Myt &_Right, _Pr2 _Pred) {
    Mutex::Lock lock(m_mutex);
    val.merge(_Right, _Pred);
  }

  // merge in elements from _Right, both ordered by _Pred
  template <class _Pr2> void merge(_Myt &&_Right, _Pr2 _Pred) {
    Mutex::Lock lock(m_mutex);
    val.merge(_Right, _Pred);
  }

  // merge in elements from _Right, both ordered by _Pred
  template <class _Pr2> void _Merge1(_Myt &_Right, _Pr2 &&_Pred) {
    Mutex::Lock lock(m_mutex);
    val._Merge1(_Right, _Pred);
  }

  // order sequence, using operator<
  void sort() {
    Mutex::Lock lock(m_mutex);
    val.sort();
  }

  // order sequence, using _Pred
  template <class _Pr2> void sort(_Pr2 _Pred) {
    Mutex::Lock lock(m_mutex);
    val.sort(_Pred);
  }

  // reverse sequence
  void reverse() {
    Mutex::Lock lock(m_mutex);
    val.sort(_Pred);
  }

  // insert _Count * _Val at _Where
  void _Insert_n(_Unchecked_const_iterator _Where, size_type _Count,
                 const _Ty &_Val) {
    Mutex::Lock lock(m_mutex);
    val._Insert_n(_Where, _Count, _Val);
  }

  // alter element count, with checking
  void _Incsize(size_type _Count) {
    Mutex::Lock lock(m_mutex);
    val._Incsize(_Count);
  }
};

// 半特化map保护类
template <typename _Kty, class _Ty>
class CStlLockHelper<std::map<_Kty, _Ty>>
    : public CStlLockHelperBase<std::map<_Kty, _Ty>> {
public:
  // 默认构造函数是必须的，不然无法初始化对象
  CStlLockHelper() {}
  ~CStlLockHelper() {}

  typedef typename CStlLockHelperBase::iterator iterator;
  typedef typename CStlLockHelperBase::reverse_iterator reverse_iterator;
  typedef typename CStlLockHelperBase::const_reverse_iterator
      const_reverse_iterator;
  typedef typename CStlLockHelperBase::const_iterator const_iterator;

  typedef typename std::map<_Kty, _Ty>::_Myt _Myt;
  typedef typename std::map<_Kty, _Ty>::key_type key_type;
  typedef typename std::map<_Kty, _Ty>::mapped_type mapped_type;
  typedef typename std::map<_Kty, _Ty>::_Pairib _Pairib;
  typedef typename std::map<_Kty, _Ty>::key_compare key_compare;
  typedef typename std::map<_Kty, _Ty>::value_compare value_compare;
  typedef typename std::map<_Kty, _Ty>::allocator_type allocator_type;
  typedef typename std::map<_Kty, _Ty>::size_type size_type;
  typedef typename std::map<_Kty, _Ty>::difference_type difference_type;
  typedef typename std::map<_Kty, _Ty>::pointer pointer;
  typedef typename std::map<_Kty, _Ty>::const_pointer const_pointer;
  typedef typename std::map<_Kty, _Ty>::reference reference;
  typedef typename std::map<_Kty, _Ty>::const_reference const_reference;
  typedef typename std::map<_Kty, _Ty>::value_type value_type;

  CStlLockHelper(_Myt &&_Right) : val(_Right) {}

  template <class _Iter>
  CStlLockHelper(_Iter _First, _Iter _Last) : val(_First, _Last) {}

  _Myt &operator=(_Myt &&_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right;
    return val;
  }

  CStlLockHelper(const _Myt &_Right) : val(_Right) {}

  CStlLockHelper &operator=(CStlLockHelper &&_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right.val;
    return *this;
  }

  CStlLockHelper &operator=(const CStlLockHelper &_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right.val;
    return *this;
  }

  // assign by copying _Right
  _Myt &operator=(const _Myt &_Right) {
    Mutex::Lock lock(m_mutex);
    val = _Right;
    return val;
  }

  // find element matching _Keyval or insert with default mapped
  mapped_type &operator[](key_type &&_Keyval) {
    Mutex::Lock lock(m_mutex);
    return val.operator[](_Keyval);
  }

  // find element matching _Keyval or insert with default mapped
  mapped_type &operator[](const key_type &_Keyval) {
    Mutex::Lock lock(m_mutex);
    return val.operator[](_Keyval);
  }

  // exchange contents with non-movable _Right
  void swap(_Myt &_Right) {
    Mutex::Lock lock(m_mutex);
    return val.swap(_Right);
  }

  // find element matching _Keyval
  mapped_type &at(const key_type &_Keyval) {
    Mutex::Lock lock(m_mutex);
    return val.at(_Keyval);
  }

  // find element matching _Keyval
  const mapped_type &at(const key_type &_Keyval) const {
    Mutex::Lock lock(m_mutex);
    return val.at(_Keyval);
  }

  void insert(const std::pair<_Kty, _Ty> &Item) {
    Mutex::Lock lock(m_mutex);
    val.insert(Item);
  }

  // try to insert node with value _Val, favoring right side
  _Pairib insert(value_type &&_Val) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_Val);
  }

  // try to insert node with value _Val using _Where as a hint
  iterator insert(const_iterator _Where, value_type &&_Val) {
    Mutex::Lock lock(m_mutex);
    return val.insert(_Where, _Val);
  }

  iterator erase(const_iterator _Where) {
    Mutex::Lock lock(m_mutex);
    return val.erase(_Where);
  }

  // erase [_First, _Last)
  iterator erase(const_iterator _First, const_iterator _Last) {
    Mutex::Lock lock(m_mutex);
    return val.erase(_First, _Last);
  }

  // erase and count all that match _Keyval
  size_type erase(const key_type &_Keyval) {
    Mutex::Lock lock(m_mutex);
    return val.erase(_Keyval);
  }

  // find an element in mutable sequence that matches _Keyval
  iterator find(const key_type &_Keyval) {
    Mutex::Lock lock(m_mutex);
    return val.find(_Keyval);
  }

  // find an element in nonmutable sequence that matches _Keyval
  const_iterator find(const key_type &_Keyval) const {
    Mutex::Lock lock(m_mutex);
    return val.find(_Keyval);
  }
};
