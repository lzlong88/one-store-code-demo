#include "stdafx.h"
#include "ShareMem.h"
#include <cassert>

CShareMem::CShareMem() : m_hFileMap(NULL), m_pView(NULL) {}

CShareMem::~CShareMem() {
  if (m_pView) {
    ::UnmapViewOfFile(m_pView);
    m_pView = NULL;
  }

  if (m_hFileMap) {
    ::CloseHandle(m_hFileMap);
    m_hFileMap = NULL;
  }
}

void CShareMem::set_sa_everyone_priv(SECURITY_DESCRIPTOR &sd,
                                     SECURITY_ATTRIBUTES &sa) {
  BOOL b = InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
  b = SetSecurityDescriptorDacl(&sd, TRUE, NULL, FALSE);

  sa.nLength = sizeof(SECURITY_ATTRIBUTES);
  sa.bInheritHandle = FALSE;
  sa.lpSecurityDescriptor = &sd;
}

HRESULT CShareMem::Create(std::wstring const &strName, DWORD dwSize) {
  SECURITY_DESCRIPTOR sd;
  SECURITY_ATTRIBUTES sa;
  set_sa_everyone_priv(sd, sa);

  m_hFileMap = ::CreateFileMapping(
      INVALID_HANDLE_VALUE, &sa, PAGE_READWRITE, 0,
      dwSize + sizeof(DWORD)  // 最前面的四个字节表示共享内存的大小
      ,
      strName.c_str());

  if (m_hFileMap == NULL) {
    return E_FAIL;
  }

  HRESULT hr = S_OK;
  if (::GetLastError() == ERROR_ALREADY_EXISTS) {
    // 已经存在
    hr = S_FALSE;
  }

  m_pView = (BYTE *)::MapViewOfFile(m_hFileMap, FILE_MAP_WRITE | FILE_MAP_READ,
                                    0, 0, 0);

  if (m_pView == NULL) {
    ::CloseHandle(m_hFileMap);
    m_hFileMap = NULL;
    return E_FAIL;
  }

  // 将共享内存的长度记录下来
  *(DWORD *)(m_pView) = dwSize + sizeof(DWORD);

  return hr;
}

HRESULT CShareMem::Open(std::wstring const &strName) {
  m_hFileMap =
      ::OpenFileMapping(FILE_MAP_WRITE | FILE_MAP_READ, FALSE, strName.c_str());
  if (m_hFileMap == NULL) {
    return E_FAIL;
  }

  m_pView = (BYTE *)::MapViewOfFile(m_hFileMap, FILE_MAP_WRITE | FILE_MAP_READ,
                                    0, 0, 0);

  if (m_pView == NULL) {
    ::CloseHandle(m_hFileMap);
    m_hFileMap = NULL;
    return E_FAIL;
  }

  return S_OK;
}

HRESULT CShareMem::Write(BYTE const *pbData, DWORD dwLength,
                         DWORD dwOffset /* = 0 */) {
  DWORD dwSize = *(DWORD *)(m_pView);

  if (dwOffset + dwLength > dwSize) {
    assert(false);
    return E_OUTOFMEMORY;
  }
  ::CopyMemory(m_pView + dwOffset, pbData, dwLength);
  return S_OK;
}

HRESULT CShareMem::Read(BYTE *pbData, DWORD dwLength,
                        DWORD dwOffset /* = 0 */) {
  DWORD dwSize = *(DWORD *)(m_pView);

  if (dwOffset + dwLength > dwSize) {
    assert(false);
    return E_OUTOFMEMORY;
  }

  ::CopyMemory(pbData, m_pView + dwOffset, dwLength);
  return S_OK;
}
