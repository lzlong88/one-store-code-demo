#include "stdafx.h"

#include "../define.h"
#include "local_onestore.h"
#include "IPC/ByteBuffer.h"
#include "local_onestore_protocol.h"

#include "../OnestoreCodeDlgDemoDlg.h"

#include "atlbase.h"
#include "atlstr.h"

COnestoreCodeDlgDemoDlg* g_pDlg = NULL;
static BOOL StartAppMarket(const std::wstring &params) {
  CString strMarketPath;
  CEdit* pEdit = (CEdit*)g_pDlg->GetDlgItem(1011/*IDC_EDIT2*/);
  pEdit->GetWindowTextW(strMarketPath);
  std::wstring marketPath = (LPCTSTR)strMarketPath;

  std::wstring parameter = L" -newcmdparser " + params;
  PROCESS_INFORMATION processInformation = {0};
  STARTUPINFO startupInfo = {0};
  startupInfo.cb = sizeof(STARTUPINFO);
  BOOL result = CreateProcessW(
      marketPath.c_str(), const_cast<wchar_t *>(parameter.c_str()), nullptr,
      nullptr, FALSE, 0, nullptr, nullptr, &startupInfo, &processInformation);

  if (processInformation.hProcess) {
    CloseHandle(processInformation.hProcess);
  }
  if (processInformation.hThread) {
    CloseHandle(processInformation.hThread);
  }
  return result;
}

static BOOL IsAppInstanceExist(LPCTSTR szAppInstanceName) {
  HANDLE hMutex = NULL;

  DWORD dwPid = ::GetCurrentProcessId();
  DWORD dwSid = 0;
  TCHAR szBuf[MAX_PATH] = { 0 };

  ::ProcessIdToSessionId(dwPid, &dwSid);
  ::swprintf_s(szBuf, MAX_PATH, _T("%s_%u"), szAppInstanceName, dwSid);

  hMutex = ::OpenMutex(MUTEX_ALL_ACCESS, FALSE, szBuf);
  if (NULL == hMutex && ERROR_FILE_NOT_FOUND == ::GetLastError()) {
    return FALSE;
  }

  if (hMutex) {
    CloseHandle(hMutex);
    hMutex = NULL;
  }

  return TRUE;
}

static BOOL PostMessageToClient(uint32_t wmMsg) {
  HWND hWnd = FindWindow(CLIENT_CLASS_NAME, NULL);
  if (hWnd == NULL) {
    return FALSE;
  }
  PostMessage(hWnd, wmMsg, 0, 0);
  return TRUE;
}

local_onestore::local_onestore(COnestoreCodeDlgDemoDlg* pdlg) {
  m_pDlg = pdlg;
  g_pDlg = pdlg;
  m_bExitCurCon = false;
  RegistProtocol();
  m_ProcMsgQueueMgr.CreateNewHostById(msgqueuedefaultserverid, this);
}

local_onestore::~local_onestore() {
  m_bExitCurCon = true;
  UnRegistProtocol();
}

void local_onestore::RegistProtocol() {
  m_RegMgr.Register(CRegHelperT<OST_ServiceInfo_Pro<local_onestore>>(), true);
  m_RegMgr.Register(CRegHelperT<OST_auth_token_Pro<local_onestore>>(), true);
  m_RegMgr.Register(CRegHelperT<OST_client_id_Pro<local_onestore>>(), true);
  m_RegMgr.Register(CRegHelperT<OST_update_seedapp_Pro<local_onestore>>(),
                    true);
  m_RegMgr.Register(CRegHelperT<OST_product_detail_Pro<local_onestore>>(),
                    true);
  m_RegMgr.Register(CRegHelperT<OST_show_error_Pro<local_onestore>>(), true);
  m_RegMgr.Register(CRegHelperT<OST_show_login_view_Pro<local_onestore>>(),
                    true);
  m_RegMgr.Register(CRegHelperT<OST_notify_auth_Pro<local_onestore>>(), true);
  m_RegMgr.Register(CRegHelperT<OST_bye_gameloop_Pro<local_onestore>>(), true);
}

void local_onestore::UnRegistProtocol() {
  m_RegMgr.UnRegister(OST_ServiceInfo_Pro<local_onestore>::GetRegItemName(),
                      OST_ServiceInfo_Pro<local_onestore>::GetRegType());
  m_RegMgr.UnRegister(OST_auth_token_Pro<local_onestore>::GetRegItemName(),
                      OST_auth_token_Pro<local_onestore>::GetRegType());
  m_RegMgr.UnRegister(OST_client_id_Pro<local_onestore>::GetRegItemName(),
                      OST_client_id_Pro<local_onestore>::GetRegType());
  m_RegMgr.UnRegister(OST_update_seedapp_Pro<local_onestore>::GetRegItemName(),
                      OST_update_seedapp_Pro<local_onestore>::GetRegType());
  m_RegMgr.UnRegister(OST_product_detail_Pro<local_onestore>::GetRegItemName(),
                      OST_product_detail_Pro<local_onestore>::GetRegType());
  m_RegMgr.UnRegister(OST_show_error_Pro<local_onestore>::GetRegItemName(),
                      OST_show_error_Pro<local_onestore>::GetRegType());
  m_RegMgr.UnRegister(OST_show_login_view_Pro<local_onestore>::GetRegItemName(),
                      OST_show_login_view_Pro<local_onestore>::GetRegType());
  m_RegMgr.UnRegister(OST_notify_auth_Pro<local_onestore>::GetRegItemName(),
                      OST_notify_auth_Pro<local_onestore>::GetRegType());
  m_RegMgr.UnRegister(OST_bye_gameloop_Pro<local_onestore>::GetRegItemName(),
                      OST_bye_gameloop_Pro<local_onestore>::GetRegType());
}

void local_onestore::on_connected(LPCSTR szBuff, int len) {
  m_bExitCurCon = false;
  ProcessOnestoreData(szBuff, len);
}

bool local_onestore::write(LPCSTR szbuff, int len)
{
  // write back
  PMsgInternal *pProtocolMsg = (PMsgInternal *)szbuff;

  m_pDlg->writeSendReceive(std::string(&pProtocolMsg->buff[0], pProtocolMsg->len));
  return true;
}

void local_onestore::ProcessOnestoreData(LPCSTR szBuff, int iLen) {
  do {
    PMsgInternal *pProtocolMsg =
        reinterpret_cast<PMsgInternal *>(const_cast<LPSTR>(szBuff));
    if (!pProtocolMsg || pProtocolMsg->len <= 0 ||
        pProtocolMsg->len > iLen - 8 || iLen <= 8) {
      break;
    }

    __IOSTPROTOCOL<local_onestore>::IOSTPROTOCOL pProtocolHandler = NULL;
    m_RegMgr.CreateRegObj(reinterpret_cast<PVOID *>(&pProtocolHandler), NULL,
                          pProtocolMsg->type);
    if (!pProtocolHandler) {
      //LOG_ERROR_PRINTF(_T("%S:CRegMgr CreateRegObj failed, msgType:%d"),
      //                 __FUNCTION__, (int)pProtocolMsg->type);
      break;
    }

    pProtocolHandler->set_local_device_ptr(this);
    pProtocolHandler->parse_request(
        std::string(&pProtocolMsg->buff[0], pProtocolMsg->len));
    pProtocolHandler->Invoke_doWork();
    SAFEDELEPTR(pProtocolHandler);
  } while (FALSE);
}

BOOL local_onestore::get_service_info(const OST_Request_JSDataBase &req,
                                      service_info_reply &reply) {
  reply.service_name = "MyService";
  reply.emulator_name = "onestore server";

  std::wstring strVer = L"2.65.68.4";
  reply.emulator_version = CW2A(strVer.c_str(), CP_ACP);

  return TRUE;
}

BOOL local_onestore::get_auth_token(const OST_Request_JSDataBase &req,
                                    auth_token_reply &reply) {
  if (IsAppInstanceExist(ONESTORECLINE_MUTEX_NAME) &&
    PostMessageToClient(WNDMSG_ONESTORE_AUTHTOKEN)) {
    return TRUE;
  }
  return StartAppMarket(L"-auth_token");
}

BOOL local_onestore::get_client_id(const OST_Request_JSDataBase &req,
                                   client_id_reply &reply) {
  std::string strClientId = "clienidW214we2dsfdg234dw";
  reply.client_id = strClientId;
  return TRUE;
}

BOOL local_onestore::update_seedapp(const update_seedapp_ReqData &req,
                                    update_seedapp_reply &reply) {
  BOOL bRet = FALSE;
  // 这条协议暂时没处理
  return bRet;
}

BOOL local_onestore::show_product_detail(const product_detail_ReqData &req,
                                         product_detail_reply &reply) {

  CString strProduct = m_pDlg->getdlgproductdetail();
  if (IsAppInstanceExist(ONESTORECLINE_MUTEX_NAME)) {
    HWND hWnd = FindWindow(CLIENT_CLASS_NAME, NULL);
    if (hWnd != NULL) {
      struct {
        WCHAR cls_type[2];
        WCHAR cls_value[MAX_PATH];
      } msg = {{0}, {0}};
      memcpy(msg.cls_type, CA2W(req.req_cls_type.c_str()), 1 * sizeof(WCHAR));
      memcpy(msg.cls_value, strProduct, strProduct.GetLength() * sizeof(WCHAR));
      COPYDATASTRUCT copyData = {0};
      copyData.cbData = sizeof(msg);
      copyData.lpData = &msg;
      copyData.dwData = WNDMSG_ONESTORE_SHOWPRODETAIL;
      DWORD dwSendResult = 0;

      SendMessageTimeout((HWND)hWnd, WM_COPYDATA, NULL, (LPARAM)&copyData,
                         SMTO_ABORTIFHUNG, 5000, &dwSendResult);
      return TRUE;
    }
  }
  std::wstring strParam = L" -show_product_detail ";
  strParam += CA2W(req.req_cls_type.c_str());
  strParam += L" ";
  if (strProduct.IsEmpty()) {
    strParam += CA2W("com.gaa.iap.sample6");
  } else {
    strParam += strProduct;
  }
  return StartAppMarket(strParam);
}

BOOL local_onestore::show_error_dialog(const show_error_ReqData &req,
                                       show_error_reply &reply) {
  BOOL bRet = FALSE;
  do {
    MessageBoxA(NULL, req.message.c_str(), req.title.c_str(), NULL);
    bRet = TRUE;
  } while (FALSE);
  return bRet;
}

BOOL local_onestore::show_login_view(const OST_Request_JSDataBase &req,
                                     show_login_view_reply &reply) {
  if (IsAppInstanceExist(ONESTORECLINE_MUTEX_NAME) &&
      PostMessageToClient(WNDMSG_ONESTORE_SHOWLOGINVIEW)) {
    return TRUE;
  }
  return StartAppMarket(L"-show_login_view");
}

BOOL local_onestore::bye_gameloop(const OST_Request_JSDataBase &req,
                                  bye_gameloop_reply &reply) {
  // 退出当前连接
  m_bExitCurCon = true;
  return TRUE;
}

void local_onestore::send_notify_auth_cmd(bool bLogin) {
  __IOSTPROTOCOL<local_onestore>::IOSTPROTOCOL pProtocolHandler = NULL;
  do {
    m_RegMgr.CreateRegObj((void **)&pProtocolHandler, NULL,
                          EmProtocolMsgType::notify_auth);
    if (!pProtocolHandler) {
      //LOG_ERROR_PRINTF(_T("%S:CRegMgr CreateRegObj failed, msgType:%d"),
      //                 __FUNCTION__, EmProtocolMsgType::notify_auth);
      break;
    }

    pProtocolHandler->set_local_device_ptr(this);
    OST_notify_auth_Pro<local_onestore> *pNotifyAuthPro =
        dynamic_cast<OST_notify_auth_Pro<local_onestore> *>(pProtocolHandler);
    if (!pNotifyAuthPro) {
      break;
    }
    notify_auth_reply replyData = pNotifyAuthPro->ConstructDefaultReplyData();
    replyData.result_code = bLogin ? OSTSUCCESSCODE : OSTFAILCODE;
    pNotifyAuthPro->RedefineReplyData(replyData);
    pProtocolHandler->Invoke_replyJSon();
  } while (FALSE);
  SAFEDELEPTR(pProtocolHandler);
}

void local_onestore::HandleMsgQueue(ByteBuffer &bytebuffer, DWORD dwMsgType,
                                    DWORD clientId) {
  __IOSTPROTOCOL<local_onestore>::IOSTPROTOCOL pProtocolHandler = NULL;
  m_RegMgr.CreateRegObj((void **)&pProtocolHandler, NULL, dwMsgType);
  if (!pProtocolHandler) {
    //LOG_ERROR_PRINTF(_T("%S:CRegMgr CreateRegObj failed, msgType:%d"),
    //                 __FUNCTION__, dwMsgType);
    return;
  }
  pProtocolHandler->set_local_device_ptr(this);

  if (dwMsgType == EmProtocolMsgType::auth_token) {
    std::string strOneStoreAge, strOneStoreToken;
    bytebuffer >> strOneStoreToken;
    bytebuffer >> strOneStoreAge;

    do {
      OST_auth_token_Pro<local_onestore> *pAuthTokenPro =
          dynamic_cast<OST_auth_token_Pro<local_onestore> *>(pProtocolHandler);
      if (!pAuthTokenPro) {
        break;
      }
      auth_token_reply replyData = pAuthTokenPro->ConstructDefaultReplyData();
      replyData.result_code =
          !strOneStoreToken.empty() ? OSTSUCCESSCODE : OSTFAILCODE;
      replyData.auth_token = strOneStoreToken;
      pAuthTokenPro->RedefineReplyData(replyData);
      pAuthTokenPro->OST_Protocol_Base::Invoke_replyJSon();
    } while (FALSE);
  } else if (dwMsgType == EmProtocolMsgType::show_login_view) {
    do {
      OST_show_login_view_Pro<local_onestore> *pShowLoginViewPro =
          dynamic_cast<OST_show_login_view_Pro<local_onestore> *>(
              pProtocolHandler);
      if (!pShowLoginViewPro) {
        break;
      }
      show_login_view_reply replyData =
          pShowLoginViewPro->ConstructDefaultReplyData();
      pShowLoginViewPro->RedefineReplyData(replyData);
      pShowLoginViewPro->OST_Protocol_Base::Invoke_replyJSon();
    } while (FALSE);
  } else if (dwMsgType == EmProtocolMsgType::notify_auth) {
    uint32_t uiLogin;
    bytebuffer >> uiLogin;
    do {
      OST_notify_auth_Pro<local_onestore> *pNotifyAuthPro =
          dynamic_cast<OST_notify_auth_Pro<local_onestore> *>(pProtocolHandler);
      if (!pNotifyAuthPro) {
        break;
      }
      notify_auth_reply replyData = pNotifyAuthPro->ConstructDefaultReplyData();
      replyData.result_code = uiLogin ? OSTSUCCESSCODE : OSTFAILCODE;
      pNotifyAuthPro->RedefineReplyData(replyData);
      pNotifyAuthPro->OST_Protocol_Base::Invoke_replyJSon();
    } while (FALSE);
  } else if (dwMsgType == EmProtocolMsgType::product_detail) {
    do {
      OST_product_detail_Pro<local_onestore> *pProductDetailPro =
          dynamic_cast<OST_product_detail_Pro<local_onestore> *>(
              pProtocolHandler);
      if (!pProductDetailPro) {
        break;
      }
      product_detail_reply replyData =
          pProductDetailPro->ConstructDefaultReplyData();
      pProductDetailPro->RedefineReplyData(replyData);
      pProductDetailPro->OST_Protocol_Base::Invoke_replyJSon();
    } while (FALSE);
  }
  SAFEDELEPTR(pProtocolHandler);
}
