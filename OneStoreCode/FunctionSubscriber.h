//-------------------------------------------------------
// Copyright (c) tencent lnc
// All rights reserved.
//
// File Name: FunctionSubscriber.h
// File Des: 定义观察者模式的两种注册方式，全局函数+成员函数
// File Summary:
// 使用方式：
// 1. FunctorDelegateBase<int(int)>* pNormalFunc1 =
// FunctorSubscriber(&normalfun1).Clone();
//    pNormalFunc1->operator()(5);   delete pNormalFunc1;
// 2. FunctorDelegateBase<int()>* pMemberFunc0 =
// FunctorSubscriber(&CDemo::memberfun0, this).Clone();
//    pMemberFunc0->operator()();  delete pMemberFunc0;
// 3.  FunctorDelegateBase<int(int)>& pMemberFunc0 =
//       FunctorSubscriber(&CDemo::memberfun1, this);
//     pMemberFunc0(3);
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron		2021-7-1	1.0
//--------------------------------------------------------
#pragma once

typedef void DefaultVoid;

enum _SLOTTYPE { SLOT_NORFUN, SLOT_MEMBERFUN };
template <typename Signature> class FunctorDelegateBase;

/// 偏特化 0个参数的函数
template <typename R> class FunctorDelegateBase<R()> {
public:
  virtual ~FunctorDelegateBase(){};
  virtual R operator()() = 0;
  virtual bool Equal(const FunctorDelegateBase &src) const = 0;
  virtual FunctorDelegateBase *Clone() const = 0;
  virtual UINT GetSlotType() const = 0;
  const FunctorDelegateBase *GetThisPtr() const { return this; }
};

/// 偏特化 1个参数的函数
template <typename R, class Param1> class FunctorDelegateBase<R(Param1)> {
public:
  virtual ~FunctorDelegateBase(){};
  virtual R operator()(Param1) = 0;
  virtual bool Equal(const FunctorDelegateBase &src) const = 0;
  virtual FunctorDelegateBase *Clone() const = 0;
  virtual UINT GetSlotType() const = 0;
  const FunctorDelegateBase<R(Param1)> *GetThisPtr() const { return this; }
};

/// 偏特化 2个参数的函数
template <typename R, class Param1, class Param2>
class FunctorDelegateBase<R(Param1, Param2)> {
public:
  virtual ~FunctorDelegateBase(){};
  virtual R operator()(Param1, Param2) = 0;
  virtual bool Equal(const FunctorDelegateBase &src) const = 0;
  virtual FunctorDelegateBase *Clone() const = 0;
  virtual UINT GetSlotType() const = 0;
  const FunctorDelegateBase<R(Param1, Param2)> *GetThisPtr() const { return this; }
};

/// 偏特化 3个参数的函数
template <typename R, class Param1, class Param2, class Param3>
class FunctorDelegateBase<R(Param1, Param2, Param3)> {
public:
  virtual ~FunctorDelegateBase(){};
  virtual R operator()(Param1, Param2, Param3) = 0;
  virtual bool Equal(const FunctorDelegateBase &src) const = 0;
  virtual FunctorDelegateBase *Clone() const = 0;
  virtual UINT GetSlotType() const = 0;
  const FunctorDelegateBase<R(Param1, Param2, Param3)> *GetThisPtr() const {
    return this;
  }
};

/// 偏特化 4个参数的函数
template <typename R, class Param1, class Param2, class Param3, class Param4>
class FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> {
public:
  virtual ~FunctorDelegateBase(){};
  virtual R operator()(Param1, Param2, Param3, Param4) = 0;
  virtual bool Equal(const FunctorDelegateBase &src) const = 0;
  virtual FunctorDelegateBase *Clone() const = 0;
  virtual UINT GetSlotType() const = 0;
  const FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> *GetThisPtr() const {
    return this;
  }
};

/// 偏特化 5个参数的函数
template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5>
class FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)> {
public:
  virtual ~FunctorDelegateBase(){};
  virtual R operator()(Param1, Param2, Param3, Param4, Param5) = 0;
  virtual bool Equal(const FunctorDelegateBase &src) const = 0;
  virtual FunctorDelegateBase *Clone() const = 0;
  virtual UINT GetSlotType() const = 0;
  const FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)> *
  GetThisPtr() const {
    return this;
  }
};

/// 偏特化 6个参数的函数
template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5, class Param6>
class FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5, Param6)> {
public:
  virtual ~FunctorDelegateBase(){};
  virtual R operator()(Param1, Param2, Param3, Param4, Param5, Param6) = 0;
  virtual bool Equal(const FunctorDelegateBase &src) const = 0;
  virtual FunctorDelegateBase *Clone() const = 0;
  virtual UINT GetSlotType() const = 0;
  const FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5, Param6)> *
  GetThisPtr() const {
    return this;
  }
};

/// 0 parameter normal function
template <typename R>
class FreeFunctionSlot0 : public FunctorDelegateBase<R()> {
public:
  typedef R(SlotFunction)();
  FreeFunctionSlot0(SlotFunction *func) : m_function(func) {}

  virtual R operator()() { return R(m_function()); }

  virtual FunctorDelegateBase<R()> *Clone() const {
    return new FreeFunctionSlot0(m_function);
  }

  virtual bool Equal(const FunctorDelegateBase<R()> &src) const {
    bool bRet = false;
    do {
      if (SLOT_NORFUN != src.GetSlotType()) {
        break;
      }

      const FreeFunctionSlot0 *psrc =
          static_cast<const FreeFunctionSlot0 *>(&src);
      bRet = (psrc->m_function == m_function);
    } while (false);
    return bRet;
  }

  virtual UINT GetSlotType() const { return SLOT_NORFUN; }

public:
  SlotFunction *m_function;
};

/// 0 parameter member function
template <typename R, class T>
class MemberFunctionSlot0 : public FunctorDelegateBase<R()> {
public:
  typedef R (T::*MemberFunctionType)();
  MemberFunctionSlot0(MemberFunctionType func, T *obj)
      : m_function(func), m_object(obj) {}

  virtual R operator()() { return R((m_object->*m_function)()); }

  virtual bool Equal(const FunctorDelegateBase<R()> &src) const {

    bool bRet = false;
    do {
      if (SLOT_MEMBERFUN != src.GetSlotType()) {
        break;
      }
      const MemberFunctionSlot0 *psrc =
          static_cast<const MemberFunctionSlot0 *>(&src);
      bRet = (psrc->m_function == m_function && psrc->m_object == m_object);
    } while (false);
    return bRet;
  }

  virtual FunctorDelegateBase<R()> *Clone() const {
    return new MemberFunctionSlot0(m_function, m_object);
  }

  virtual UINT GetSlotType() const { return SLOT_MEMBERFUN; }

protected:
  MemberFunctionType m_function;  // 成员函数
  T *m_object;                    // this指针
};

/// 1 parameter free function
template <typename R, class Param1>
class FreeFunctionSlot1 : public FunctorDelegateBase<R(Param1)> {
public:
  typedef R(SlotFunction)(Param1);
  FreeFunctionSlot1(SlotFunction *func) : m_function(func) {}

  virtual R operator()(Param1 param1) { return R(m_function(param1)); }

  virtual FunctorDelegateBase<R(Param1)> *Clone() const {
    return new FreeFunctionSlot1(m_function);
  }

  virtual bool Equal(const FunctorDelegateBase<R(Param1)> &src) const {
    bool bRet = false;
    do {
      if (SLOT_NORFUN != src.GetSlotType()) {
        break;
      }

      const FreeFunctionSlot1 *psrc =
          static_cast<const FreeFunctionSlot1 *>(&src);
      bRet = (psrc->m_function == m_function);
    } while (false);
    return bRet;
  }

  virtual UINT GetSlotType() const { return SLOT_NORFUN; }

public:
  SlotFunction *m_function;
};

/// 1 parameter member function
template <typename R, class T, class Param1>
class MemberFunctionSlot1 : public FunctorDelegateBase<R(Param1)> {
public:
  typedef R (T::*MemberFunctionType)(Param1);
  MemberFunctionSlot1(MemberFunctionType func, T *obj)
      : m_function(func), m_object(obj) {}

  virtual R operator()(Param1 param1) {
    return R((m_object->*m_function)(param1));
  }

  virtual bool Equal(const FunctorDelegateBase<R(Param1)> &src) const {
    bool bRet = false;
    do {
      if (SLOT_MEMBERFUN != src.GetSlotType()) {
        break;
      }
      const MemberFunctionSlot1 *psrc =
          static_cast<const MemberFunctionSlot1 *>(&src);
      bRet = (psrc->m_function == m_function && psrc->m_object == m_object);
    } while (false);
    return bRet;
  }

  virtual FunctorDelegateBase<R(Param1)> *Clone() const {
    return new MemberFunctionSlot1(m_function, m_object);
  }

  virtual UINT GetSlotType() const { return SLOT_MEMBERFUN; }

protected:
  MemberFunctionType m_function;  // 成员函数
  T *m_object;                    // this指针
};

/// 2 parameter mormal function
template <typename R, class Param1, class Param2>
class FreeFunctionSlot2 : public FunctorDelegateBase<R(Param1, Param2)> {
public:
  typedef R(SlotFunction)(Param1, Param2);
  FreeFunctionSlot2(SlotFunction *func) : m_function(func) {}

  virtual R operator()(Param1 param1, Param2 param2) {
    return R(m_function(param1, param2));
  }

  virtual FunctorDelegateBase<R(Param1, Param2)> *Clone() const {
    return new FreeFunctionSlot2(m_function);
  }

  virtual bool Equal(const FunctorDelegateBase<R(Param1, Param2)> &src) const {
    bool bRet = false;
    do {
      if (SLOT_NORFUN != src.GetSlotType()) {
        break;
      }

      const FreeFunctionSlot2 *psrc =
          static_cast<const FreeFunctionSlot2 *>(&src);
      bRet = (psrc->m_function == m_function);
    } while (false);
    return bRet;
  }

  virtual UINT GetSlotType() const { return SLOT_NORFUN; }

public:
  SlotFunction *m_function;
};

/// 2 parameter member function
template <typename R, class T, class Param1, class Param2>
class MemberFunctionSlot2 : public FunctorDelegateBase<R(Param1, Param2)> {
public:
  typedef R (T::*MemberFunctionType)(Param1, Param2);
  MemberFunctionSlot2(MemberFunctionType func, T *obj)
      : m_function(func), m_object(obj) {}

  virtual R operator()(Param1 param1, Param2 param2) {
    return R((m_object->*m_function)(param1, param2));
  }

  virtual bool Equal(const FunctorDelegateBase<R(Param1, Param2)> &src) const {

    bool bRet = false;
    do {
      if (SLOT_MEMBERFUN != src.GetSlotType()) {
        break;
      }
      const MemberFunctionSlot2 *psrc =
          static_cast<const MemberFunctionSlot2 *>(&src);
      bRet = (psrc->m_function == m_function && psrc->m_object == m_object);
    } while (false);
    return bRet;
  }

  virtual FunctorDelegateBase<R(Param1, Param2)> *Clone() const {
    return new MemberFunctionSlot2(m_function, m_object);
  }

  virtual UINT GetSlotType() const { return SLOT_MEMBERFUN; }

protected:
  MemberFunctionType m_function;  // 成员函数
  T *m_object;                    // this指针
};

/// 3 parameter mormal function
template <typename R, class Param1, class Param2, class Param3>
class FreeFunctionSlot3
    : public FunctorDelegateBase<R(Param1, Param2, Param3)> {
public:
  typedef R(SlotFunction)(Param1, Param2, Param3);
  FreeFunctionSlot3(SlotFunction *func) : m_function(func) {}

  virtual R operator()(Param1 param1, Param2 param2, Param3 param3) {
    return R(m_function(param1, param2, param3));
  }

  virtual FunctorDelegateBase<R(Param1, Param2, Param3)> *Clone() const {
    return new FreeFunctionSlot3(m_function);
  }

  virtual bool
  Equal(const FunctorDelegateBase<R(Param1, Param2, Param3)> &src) const {
    bool bRet = false;
    do {
      if (SLOT_NORFUN != src.GetSlotType()) {
        break;
      }

      const FreeFunctionSlot3 *psrc =
          static_cast<const FreeFunctionSlot3 *>(&src);
      bRet = (psrc->m_function == m_function);
    } while (false);
    return bRet;
  }

  virtual UINT GetSlotType() const { return SLOT_NORFUN; }

public:
  SlotFunction *m_function;
};

/// 3 parameter member function
template <typename R, class T, class Param1, class Param2, class Param3>
class MemberFunctionSlot3
    : public FunctorDelegateBase<R(Param1, Param2, Param3)> {
public:
  typedef R (T::*MemberFunctionType)(Param1, Param2, Param3);
  MemberFunctionSlot3(MemberFunctionType func, T *obj)
      : m_function(func), m_object(obj) {}

  virtual R operator()(Param1 param1, Param2 param2, Param3 param3) {
    return R((m_object->*m_function)(param1, param2, param3));
  }

  virtual bool
  Equal(const FunctorDelegateBase<R(Param1, Param2, Param3)> &src) const {

    bool bRet = false;
    do {
      if (SLOT_MEMBERFUN != src.GetSlotType()) {
        break;
      }
      const MemberFunctionSlot3 *psrc =
          static_cast<const MemberFunctionSlot3 *>(&src);
      bRet = (psrc->m_function == m_function && psrc->m_object == m_object);
    } while (false);
    return bRet;
  }

  virtual FunctorDelegateBase<R(Param1, Param2, Param3)> *Clone() const {
    return new MemberFunctionSlot3(m_function, m_object);
  }

  virtual UINT GetSlotType() const { return SLOT_MEMBERFUN; }

protected:
  MemberFunctionType m_function;  // 成员函数
  T *m_object;                    // this指针
};

/// 4 parameter mormal function
template <typename R, class Param1, class Param2, class Param3, class Param4>
class FreeFunctionSlot4
    : public FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> {
public:
  typedef R(SlotFunction)(Param1, Param2, Param3, Param4);
  FreeFunctionSlot4(SlotFunction *func) : m_function(func) {}

  virtual R operator()(Param1 param1, Param2 param2, Param3 param3,
                       Param4 param4) {
    return R(m_function(param1, param2, param3, param4));
  }

  virtual FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> *
  Clone() const {
    return new FreeFunctionSlot4(m_function);
  }

  virtual bool Equal(
      const FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> &src) const {
    bool bRet = false;
    do {
      if (SLOT_NORFUN != src.GetSlotType()) {
        break;
      }

      const FreeFunctionSlot4 *psrc =
          static_cast<const FreeFunctionSlot4 *>(&src);
      bRet = (psrc->m_function == m_function);
    } while (false);
    return bRet;
  }

  virtual UINT GetSlotType() const { return SLOT_NORFUN; }

public:
  SlotFunction *m_function;
};

/// 4 parameter member function
template <typename R, class T, class Param1, class Param2, class Param3,
          class Param4>
class MemberFunctionSlot4
    : public FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> {
public:
  typedef R (T::*MemberFunctionType)(Param1, Param2, Param3, Param4);
  MemberFunctionSlot4(MemberFunctionType func, T *obj)
      : m_function(func), m_object(obj) {}

  virtual R operator()(Param1 param1, Param2 param2, Param3 param3,
                       Param4 param4) {
    return R((m_object->*m_function)(param1, param2, param3, param4));
  }

  virtual bool Equal(
      const FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> &src) const {

    bool bRet = false;
    do {
      if (SLOT_MEMBERFUN != src.GetSlotType()) {
        break;
      }
      const MemberFunctionSlot4 *psrc =
          static_cast<const MemberFunctionSlot4 *>(&src);
      bRet = (psrc->m_function == m_function && psrc->m_object == m_object);
    } while (false);
    return bRet;
  }

  virtual FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> *
  Clone() const {
    return new MemberFunctionSlot4(m_function, m_object);
  }

  virtual UINT GetSlotType() const { return SLOT_MEMBERFUN; }

protected:
  MemberFunctionType m_function;  // 成员函数
  T *m_object;                    // this指针
};

/// 5 parameter mormal function
template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5>
class FreeFunctionSlot5
    : public FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)> {
public:
  typedef R(SlotFunction)(Param1, Param2, Param3, Param4, Param5);
  FreeFunctionSlot5(SlotFunction *func) : m_function(func) {}

  virtual R operator()(Param1 param1, Param2 param2, Param3 param3,
                       Param4 param4, Param5 param5) {
    return R(m_function(param1, param2, param3, param4, param5));
  }

  virtual FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)> *
  Clone() const {
    return new FreeFunctionSlot5(m_function);
  }

  virtual bool Equal(const FunctorDelegateBase<R(Param1, Param2, Param3, Param4,
                                                 Param5)> &src) const {
    bool bRet = false;
    do {
      if (SLOT_NORFUN != src.GetSlotType()) {
        break;
      }

      const FreeFunctionSlot5 *psrc =
          static_cast<const FreeFunctionSlot5 *>(&src);
      bRet = (psrc->m_function == m_function);
    } while (false);
    return bRet;
  }

  virtual UINT GetSlotType() const { return SLOT_NORFUN; }

public:
  SlotFunction *m_function;
};

/// 5 parameter member function
template <typename R, class T, class Param1, class Param2, class Param3,
          class Param4, class Param5>
class MemberFunctionSlot5
    : public FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)> {
public:
  typedef R (T::*MemberFunctionType)(Param1, Param2, Param3, Param4, Param5);
  MemberFunctionSlot5(MemberFunctionType func, T *obj)
      : m_function(func), m_object(obj) {}

  virtual R operator()(Param1 param1, Param2 param2, Param3 param3,
                       Param4 param4, Param5 param5) {
    return R((m_object->*m_function)(param1, param2, param3, param4, param5));
  }

  virtual bool Equal(const FunctorDelegateBase<R(Param1, Param2, Param3, Param4,
                                                 Param5)> &src) const {

    bool bRet = false;
    do {
      if (SLOT_MEMBERFUN != src.GetSlotType()) {
        break;
      }
      const MemberFunctionSlot5 *psrc =
          static_cast<const MemberFunctionSlot5 *>(&src);
      bRet = (psrc->m_function == m_function && psrc->m_object == m_object);
    } while (false);
    return bRet;
  }

  virtual FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)> *
  Clone() const {
    return new MemberFunctionSlot5(m_function, m_object);
  }

  virtual UINT GetSlotType() const { return SLOT_MEMBERFUN; }

protected:
  MemberFunctionType m_function;  // 成员函数
  T *m_object;                    // this指针
};

/// 6 parameter mormal function
template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5, class Param6>
class FreeFunctionSlot6 : public FunctorDelegateBase<R(
                              Param1, Param2, Param3, Param4, Param5, Param6)> {
public:
  typedef R(SlotFunction)(Param1, Param2, Param3, Param4, Param5, Param6);
  FreeFunctionSlot6(SlotFunction *func) : m_function(func) {}

  virtual R operator()(Param1 param1, Param2 param2, Param3 param3,
                       Param4 param4, Param5 param5, Param6 param6) {
    return R(m_function(param1, param2, param3, param4, param5, param6));
  }

  virtual FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5, Param6)>
      *Clone() const {
    return new FreeFunctionSlot6(m_function);
  }

  virtual bool Equal(const FunctorDelegateBase<R(Param1, Param2, Param3, Param4,
                                                 Param5, Param6)> &src) const {
    bool bRet = false;
    do {
      if (SLOT_NORFUN != src.GetSlotType()) {
        break;
      }

      const FreeFunctionSlot6 *psrc =
          static_cast<const FreeFunctionSlot6 *>(&src);
      bRet = (psrc->m_function == m_function);
    } while (false);
    return bRet;
  }

  virtual UINT GetSlotType() const { return SLOT_NORFUN; }

public:
  SlotFunction *m_function;
};

/// 6 parameter member function
template <typename R, class T, class Param1, class Param2, class Param3,
          class Param4, class Param5, class Param6>
class MemberFunctionSlot6
    : public FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5,
                                   Param6)> {
public:
  typedef R (T::*MemberFunctionType)(Param1, Param2, Param3, Param4, Param5,
                                     Param6);
  MemberFunctionSlot6(MemberFunctionType func, T *obj)
      : m_function(func), m_object(obj) {}

  virtual R operator()(Param1 param1, Param2 param2, Param3 param3,
                       Param4 param4, Param5 param5, Param6 param6) {
    return R((m_object->*m_function)(param1, param2, param3, param4, param5,
                                     param6));
  }

  virtual bool Equal(const FunctorDelegateBase<R(Param1, Param2, Param3, Param4,
                                                 Param5, Param6)> &src) const {

    bool bRet = false;
    do {
      if (SLOT_MEMBERFUN != src.GetSlotType()) {
        break;
      }
      const MemberFunctionSlot6 *psrc =
          static_cast<const MemberFunctionSlot6 *>(&src);
      bRet = (psrc->m_function == m_function && psrc->m_object == m_object);
    } while (false);
    return bRet;
  }

  virtual FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5, Param6)>
      *Clone() const {
    return new MemberFunctionSlot6(m_function, m_object);
  }

  virtual UINT GetSlotType() const { return SLOT_MEMBERFUN; }

protected:
  MemberFunctionType m_function;  // 成员函数
  T *m_object;                    // this指针
};

/// subscriber function 0 param
template <typename R, class T>
MemberFunctionSlot0<R, T> FunctorSubscriber(R (T::*pFn)(), T *pObject) {
  return MemberFunctionSlot0<R, T>(pFn, pObject);
}

template <typename R>
inline FreeFunctionSlot0<R> FunctorSubscriber(R (*pFn)()) {
  return FreeFunctionSlot0<R>(pFn);
}

/// subscriber function 1 param
template <typename R, class T, class Param1>
MemberFunctionSlot1<R, T, Param1> FunctorSubscriber(R (T::*pFn)(Param1),
                                                    T *pObject) {
  return MemberFunctionSlot1<R, T, Param1>(pFn, pObject);
}

template <typename R, class Param1>
inline FreeFunctionSlot1<R, Param1> FunctorSubscriber(R (*pFn)(Param1)) {
  return FreeFunctionSlot1<R, Param1>(pFn);
}

/// subscriber function 2 params
template <typename R, class T, class Param1, class Param2>
MemberFunctionSlot2<R, T, Param1, Param2>
FunctorSubscriber(R (T::*pFn)(Param1, Param2), T *pObject) {
  return MemberFunctionSlot2<R, T, Param1, Param2>(pFn, pObject);
}

template <typename R, class Param1, class Param2>
inline FreeFunctionSlot2<R, Param1, Param2>
FunctorSubscriber(R (*pFn)(Param1, Param2)) {
  return FreeFunctionSlot2<R, Param1, Param2>(pFn);
}

/// subscriber function 3 params
template <typename R, class T, class Param1, class Param2, class Param3>
MemberFunctionSlot3<R, T, Param1, Param2, Param3>
FunctorSubscriber(R (T::*pFn)(Param1, Param2, Param3), T *pObject) {
  return MemberFunctionSlot3<R, T, Param1, Param2, Param3>(pFn, pObject);
}

template <typename R, class Param1, class Param2, class Param3>
inline FreeFunctionSlot3<R, Param1, Param2, Param3>
FunctorSubscriber(R (*pFn)(Param1, Param2, Param3)) {
  return FreeFunctionSlot3<R, Param1, Param2, Param3>(pFn);
}

/// subscriber function 4 params
template <typename R, class T, class Param1, class Param2, class Param3,
          class Param4>
MemberFunctionSlot4<R, T, Param1, Param2, Param3, Param4>
FunctorSubscriber(R (T::*pFn)(Param1, Param2, Param3, Param4), T *pObject) {
  return MemberFunctionSlot4<R, T, Param1, Param2, Param3, Param4>(pFn,
                                                                   pObject);
}

template <typename R, class Param1, class Param2, class Param3, class Param4>
inline FreeFunctionSlot4<R, Param1, Param2, Param3, Param4>
FunctorSubscriber(R (*pFn)(Param1, Param2, Param3, Param4)) {
  return FreeFunctionSlot4<R, Param1, Param2, Param3, Param4>(pFn);
}

/// subscriber function 5 params
template <typename R, class T, class Param1, class Param2, class Param3,
          class Param4, class Param5>
MemberFunctionSlot5<R, T, Param1, Param2, Param3, Param4, Param5>
FunctorSubscriber(R (T::*pFn)(Param1, Param2, Param3, Param4, Param5),
                  T *pObject) {
  return MemberFunctionSlot5<R, T, Param1, Param2, Param3, Param4, Param5>(
      pFn, pObject);
}

template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5>
inline FreeFunctionSlot5<R, Param1, Param2, Param3, Param4, Param5>
FunctorSubscriber(R (*pFn)(Param1, Param2, Param3, Param4, Param5)) {
  return FreeFunctionSlot5<R, Param1, Param2, Param3, Param4, Param5>(pFn);
}

/// subscriber function 6 params
template <typename R, class T, class Param1, class Param2, class Param3,
          class Param4, class Param5, class Param6>
MemberFunctionSlot6<R, T, Param1, Param2, Param3, Param4, Param5, Param6>
FunctorSubscriber(R (T::*pFn)(Param1, Param2, Param3, Param4, Param5, Param6),
                  T *pObject) {
  return MemberFunctionSlot6<R, T, Param1, Param2, Param3, Param4, Param5,
                             Param6>(pFn, pObject);
}

template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5, class Param6>
inline FreeFunctionSlot6<R, Param1, Param2, Param3, Param4, Param5, Param6>
FunctorSubscriber(R (*pFn)(Param1, Param2, Param3, Param4, Param5, Param6)) {
  return FreeFunctionSlot6<R, Param1, Param2, Param3, Param4, Param5, Param6>(
      pFn);
}
