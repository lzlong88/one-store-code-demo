//-------------------------------------------------------
// Copyright (c) tencent
// All rights reserved.
//
// File Name: local_onestore_protocol.h
// File Des: 实现与onestore的通信协议 https://docs.qq.com/doc/DVkNRcnpYcURpdXFI
// File Summary:
// Cur Version: 1.0
// Author:
// Create Data:2021-5-20
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron		2021-5-22 	1.0
//-------------------------------------------------------
#ifndef AOW_ONESTORE_PROTOCOL_HEADER
#define AOW_ONESTORE_PROTOCOL_HEADER

#include "DeclareDefine.h"
#include "json_auto.hpp"
#include <string.h>


#define OST_DECLAREREG_CLASS(OBJ, Type)                                        \
  DECLARE_REG_CLASS(OBJ<TDevice>, Enum2WStr(Type),                             \
  static_cast<int>(Type))

#define OSTSUCCESSCODE "success"
#define OSTFAILCODE "fail"

enum EmProtocolMsgType {
  service_info = 0x00000010,
  auth_token = 0x00000011,
  client_id = 0x00000012,
  update_seedapp = 0x00000013,
  product_detail = 0x00000020,
  show_error = 0x00000021,
  show_login_view = 0x00000030,
  notify_auth = 0x00000031,
  bye_gameloop = 0x00000032,
};

typedef struct PMsgInternalTag {
  EmProtocolMsgType type;
  int32_t len;
  char buff[1];
} PMsgInternal, *PMsgInternalPtr;

template <class OutputClass, class InputClass> union horrible_union {
  OutputClass out;
  InputClass in;
};
template <class OutputClass, class InputClass>
inline OutputClass horrible_cast(const InputClass input) {
  horrible_union<OutputClass, InputClass> u;
  u.in = input;
  return u.out;
}

class GenericClass {};
template <typename Signature> class FunCallDelegate;

template <typename R, class Param1, class Param2>
class FunCallDelegate<R(Param1, Param2)> {
  typedef R (GenericClass::*pfnSlot)(Param1, Param2);

public:
  FunCallDelegate() {
    m_pThis = NULL;
    m_fn = NULL;
  }

  template <class Y> FunCallDelegate(const Y *pthis) {
    horrible_set_this(pthis);
  }

  template <class X, class Y>
  FunCallDelegate(R (X::*function_to_bind)(Param1 p1, Param2 p2),
                  const Y *pthis) {
    horrible_set_this(pthis);
    horrible_set_function(function_to_bind);
  }

  virtual ~FunCallDelegate() {
    m_pThis = NULL;
    m_fn = NULL;
  }

  template <class Y> void horrible_set_this(Y *pthis) {
    m_pThis = horrible_cast<GenericClass *, Y *>(pthis);
  }

  template <class X>
  void horrible_set_function(R (X::*function_to_bind)(Param1 p1, Param2 p2)) {
    m_fn = horrible_cast<pfnSlot, R (X::*)(Param1 p1, Param2 p2)>(
        function_to_bind);
  }

  bool IsFunCallable() const { return m_pThis && m_fn; }

  R operator()(Param1 p1, Param2 p2) const {
    if (!IsFunCallable())
      return R(0);
    return (m_pThis->*(m_fn))(p1, p2);
  }

  GenericClass *GetThisPtr() { return m_pThis; }
  pfnSlot GetpfnSlot() { return m_fn; }

private:
  GenericClass *m_pThis;
  pfnSlot m_fn;
};

// 协议接口基类 //////////////////////////////////////////////
template <typename TDevice> class __IOSTPROTOCOL {
public:
  typedef TDevice TDeviceType;
  typedef __IOSTPROTOCOL<TDevice> *IOSTPROTOCOL;

public:
  virtual void set_local_device_ptr(TDeviceType *pdevice) = 0;
  virtual BOOL parse_request(const std::string &strRequest) = 0;
  virtual BOOL Invoke_doWork() = 0;
  virtual BOOL Invoke_replyJSon() = 0;
  // 注意，一定要有虚析构函数，不然不会析构子类导致内存泄漏
  virtual ~__IOSTPROTOCOL() {};
};

// 协议实现基类 //////////////////////////////////////////////
template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
class OST_Protocol_Base
    : public __IOSTPROTOCOL<TDevice>,
      public FunCallDelegate<BOOL(const RequestJSObj &, ReplyJSObj &)> {
  typedef BOOL (TDevice::*pfnRequestJobSlot)(const RequestJSObj &,
                                             ReplyJSObj &);
  typedef typename __IOSTPROTOCOL::TDeviceType TDeviceType;

public:
  OST_Protocol_Base();
  OST_Protocol_Base(TDeviceType *pdevice);

  template <class X>
  OST_Protocol_Base(TDeviceType *pdevice,
                    BOOL (X::*function_to_bind)(const RequestJSObj &,
                                                ReplyJSObj &));

  virtual ~OST_Protocol_Base();

  void set_local_device_ptr(TDeviceType *pdevice) override;
  BOOL parse_request(const std::string &strRequest) override;
  BOOL Invoke_doWork() override;
  BOOL Invoke_replyJSon() override;

  virtual const ReplyJSObj &ConstructDefaultReplyData();
  virtual void RedefineReplyData(const ReplyJSObj &replyData);

protected:
  RequestJSObj m_RequestJSData;
  ReplyJSObj m_ReplyJSData;
  TDeviceType *m_pDevice;
};

// OST_Protocol_Base function implement
template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj, TDevice>::OST_Protocol_Base()
    : FunCallDelegate<BOOL(const RequestJSObj &, ReplyJSObj &)>(),
      m_pDevice(NULL) {}

template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj, TDevice>::OST_Protocol_Base(
    TDeviceType *pDevice)
    : FunCallDelegate<BOOL(const RequestJSObj &, ReplyJSObj &)>(pDevice),
      m_pDevice(pDevice) {}

template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
template <class X>
OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj, TDevice>::OST_Protocol_Base(
    TDeviceType *pDevice,
    BOOL (X::*function_to_bind)(const RequestJSObj &, ReplyJSObj &))
    : FunCallDelegate<BOOL(const RequestJSObj &, ReplyJSObj &)>(
          function_to_bind, pDevice),
      m_pDevice(pDevice) {}

template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj,
                  TDevice>::~OST_Protocol_Base() {
  m_pDevice = NULL;
}

template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
void OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj,
                       TDevice>::set_local_device_ptr(TDeviceType *pdevice) {
  FunCallDelegate::horrible_set_this(pdevice);
  m_pDevice = pdevice;
}

template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
BOOL OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj, TDevice>::parse_request(
    const std::string &strRequest) {
  return m_RequestJSData.FromRequestStr(strRequest);
}

template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
const ReplyJSObj &OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj,
                                    TDevice>::ConstructDefaultReplyData() {
  m_ReplyJSData.res_type = CW2A(TObj::GetRegItemName(), CP_ACP);
  m_ReplyJSData.result_code = OSTSUCCESSCODE;
  return m_ReplyJSData;
}

template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
BOOL OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj,
                       TDevice>::Invoke_doWork() {
  // reply 初始化默认的数据
  ConstructDefaultReplyData();
  if (FunCallDelegate::IsFunCallable()) {
    if (!FunCallDelegate::operator()(m_RequestJSData, m_ReplyJSData)) {
      m_ReplyJSData.result_code = OSTFAILCODE;
    }
  }
  return Invoke_replyJSon();
}

template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
BOOL OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj,
                       TDevice>::Invoke_replyJSon() {
  BOOL bRet = FALSE;
  do {
    if (!m_pDevice) {
      break;
    }
    std::string strJsonText;
    if (!m_ReplyJSData.ConvertJsonObjToStr(strJsonText)) {
      break;
    }

    std::string strSendText;
    EmProtocolMsgType MsgType =
        static_cast<EmProtocolMsgType>(TObj::GetRegType());
    int32_t len = strJsonText.length();
    strSendText.append((const char *)(&MsgType), sizeof(EmProtocolMsgType));
    strSendText.append((const char *)(&len), sizeof(int32_t));
    strSendText.append((const char *)(strJsonText.c_str()),
                       strJsonText.length());

    m_pDevice->write(strSendText.c_str(), strSendText.length());
    bRet = TRUE;
  } while (FALSE);
  return bRet;
}

template <typename TObj, typename RequestJSObj, typename ReplyJSObj,
          typename TDevice>
void OST_Protocol_Base<TObj, RequestJSObj, ReplyJSObj, TDevice>::
    RedefineReplyData(const ReplyJSObj &replyData) {
  m_ReplyJSData = replyData;
}

// JSonObjToStrBase JSon转String基类
// //////////////////////////////////////////////
template <class T> class JSonObjToStrBase {
public:
  BOOL ConvertJsonObjToStr(std::string &strOut) {
    BOOL bRet = TRUE;
    try {
      T *This = static_cast<T *>(this);
      nlohmann::json jsonObj = *This;
      strOut = jsonObj.dump();
    } catch (const std::exception &/*err*/) {
      //LOG_ERROR_PRINTF(
      //    _T("%S: convert json obj to string catch except\n error:%s"),
      //    __FUNCTION__, CA2W(err.what(), CP_ACP));
      bRet = FALSE;
    }
    return bRet;
  }
};

// OST_Request_JSDataBase JSon请求基类
// //////////////////////////////////////////////
class OST_Request_JSDataBase {
public:
  virtual BOOL FromRequestStr(const std::string &strRequest);
  virtual void AssignJsonVal(const nlohmann::json &jsonObj);

public:
  std::string req_type;
};
JSON_AUTO(OST_Request_JSDataBase, req_type);

// OST_Request_JSDataBase JSon请求扩展基类
// ///////////////////////////////////////////
template <class T>
class OST_Request_JSData_Derive : public OST_Request_JSDataBase {
public:
  void AssignJsonVal(const nlohmann::json &jsonObj) override {
    T *This = static_cast<T *>(this);
    *This = jsonObj;
  }
};

// service_info 协议 //////////////////////////////////////////////
class service_info_reply : public JSonObjToStrBase<service_info_reply> {
public:
  std::string res_type;       // "service_info"
  std::string result_code;    // "success" or "fail"
  std::string service_name;   // loop service name. "GameLoop" or Other String.
  std::string emulator_name;  // emulator   name
  std::string emulator_version;  // emulator version
};
JSON_AUTO(service_info_reply, res_type, result_code, service_name,
          emulator_name, emulator_version);

template <typename TDevice>
class OST_ServiceInfo_Pro
    : public OST_Protocol_Base<OST_ServiceInfo_Pro<TDevice>,
                               OST_Request_JSDataBase, service_info_reply,
                               TDevice> {
  OST_DECLAREREG_CLASS(OST_ServiceInfo_Pro, service_info);
  typedef OST_Protocol_Base<OST_ServiceInfo_Pro<TDevice>,
                            OST_Request_JSDataBase, service_info_reply, TDevice>
      __baseClass;

public:
  OST_ServiceInfo_Pro() : __baseClass(NULL, &TDevice::get_service_info) {}
  OST_ServiceInfo_Pro(TDevice *pLocalDevice)
      : __baseClass(pLocalDevice, &TDevice::get_service_info) {}
};

// onestore_auth_token 协议 /////////////////////////////////////////
class auth_token_reply : public JSonObjToStrBase<auth_token_reply> {
public:
  std::string res_type;     // "auth_token"
  std::string result_code;  // "success" or "fail"
  std::string auth_token;   // "onestore_auth_token"
};
JSON_AUTO(auth_token_reply, res_type, result_code, auth_token);

template <typename TDevice>
class OST_auth_token_Pro : public OST_Protocol_Base<OST_auth_token_Pro<TDevice>,
                                                    OST_Request_JSDataBase,
                                                    auth_token_reply, TDevice> {
  OST_DECLAREREG_CLASS(OST_auth_token_Pro, auth_token);
  typedef OST_Protocol_Base<OST_auth_token_Pro<TDevice>, OST_Request_JSDataBase,
                            auth_token_reply, TDevice>
      __baseClass;

public:
  OST_auth_token_Pro() : __baseClass(NULL, &TDevice::get_auth_token) {}
  OST_auth_token_Pro(TDevice *pLocalDevice)
      : __baseClass(pLocalDevice, &TDevice::get_auth_token) {}
  // 屏蔽json默认回复功能 等待助手回复
  BOOL Invoke_replyJSon() override { return TRUE; }
};

// client_id 协议 /////////////////////////////////////////
class client_id_reply : public JSonObjToStrBase<client_id_reply> {
public:
  std::string res_type;     // "client_id"
  std::string result_code;  // "success" or "fail"
  std::string client_id;    // client_id
};
JSON_AUTO(client_id_reply, res_type, result_code, client_id);

template <typename TDevice>
class OST_client_id_Pro : public OST_Protocol_Base<OST_client_id_Pro<TDevice>,
                                                   OST_Request_JSDataBase,
                                                   client_id_reply, TDevice> {
  OST_DECLAREREG_CLASS(OST_client_id_Pro, client_id);
  typedef OST_Protocol_Base<OST_client_id_Pro<TDevice>, OST_Request_JSDataBase,
                            client_id_reply, TDevice>
      __baseClass;

public:
  OST_client_id_Pro() : __baseClass(NULL, &TDevice::get_client_id) {}
  OST_client_id_Pro(TDevice *pLocalDevice)
      : __baseClass(pLocalDevice, &TDevice::get_client_id) {}
};

// update_seedapp 协议 /////////////////////////////////////////
class update_seedapp_ReqData
    : public OST_Request_JSData_Derive<update_seedapp_ReqData> {
public:
  std::string
      req_cls_type;  // "1": It means that "req_cls_value" has a "product id"
                     // "2" : It means that "req_cls_value" has a "package name".
                     // In   this case "req_cls_type" is always "2"
  std::string
      req_cls_value;  // product   id or package name.
                      // In   this case "req_cls_value" is always "package name".
};
JSON_AUTO(update_seedapp_ReqData, req_type, req_cls_type, req_cls_value);

class update_seedapp_reply : public JSonObjToStrBase<update_seedapp_reply> {
public:
  std::string res_type;     // "update_seedapp"
  std::string result_code;  // "success" or "fail"
};
JSON_AUTO(update_seedapp_reply, res_type, result_code);

template <typename TDevice>
class OST_update_seedapp_Pro
    : public OST_Protocol_Base<OST_update_seedapp_Pro<TDevice>,
                               update_seedapp_ReqData, update_seedapp_reply,
                               TDevice> {
  OST_DECLAREREG_CLASS(OST_update_seedapp_Pro, update_seedapp);
  typedef OST_Protocol_Base<OST_update_seedapp_Pro<TDevice>,
                            update_seedapp_ReqData, update_seedapp_reply,
                            TDevice>
      __baseClass;

public:
  OST_update_seedapp_Pro() : __baseClass(NULL, &TDevice::update_seedapp) {}
  OST_update_seedapp_Pro(TDevice *pLocalDevice)
      : __baseClass(pLocalDevice, &TDevice::update_seedapp) {}
};

// product_detail 协议 /////////////////////////////////////////
class product_detail_ReqData
    : public OST_Request_JSData_Derive<product_detail_ReqData> {
public:
  std::string
      req_cls_type;  // "1":  It means that "req_cls_value" has a "product id".
                     // "2":  It means that "req_cls_value" has a "package name".
  std::string req_cls_value;  // product id or package name
};
JSON_AUTO(product_detail_ReqData, req_type, req_cls_type, req_cls_value);

class product_detail_reply : public JSonObjToStrBase<product_detail_reply> {
public:
  std::string res_type;     // "product_detail"
  std::string result_code;  // "success" or "fail"
};
JSON_AUTO(product_detail_reply, res_type, result_code);

template <typename TDevice>
class OST_product_detail_Pro
    : public OST_Protocol_Base<OST_product_detail_Pro<TDevice>,
                               product_detail_ReqData, product_detail_reply,
                               TDevice> {
  OST_DECLAREREG_CLASS(OST_product_detail_Pro, product_detail);
  typedef OST_Protocol_Base<OST_product_detail_Pro<TDevice>,
                            product_detail_ReqData, product_detail_reply,
                            TDevice>
      __baseClass;

public:
  OST_product_detail_Pro() : __baseClass(NULL, &TDevice::show_product_detail) {}
  OST_product_detail_Pro(TDevice *pLocalDevice)
      : __baseClass(pLocalDevice, &TDevice::show_product_detail) {}
  // 屏蔽json默认回复功能 等待助手回复
  BOOL Invoke_replyJSon() override { return TRUE; }
};

// show_error 协议 /////////////////////////////////////////
class show_error_ReqData
    : public OST_Request_JSData_Derive<show_error_ReqData> {
public:
  std::string title;    // Title   of a popup dialog.
  std::string message;  // Message   of a popup dialog.
};
JSON_AUTO(show_error_ReqData, req_type, title, message);

class show_error_reply : public JSonObjToStrBase<show_error_reply> {
public:
  std::string res_type;     // "show_error"
  std::string result_code;  // "success" or "fail"
};
JSON_AUTO(show_error_reply, res_type, result_code);

template <typename TDevice>
class OST_show_error_Pro
    : public OST_Protocol_Base<OST_show_error_Pro<TDevice>, show_error_ReqData,
                               show_error_reply, TDevice> {
  OST_DECLAREREG_CLASS(OST_show_error_Pro, show_error);
  typedef OST_Protocol_Base<OST_show_error_Pro<TDevice>, show_error_ReqData,
                            show_error_reply, TDevice>
      __baseClass;

public:
  OST_show_error_Pro() : __baseClass(NULL, &TDevice::show_error_dialog) {}
  OST_show_error_Pro(TDevice *pLocalDevice)
      : __baseClass(pLocalDevice, &TDevice::show_error_dialog) {}
};

// show_login_view 协议 /////////////////////////////////////////
class show_login_view_reply : public JSonObjToStrBase<show_login_view_reply> {
public:
  std::string res_type;     // "show_login_view"
  std::string result_code;  // "success" or "fail"
};
JSON_AUTO(show_login_view_reply, res_type, result_code);

template <typename TDevice>
class OST_show_login_view_Pro
    : public OST_Protocol_Base<OST_show_login_view_Pro<TDevice>,
                               OST_Request_JSDataBase, show_login_view_reply,
                               TDevice> {
  OST_DECLAREREG_CLASS(OST_show_login_view_Pro, show_login_view);
  typedef OST_Protocol_Base<OST_show_login_view_Pro<TDevice>,
                            OST_Request_JSDataBase, show_login_view_reply,
                            TDevice>
      __baseClass;

public:
  OST_show_login_view_Pro() : __baseClass(NULL, &TDevice::show_login_view) {}
  OST_show_login_view_Pro(TDevice *pLocalDevice)
      : __baseClass(pLocalDevice, &TDevice::show_login_view) {}
  // 屏蔽json默认回复功能 等待助手回复
  BOOL Invoke_replyJSon() override { return TRUE; }
};

// notify_auth 协议 /////////////////////////////////////////
class notify_auth_reply : public JSonObjToStrBase<notify_auth_reply> {
public:
  std::string res_type;     // "notify_auth"
  std::string result_code;  // "success" or "fail"
};
JSON_AUTO(notify_auth_reply, res_type, result_code);

template <typename TDevice>
class OST_notify_auth_Pro
    : public OST_Protocol_Base<OST_notify_auth_Pro<TDevice>,
                               OST_Request_JSDataBase, notify_auth_reply,
                               TDevice> {
  OST_DECLAREREG_CLASS(OST_notify_auth_Pro, notify_auth);
  typedef OST_Protocol_Base<OST_notify_auth_Pro<TDevice>,
                            OST_Request_JSDataBase, notify_auth_reply, TDevice>
      __baseClass;

public:
  OST_notify_auth_Pro() : __baseClass(NULL) {}
  OST_notify_auth_Pro(TDevice *pLocalDevice) : __baseClass(pLocalDevice) {}
};

// bye_gameloop 协议 /////////////////////////////////////////
class bye_gameloop_reply : public JSonObjToStrBase<bye_gameloop_reply> {
public:
  std::string res_type;     // "bye_gameloop"
  std::string result_code;  // "success" or "fail"
};
JSON_AUTO(bye_gameloop_reply, res_type, result_code);

template <typename TDevice>
class OST_bye_gameloop_Pro
    : public OST_Protocol_Base<OST_bye_gameloop_Pro<TDevice>,
                               OST_Request_JSDataBase, bye_gameloop_reply,
                               TDevice> {
  OST_DECLAREREG_CLASS(OST_bye_gameloop_Pro, bye_gameloop);
  typedef OST_Protocol_Base<OST_bye_gameloop_Pro<TDevice>,
                            OST_Request_JSDataBase, bye_gameloop_reply, TDevice>
      __baseClass;

public:
  OST_bye_gameloop_Pro() : __baseClass(NULL, &TDevice::bye_gameloop) {}
  OST_bye_gameloop_Pro(TDevice *pLocalDevice)
      : __baseClass(pLocalDevice, &TDevice::bye_gameloop) {}
};

// AOW_ONESTORE_PROTOCOL_HEADER
#endif
