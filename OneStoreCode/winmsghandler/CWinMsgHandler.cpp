#include "stdafx.h"
#include "CWinMsgHandler.h"
#include "../DeclareDefine.h"

CWinMsgHandlerItem::CWinMsgHandlerItem() : m_uiMsgId(0) {}

CWinMsgHandlerItem::~CWinMsgHandlerItem() { RemoveAll(); }

void CWinMsgHandlerItem::PreVecObjRemove(const MsgFunctorSlotPtr &obj) {
  if (obj) {
    delete obj;
  }
}

bool CWinMsgHandlerItem::EqualVecObj(const MsgFunctorSlotPtr &objsrc,
                                     const MsgFunctorSlotPtr &objdest) const {
  return objdest->Equal(*objsrc);
}

VOID CWinMsgHandlerItem::SetRegMsgId(UINT uiMsgId) { m_uiMsgId = uiMsgId; }

UINT CWinMsgHandlerItem::GetRegMsgId() { return m_uiMsgId; }

CWinMsgHandlerMgr::CWinMsgHandlerMgr() {}

CWinMsgHandlerMgr::~CWinMsgHandlerMgr() { RemoveAll(); }
void CWinMsgHandlerMgr::PreMapKeyRemove(const CWinMsgHandlerItemPtr &obj) {
  if (obj) {
    delete obj;
  }
}

int CWinMsgHandlerMgr::RegisterMsg(UINT uiMsg,
                                   const MsgFunctorSlotPtr &FunctorSlot) {
  int iRet = -1;
  do {
    CWinMsgHandlerItemPtr pMsgHandlerItem = NULL;
    if (!GetObjByKey(uiMsg, pMsgHandlerItem)) {
      // 不存在
      pMsgHandlerItem = new CWinMsgHandlerItem;
      pMsgHandlerItem->SetRegMsgId(uiMsg);
      AddKey(uiMsg, pMsgHandlerItem);
    }

    int iElement = pMsgHandlerItem->FindObj(FunctorSlot);
    if (-1 != iElement) {
      // 已经存在
      iRet = -2;
      break;
    }

    if (pMsgHandlerItem->AddObj(FunctorSlot)) {
      iRet = 0;
    }
  } while (FALSE);
  return iRet;
}

int CWinMsgHandlerMgr::UnRegisterMsg(UINT uiMsg,
                                     const MsgFunctorSlotPtr &FunctorSlot) {
  int iErr = -1;
  do {
    CWinMsgHandlerItemPtr pMsgHandlerItem = NULL;
    if (!GetObjByKey(uiMsg, pMsgHandlerItem)) {
      // 没有注册
      break;
    }

    int iElement = pMsgHandlerItem->FindObj(FunctorSlot);
    if (-1 != iElement) {
      pMsgHandlerItem->RemoveObj(iElement);
      iErr = 0;
      break;
    }
  } while (FALSE);
  if (FunctorSlot) {
    delete FunctorSlot;
  }
  return iErr;
}

LRESULT CWinMsgHandlerMgr::ProcessWinMsg(HWND hWnd, UINT message, WPARAM wParam,
                                         LPARAM lParam) {
  LRESULT hr = S_FALSE;
  do {
    CWinMsgHandlerItemPtr pMsgHandlerItem = NULL;
    if (!GetObjByKey(message, pMsgHandlerItem)) {
      // 不存在
      break;
    }

    int count = (int)pMsgHandlerItem->GetCount();
    for (int i = 0; i < count; i++) {
      hr =
          pMsgHandlerItem->GetObj(i)->operator()(hWnd, message, wParam, lParam);
    }
  } while (FALSE);
  return hr;
}
