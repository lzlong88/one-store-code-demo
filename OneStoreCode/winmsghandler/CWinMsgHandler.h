//-------------------------------------------------------
// Copyright (c) tencent
// All rights reserved.
//
// File Name: CWinMsgHandler.h
// File Des: windows消息命令监听函数绑定处理类 观察者模式
// File Summary:
// Cur Version: 1.0
// Author:zeron
// 用法：1.回调函数声明：LRESULT MsgHandleSlot(HWND hWnd, UINT uiMsg, WPARAM wParam,
//                                                       LPARAM lParam);
//
//      CWinMsgHandlerMgr m_winMsgHandlerMgr;
//      2.注册消息处理回调监听：m_winMsgHandlerMgr.RegisterMsg(TESTEVENTID,
//                          FunctorSubscriber(&MsgHandleSlot).Clone());
//
//      3.处理消息调用：  m_winMsgHandlerMgr.ProcessWinMsg(0, TESTEVENTID, 0, 0);
//
//      4.如果想取消函数的监听：m_winMsgHandlerMgr.UnRegisterMsg(TESTEVENTID,
//                                FunctorSubscriber(&MsgHandleSlot).Clone());
// Create Data:2021-10-27
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron		2021-10-28 	1.0
//-------------------------------------------------------
#pragma once
#include "../CMapT.h"
#include "../CVecT.h"
#include "../FunctionSubscriber.h"

typedef FunctorDelegateBase<LRESULT(HWND, UINT, WPARAM, LPARAM)>
    *MsgFunctorSlotPtr;
class CWinMsgHandlerItem : public CVecT<MsgFunctorSlotPtr> {
public:
  CWinMsgHandlerItem();
  virtual ~CWinMsgHandlerItem();

public:
  void PreVecObjRemove(const MsgFunctorSlotPtr &obj) override;
  bool EqualVecObj(const MsgFunctorSlotPtr &objsrc,
                   const MsgFunctorSlotPtr &objdest) const override;

  VOID SetRegMsgId(UINT uiMsgId);
  UINT GetRegMsgId();

private:
  UINT m_uiMsgId;
};

typedef CWinMsgHandlerItem *CWinMsgHandlerItemPtr;
class CWinMsgHandlerMgr : public CMapT<UINT, CWinMsgHandlerItemPtr> {
public:
  CWinMsgHandlerMgr();
  ~CWinMsgHandlerMgr();

  LRESULT ProcessWinMsg(HWND, UINT, WPARAM, LPARAM);

public:
  void PreMapKeyRemove(const CWinMsgHandlerItemPtr &obj) override;
  int RegisterMsg(UINT uiMsg, const MsgFunctorSlotPtr &FunctorSlot);
  int UnRegisterMsg(UINT uiMsg, const MsgFunctorSlotPtr &FunctorSlot);
};
