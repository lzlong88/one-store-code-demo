//-------------------------------------------------------
// Copyright (c) tencent
// All rights reserved.
//
// File Name: CRegMgr.h
// File Des: 类工厂注册管理类
// File Summary:
// Cur Version: 2.0
// Author:
// Create Data:2021-5-22
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron		2021-5-22 	1.0
//      zeron		2021-6-24 	2.0       支持多种类型的注册
//-------------------------------------------------------
#pragma once
#include "CMapT.h"
#include "CRegHelperT.h"
#include "CVecT.h"

class CRegTypeItem : public CVecT<IRegisterPtr> {
public:
  CRegTypeItem();
  virtual ~CRegTypeItem();

public:
  void PreVecObjRemove(const IRegisterPtr &obj) override;
  bool EqualVecObj(const IRegisterPtr &objsrc,
                   const IRegisterPtr &objdest) const override;
  std::wstring &GetDefRegName();

private:
  std::wstring m_strDefRegName; ///< 定义默认注册类型类名
};

typedef CRegTypeItem *CRegTypeItemPtr;
class CRegMgr : public CMapT<int, CRegTypeItemPtr> {
public:
  CRegMgr();
  ~CRegMgr();

public:
  void PreMapKeyRemove(const CRegTypeItemPtr &obj) override;
  int Register(IRegister &RegObj, bool bReplace = false,
               bool bSetDefName = true);
  int CreateRegObj(void **ppObj, LPCWSTR lpszRegName, int RegType);
  int UnRegister(LPCWSTR lpszRegName, int RegType);
  int SetDefRegObj(LPCWSTR lpszRegName, int RegType);
  int GetDefRegObj(std::wstring &lpszRegName, int RegType);
};
