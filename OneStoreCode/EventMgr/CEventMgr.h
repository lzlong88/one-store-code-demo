//-------------------------------------------------------
// Copyright (c) tencent
// All rights reserved.
//
// File Name: CEventMgr.h
// File Des: 事件监听函数绑定处理类 观察者模式
// File Summary:
// Cur Version: 1.0
// Author:zeron
// 用法：1.回调函数声明：#define EVENTID1 101
//                     #define EVENTID2 102
//                     int fun0();
//                     int fun1(int param);
//      2.定义对象：    CEventMgr<int()>  EventMgr0;
//                     CEventMgr<int(int)> EventMgr1;
//      2.注册事件处理回调监听：
//          m_EventMgr0.SubscribeEvent(EVENTID1, FunctorSubscriber(&fun0));
//          m_EventMgr1.SubscribeEvent(EVENTID2, FunctorSubscriber(&fun1));
//          m_EventMgr1.SubscribeEvent(EVENTID2,
//          FunctorSubscriber(&CDemoApp::memberfun1, this));
//      3.处理消息调用： m_EventMgr0.FireEvent(EVENTID1);
//                       m_EventMgr1.FireEvent(EVENTID2, 45);
//
//      4.如果想取消事件函数的监听：m_EventMgr0.UnSubscribeEvent(1,
//          FunctorSubscriber(&fun0));
//      5.拷贝CEventMgr对象:
//          CEventMgr<int()>* pEventMgrClone = NULL;
//          {
//             CEventMgr<int()> EventMgr0;
//             EventMgr0.SubscribeEvent(EVENTID1, FunctorSubscriber(&fun0));
//             pEventMgrClone =
//             dynamic_cast<CEventMgr<int()>*>(EventMgr0.Clone());
//          }
//          pEventMgrClone->FireEvent(EVENTID1);
//          delete pEventMgrClone;
//
// Create Data:2021-10-27
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      zeron		2021-10-30 	1.0
//-------------------------------------------------------
#pragma once
#include "../CMapT.h"
#include "../CVecT.h"
#include "../FunctionSubscriber.h"

// 是否打开了/GR (Enable Run-Time Type Information) 没打开dynamic_cast转换会失败
// project->Settings->c/c++（catagory->c++ language->Enable Runtime Type
// Information（RTTI）
#ifdef _CPPRTTI
#define EVMRTTI_CAST dynamic_cast
#else
#define EVMRTTI_CAST static_cast
#endif

/// 声明类
template <typename Signature> class CEventMgr;
template <typename Signature> class CEventItem;
typedef void DefaultVoid;

///////////////////////////////////////////////////////////////////
template <typename T> class CEventItemBase : public CVecT<T> {
public:
  CEventItemBase(const DWORD dwEventID) : m_dwEventID(dwEventID) {}
  virtual ~CEventItemBase() { RemoveAll(); }
  virtual CEventItemBase *Clone() const = 0;
  const CEventItemBase *GetThisPtr() const { return this; }

  void PreVecObjRemove(const T &obj) override {
    if (obj) {
      delete obj;
    }
  }

  bool EqualVecObj(const T &objsrc, const T &objdest) const override {
    return objdest->Equal(*objsrc);
  }

  bool _SubscribeEvent(const T subscriber) {
    if (-1 != FindObj(subscriber)) {
      return false;
    }
    return AddObj(subscriber->Clone());
  }

  bool _UnSubscribeEvent(const T subscriber) {
    bool bRet = FALSE;
    do {
      int iElement = FindObj(subscriber);
      if (-1 != iElement) {
        bRet = RemoveObj(iElement);
        break;
      }
    } while (FALSE);
    return bRet;
  }

  DWORD GetEventID() const { return m_dwEventID; } ///< 获取事件ID
protected:
  DWORD m_dwEventID;
};

/// 偏特化 0个参数的类实现
template <typename R>
class CEventItem<R()> : public CEventItemBase<FunctorDelegateBase<R()> *> {
public:
  CEventItem(const CEventItem &) = delete;
  CEventItem(const DWORD dwEventID) : CEventItemBase(dwEventID) {}
  ~CEventItem() {}

  CEventItemBase *Clone() const override {
    CEventItemBase *pEventItem = new CEventItem<R()>(GetEventID());
    for (int i = 0; i < GetCount(); i++) {
      pEventItem->AddObj(GetObj(i)->Clone());
    }
    return pEventItem;
  }

  bool SubscribeEvent(const FunctorDelegateBase<R()> &subscriber) {
    return _SubscribeEvent(
        const_cast<FunctorDelegateBase<R()> *>(subscriber.GetThisPtr()));
  }

  bool UnSubscribeEvent(const FunctorDelegateBase<R()> &subscriber) {
    return _UnSubscribeEvent(
        const_cast<FunctorDelegateBase<R()> *>(subscriber.GetThisPtr()));
  }

  CEventItem &operator=(const CEventItem &src) = delete;

  void operator()() {
    for (int i = 0; i < GetCount(); i++) {
      GetObj(i)->operator()();
    }
  }
};

/// 偏特化 1个参数的类实现
template <typename R, class Param1>
class CEventItem<R(Param1)>
    : public CEventItemBase<FunctorDelegateBase<R(Param1)> *> {
public:
  CEventItem(const CEventItem &) = delete;
  CEventItem(const DWORD dwEventID) : CEventItemBase(dwEventID) {}
  ~CEventItem() {}
  CEventItemBase *Clone() const override {
    CEventItemBase *pEventItem = new CEventItem<R(Param1)>(GetEventID());
    for (int i = 0; i < GetCount(); i++) {
      pEventItem->AddObj(GetObj(i)->Clone());
    }
    return pEventItem;
  }

  bool SubscribeEvent(const FunctorDelegateBase<R(Param1)> &subscriber) {
    return _SubscribeEvent(
        const_cast<FunctorDelegateBase<R(Param1)> *>(subscriber.GetThisPtr()));
  }

  bool UnSubscribeEvent(const FunctorDelegateBase<R(Param1)> &subscriber) {
    return _UnSubscribeEvent(
        const_cast<FunctorDelegateBase<R(Param1)> *>(subscriber.GetThisPtr()));
  }

  CEventItem &operator=(const CEventItem &src) = delete;

  void operator()(Param1 param) {
    for (int i = 0; i < GetCount(); i++) {
      GetObj(i)->operator()(param);
    }
  }
};

/// 偏特化 2个参数的类实现
template <typename R, class Param1, class Param2>
class CEventItem<R(Param1, Param2)>
    : public CEventItemBase<FunctorDelegateBase<R(Param1, Param2)> *> {
public:
  CEventItem(const CEventItem &) = delete;
  CEventItem(const DWORD dwEventID) : CEventItemBase(dwEventID) {}
  ~CEventItem() {}

  CEventItemBase *Clone() const override {
    CEventItemBase *pEventItem =
        new CEventItem<R(Param1, Param2)>(GetEventID());
    for (int i = 0; i < GetCount(); i++) {
      pEventItem->AddObj(GetObj(i)->Clone());
    }
    return pEventItem;
  }

  bool
  SubscribeEvent(const FunctorDelegateBase<R(Param1, Param2)> &subscriber) {
    return _SubscribeEvent(const_cast<FunctorDelegateBase<R(Param1, Param2)> *>(
        subscriber.GetThisPtr()));
  }

  bool
  UnSubscribeEvent(const FunctorDelegateBase<R(Param1, Param2)> &subscriber) {
    return _UnSubscribeEvent(
        const_cast<FunctorDelegateBase<R(Param1, Param2)> *>(
            subscriber.GetThisPtr()));
  }

  CEventItem &operator=(const CEventItem &src) = delete;

  void operator()(Param1 param1, Param2 param2) {
    for (int i = 0; i < GetCount(); i++) {
      GetObj(i)->operator()(param1, param2);
    }
  }
};

/// 偏特化 3个参数的类实现
template <typename R, class Param1, class Param2, class Param3>
class CEventItem<R(Param1, Param2, Param3)>
    : public CEventItemBase<FunctorDelegateBase<R(Param1, Param2, Param3)> *> {
public:
  CEventItem(const CEventItem &) = delete;
  CEventItem(const DWORD dwEventID) : CEventItemBase(dwEventID) {}
  ~CEventItem() {}

  CEventItemBase *Clone() const override {
    CEventItemBase *pEventItem =
        new CEventItem<R(Param1, Param2, Param3)>(GetEventID());
    for (int i = 0; i < GetCount(); i++) {
      pEventItem->AddObj(GetObj(i)->Clone());
    }
    return pEventItem;
  }

  bool SubscribeEvent(
      const FunctorDelegateBase<R(Param1, Param2, Param3)> &subscriber) {
    return _SubscribeEvent(
        const_cast<FunctorDelegateBase<R(Param1, Param2, Param3)> *>(
            subscriber.GetThisPtr()));
  }

  bool UnSubscribeEvent(
      const FunctorDelegateBase<R(Param1, Param2, Param3)> &subscriber) {
    return _UnSubscribeEvent(
        const_cast<FunctorDelegateBase<R(Param1, Param2, Param3)> *>(
            subscriber.GetThisPtr()));
  }

  CEventItem operator=(const CEventItem &src) const = delete;

  void operator()(Param1 param1, Param2 param2, Param3 param3) {
    for (int i = 0; i < GetCount(); i++) {
      GetObj(i)->operator()(param1, param2, param3);
    }
  }
};

/// 偏特化 4个参数的类实现
template <typename R, class Param1, class Param2, class Param3, class Param4>
class CEventItem<R(Param1, Param2, Param3, Param4)>
    : public CEventItemBase<
          FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> *> {
public:
  CEventItem(const CEventItem &) = delete;
  CEventItem(const DWORD dwEventID) : CEventItemBase(dwEventID) {}
  ~CEventItem() {}

  CEventItemBase *Clone() const override {
    CEventItemBase *pEventItem =
        new CEventItem<R(Param1, Param2, Param3, Param4)>(GetEventID());
    for (int i = 0; i < GetCount(); i++) {
      pEventItem->AddObj(GetObj(i)->Clone());
    }
    return pEventItem;
  }

  bool SubscribeEvent(const FunctorDelegateBase<R(Param1, Param2, Param3,
                                                  Param4)> &subscriber) {
    return _SubscribeEvent(
        const_cast<FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> *>(
            subscriber.GetThisPtr()));
  }

  bool UnSubscribeEvent(const FunctorDelegateBase<R(Param1, Param2, Param3,
                                                    Param4)> &subscriber) {
    return _UnSubscribeEvent(
        const_cast<FunctorDelegateBase<R(Param1, Param2, Param3, Param4)> *>(
            subscriber.GetThisPtr()));
  }

  CEventItem &operator=(const CEventItem &src) = delete;

  void operator()(Param1 param1, Param2 param2, Param3 param3, Param4 param4) {
    for (int i = 0; i < GetCount(); i++) {
      GetObj(i)->operator()(param1, param2, param3, param4);
    }
  }
};

/// 偏特化 5个参数的类实现
template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5>
class CEventItem<R(Param1, Param2, Param3, Param4, Param5)>
    : public CEventItemBase<
          FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)> *> {
public:
  CEventItem(const CEventItem &) = delete;
  CEventItem(const DWORD dwEventID) : CEventItemBase(dwEventID) {}
  ~CEventItem() {}

  CEventItemBase *Clone() const override {
    CEventItemBase *pEventItem =
        new CEventItem<R(Param1, Param2, Param3, Param4, Param5)>(GetEventID());
    for (int i = 0; i < GetCount(); i++) {
      pEventItem->AddObj(GetObj(i)->Clone());
    }
    return pEventItem;
  }

  bool SubscribeEvent(
      const FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)>
          &subscriber) {
    return _SubscribeEvent(
        const_cast<
            FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)> *>(
            subscriber.GetThisPtr()));
  }

  bool UnSubscribeEvent(
      const FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)>
          &subscriber) {
    return _UnSubscribeEvent(
        const_cast<
            FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)> *>(
            subscriber.GetThisPtr()));
  }

  CEventItem &operator=(const CEventItem &src) = delete;

  void operator()(Param1 param1, Param2 param2, Param3 param3, Param4 param4,
                  Param5 param5) {
    for (int i = 0; i < GetCount(); i++) {
      GetObj(i)->operator()(param1, param2, param3, param4, param5);
    }
  }
};

/// 偏特化 6个参数的类实现
template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5, class Param6>
class CEventItem<R(Param1, Param2, Param3, Param4, Param5, Param6)>
    : public CEventItemBase<FunctorDelegateBase<R(Param1, Param2, Param3,
                                                  Param4, Param5, Param6)> *> {
public:
  CEventItem(const CEventItem &) = delete;
  CEventItem(const DWORD dwEventID) : CEventItemBase(dwEventID) {}
  ~CEventItem() {}

  CEventItemBase *Clone() const override {
    CEventItemBase *pEventItem =
        new CEventItem<R(Param1, Param2, Param3, Param4, Param5, Param6)>(
            GetEventID());
    for (int i = 0; i < GetCount(); i++) {
      pEventItem->AddObj(GetObj(i)->Clone());
    }
    return pEventItem;
  }

  bool
  SubscribeEvent(const FunctorDelegateBase<R(Param1, Param2, Param3, Param4,
                                             Param5, Param6)> &subscriber) {
    return _SubscribeEvent(
        const_cast<FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5,
                                         Param6)> *>(subscriber.GetThisPtr()));
  }

  bool
  UnSubscribeEvent(const FunctorDelegateBase<R(Param1, Param2, Param3, Param4,
                                               Param5, Param6)> &subscriber) {
    return _UnSubscribeEvent(
        const_cast<FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5,
                                         Param6)> *>(subscriber.GetThisPtr()));
  }

  CEventItem &operator=(const CEventItem &src) = delete;

  void operator()(Param1 param1, Param2 param2, Param3 param3, Param4 param4,
                  Param5 param5, Param6 param6) {
    for (int i = 0; i < GetCount(); i++) {
      GetObj(i)->operator()(param1, param2, param3, param4, param5, param6);
    }
  }
};

/////////////////////////////////////////////////////////////////////
/// CEventMgr基类实现
template <typename T> class CEventMgrBase : public CMapT<DWORD, T *> {
  typedef T *TPtr;

public:
  CEventMgrBase() {}
  virtual ~CEventMgrBase() { RemoveAll(); }
  virtual CEventMgrBase *Clone() const = 0;
  const CEventMgrBase *GetThisPtr() const { return this; }

  void Clone2EvtMgr(CEventMgrBase *pEventMgr) const {
    MutexAutoLockObj(m_Map);
    MapType::const_iterator it = m_Map.begin();
    for (; it != m_Map.end(); it++) {
      T *pItem = EVMRTTI_CAST<T *>(it->second->Clone());
      pEventMgr->AddKey(it->first, pItem);
    }
  }

  void AddEvent(const DWORD dwEventID) {
    if (!IsEventPresent(dwEventID)) {
      AddKey(dwEventID, new T(dwEventID));
    }
  }

  bool IsEventPresent(const DWORD dwEventID) {
    return GetEventObject(dwEventID) != NULL;
  }

  T *GetEventObject(const DWORD dwEventID) {
    T *pEventItem = NULL;
    GetObjByKey(dwEventID, pEventItem);
    return pEventItem;
  }

  void PreMapKeyRemove(const TPtr &obj) override {
    if (obj) {
      delete obj;
    }
  }
};

////////////////////////////////////////////////////////////////
/// 偏特化 0个参数的类实现
template <typename R>
class CEventMgr<R()> : public CEventMgrBase<CEventItem<R()>> {
  // 这种偏特化的类实现不能在类外实现函数
public:
  CEventMgr(const CEventMgr &) = delete;
  CEventMgr(){};
  virtual ~CEventMgr() {}

  CEventMgrBase *Clone() const override {
    CEventMgrBase *pEventMgr = new CEventMgr<R()>();
    Clone2EvtMgr(pEventMgr);
    return pEventMgr;
  }

  CEventMgr &operator=(const CEventMgr<R()> &) = delete;

  bool SubscribeEvent(const DWORD dwEventID,
                      const FunctorDelegateBase<R()> &subscriber) {
    if (!IsEventPresent(dwEventID)) {
      AddEvent(dwEventID);
    }

    CEventItem<R()> *pEventItem = GetEventObject(dwEventID);
    return pEventItem->SubscribeEvent(subscriber);
  }

  bool UnSubscribeEvent(const DWORD dwEventID,
                        const FunctorDelegateBase<R()> &subscriber) {
    bool bRet = FALSE;
    do {
      CEventItem<R()> *pEventItem = GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }

      bRet = pEventItem->UnSubscribeEvent(subscriber);
    } while (FALSE);
    return bRet;
  }

  void FireEvent(const DWORD dwEventID) {
    do {
      CEventItem<R()> *pEventItem = GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }
      pEventItem->operator()();
    } while (FALSE);
  }
};

/// 偏特化 1个参数的类实现
template <typename R, class Param1>
class CEventMgr<R(Param1)> : public CEventMgrBase<CEventItem<R(Param1)>> {
  // 这种偏特化的类实现不能在类外实现函数
public:
  CEventMgr(const CEventMgr &) = delete;
  CEventMgr(){};
  virtual ~CEventMgr() {}

  CEventMgrBase *Clone() const override {
    CEventMgrBase *pEventMgr = new CEventMgr<R(Param1)>();
    Clone2EvtMgr(pEventMgr);
    return pEventMgr;
  }

  CEventMgr &operator=(const CEventMgr<R(Param1)> &) = delete;

  bool SubscribeEvent(const DWORD dwEventID,
                      const FunctorDelegateBase<R(Param1)> &subscriber) {
    if (!IsEventPresent(dwEventID)) {
      AddEvent(dwEventID);
    }

    CEventItem<R(Param1)> *pEventItem = GetEventObject(dwEventID);
    return pEventItem->SubscribeEvent(subscriber);
  }

  bool UnSubscribeEvent(const DWORD dwEventID,
                        const FunctorDelegateBase<R(Param1)> &subscriber) {
    bool bRet = FALSE;
    do {
      CEventItem<R(Param1)> *pEventItem = GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }

      bRet = pEventItem->UnSubscribeEvent(subscriber);
    } while (FALSE);
    return bRet;
  }

  void FireEvent(const DWORD dwEventID, Param1 param1) {
    do {
      CEventItem<R(Param1)> *pEventItem = GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }
      pEventItem->operator()(param1);
    } while (FALSE);
  }
};

/// 偏特化 2个参数的类实现
template <typename R, class Param1, class Param2>
class CEventMgr<R(Param1, Param2)>
    : public CEventMgrBase<CEventItem<R(Param1, Param2)>> {
  // 这种偏特化的类实现不能在类外实现函数
public:
  CEventMgr(const CEventMgr &) = delete;
  CEventMgr(){};
  virtual ~CEventMgr() {}

  CEventMgrBase *Clone() const override {
    CEventMgrBase *pEventMgr = new CEventMgr<R(Param1, Param2)>();
    Clone2EvtMgr(pEventMgr);
    return pEventMgr;
  }

  CEventMgr &operator=(const CEventMgr<R(Param1, Param2)> &) = delete;

  bool
  SubscribeEvent(const DWORD dwEventID,
                 const FunctorDelegateBase<R(Param1, Param2)> &subscriber) {
    if (!IsEventPresent(dwEventID)) {
      AddEvent(dwEventID);
    }

    CEventItem<R(Param1, Param2)> *pEventItem = GetEventObject(dwEventID);
    return pEventItem->SubscribeEvent(subscriber);
  }

  bool
  UnSubscribeEvent(const DWORD dwEventID,
                   const FunctorDelegateBase<R(Param1, Param2)> &subscriber) {
    bool bRet = FALSE;
    do {
      CEventItem<R(Param1, Param2)> *pEventItem = GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }

      bRet = pEventItem->UnSubscribeEvent(subscriber);
    } while (FALSE);
    return bRet;
  }

  void FireEvent(const DWORD dwEventID, Param1 param1, Param2 param2) {
    do {
      CEventItem<R(Param1, Param2)> *pEventItem = GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }
      pEventItem->operator()(param1, param2);
    } while (FALSE);
  }
};

/// 偏特化 3个参数的类实现
template <typename R, class Param1, class Param2, class Param3>
class CEventMgr<R(Param1, Param2, Param3)>
    : public CEventMgrBase<CEventItem<R(Param1, Param2, Param3)>> {
  // 这种偏特化的类实现不能在类外实现函数
public:
  CEventMgr(const CEventMgr &) = delete;
  CEventMgr(){};
  virtual ~CEventMgr() {}

  CEventMgrBase *Clone() const override {
    CEventMgrBase *pEventMgr = new CEventMgr<R(Param1, Param2, Param3)>();
    Clone2EvtMgr(pEventMgr);
    return pEventMgr;
  }

  bool SubscribeEvent(
      const DWORD dwEventID,
      const FunctorDelegateBase<R(Param1, Param2, Param3)> &subscriber) {
    if (!IsEventPresent(dwEventID)) {
      AddEvent(dwEventID);
    }

    CEventItem<R(Param1, Param2, Param3)> *pEventItem =
        GetEventObject(dwEventID);
    return pEventItem->SubscribeEvent(subscriber);
  }

  bool UnSubscribeEvent(
      const DWORD dwEventID,
      const FunctorDelegateBase<R(Param1, Param2, Param3)> &subscriber) {
    bool bRet = FALSE;
    do {
      CEventItem<R(Param1, Param2, Param3)> *pEventItem =
          GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }

      bRet = pEventItem->UnSubscribeEvent(subscriber);
    } while (FALSE);
    return bRet;
  }

  CEventMgr &operator=(const CEventMgr<R(Param1, Param2, Param3)> &) = delete;

  void FireEvent(const DWORD dwEventID, Param1 param1, Param2 param2,
                 const Param3 param3) {
    do {
      CEventItem<R(Param1, Param2, Param3)> *pEventItem =
          GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }
      pEventItem->operator()(param1, param2, param3);
    } while (FALSE);
  }
};

/// 偏特化 4个参数的类实现
template <typename R, class Param1, class Param2, class Param3, class Param4>
class CEventMgr<R(Param1, Param2, Param3, Param4)>
    : public CEventMgrBase<CEventItem<R(Param1, Param2, Param3, Param4)>> {
  // 这种偏特化的类实现不能在类外实现函数
public:
  CEventMgr(const CEventMgr &) = delete;
  CEventMgr(){};
  virtual ~CEventMgr() {}

  CEventMgrBase *Clone() const override {
    CEventMgrBase *pEventMgr =
        new CEventMgr<R(Param1, Param2, Param3, Param4)>();
    Clone2EvtMgr(pEventMgr);
    return pEventMgr;
  }

  CEventMgr &
  operator=(const CEventMgr<R(Param1, Param2, Param3, Param4)> &) = delete;

  bool SubscribeEvent(const DWORD dwEventID,
                      const FunctorDelegateBase<R(Param1, Param2, Param3,
                                                  Param4)> &subscriber) {
    if (!IsEventPresent(dwEventID)) {
      AddEvent(dwEventID);
    }

    CEventItem<R(Param1, Param2, Param3, Param4)> *pEventItem =
        GetEventObject(dwEventID);
    return pEventItem->SubscribeEvent(subscriber);
  }

  bool UnSubscribeEvent(const DWORD dwEventID,
                        const FunctorDelegateBase<R(Param1, Param2, Param3,
                                                    Param4)> &subscriber) {
    bool bRet = FALSE;
    do {
      CEventItem<R(Param1, Param2, Param3, Param4)> *pEventItem =
          GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }

      bRet = pEventItem->UnSubscribeEvent(subscriber);
    } while (FALSE);
    return bRet;
  }

  void FireEvent(const DWORD dwEventID, Param1 param1, Param2 param2,
                 Param3 param3, Param4 param4) {
    do {
      CEventItem<R(Param1, Param2, Param3, Param4)> *pEventItem =
          GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }
      pEventItem->operator()(param1, param2, param3, param4);
    } while (FALSE);
  }
};

/// 偏特化 5个参数的类实现
template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5>
class CEventMgr<R(Param1, Param2, Param3, Param4, Param5)>
    : public CEventMgrBase<
          CEventItem<R(Param1, Param2, Param3, Param4, Param5)>> {
  // 这种偏特化的类实现不能在类外实现函数
public:
  CEventMgr(const CEventMgr &) = delete;
  CEventMgr(){};
  virtual ~CEventMgr() {}

  CEventMgrBase *Clone() const override {
    CEventMgrBase *pEventMgr =
        new CEventMgr<R(Param1, Param2, Param3, Param4, Param5)>();
    Clone2EvtMgr(pEventMgr);
    return pEventMgr;
  }

  CEventMgr &operator=(
      const CEventMgr<R(Param1, Param2, Param3, Param4, Param5)> &) = delete;

  bool SubscribeEvent(
      const DWORD dwEventID,
      const FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)>
          &subscriber) {
    if (!IsEventPresent(dwEventID)) {
      AddEvent(dwEventID);
    }

    CEventItem<R(Param1, Param2, Param3, Param4, Param5)> *pEventItem =
        GetEventObject(dwEventID);
    return pEventItem->SubscribeEvent(subscriber);
  }

  bool UnSubscribeEvent(
      const DWORD dwEventID,
      const FunctorDelegateBase<R(Param1, Param2, Param3, Param4, Param5)>
          &subscriber) {
    bool bRet = FALSE;
    do {
      CEventItem<R(Param1, Param2, Param3, Param4, Param5)> *pEventItem =
          GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }

      bRet = pEventItem->UnSubscribeEvent(subscriber);
    } while (FALSE);
    return bRet;
  }

  void FireEvent(const DWORD dwEventID, Param1 param1, Param2 param2,
                 Param3 param3, Param4 param4, Param5 param5) {
    do {
      CEventItem<R(Param1, Param2, Param3, Param4, Param5)> *pEventItem =
          GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }
      pEventItem->operator()(param1, param2, param3, param4, param5);
    } while (FALSE);
  }
};

/// 偏特化 6个参数的类实现
template <typename R, class Param1, class Param2, class Param3, class Param4,
          class Param5, class Param6>
class CEventMgr<R(Param1, Param2, Param3, Param4, Param5, Param6)>
    : public CEventMgrBase<
          CEventItem<R(Param1, Param2, Param3, Param4, Param5, Param6)>> {
  // 这种偏特化的类实现不能在类外实现函数
public:
  CEventMgr(const CEventMgr &) = delete;
  CEventMgr(){};
  virtual ~CEventMgr() {}

  CEventMgrBase *Clone() const override {
    CEventMgrBase *pEventMgr =
        new CEventMgr<R(Param1, Param2, Param3, Param4, Param5, Param6)>();
    Clone2EvtMgr(pEventMgr);
    return pEventMgr;
  }

  CEventMgr &operator=(
      const CEventMgr<R(Param1, Param2, Param3, Param4, Param5, Param6)> &) =
      delete;

  bool
  SubscribeEvent(const DWORD dwEventID,
                 const FunctorDelegateBase<R(Param1, Param2, Param3, Param4,
                                             Param5, Param6)> &subscriber) {
    if (!IsEventPresent(dwEventID)) {
      AddEvent(dwEventID);
    }

    CEventItem<R(Param1, Param2, Param3, Param4, Param5, Param6)> *pEventItem =
        GetEventObject(dwEventID);
    return pEventItem->SubscribeEvent(subscriber);
  }

  bool
  UnSubscribeEvent(const DWORD dwEventID,
                   const FunctorDelegateBase<R(Param1, Param2, Param3, Param4,
                                               Param5, Param6)> &subscriber) {
    bool bRet = FALSE;
    do {
      CEventItem<R(Param1, Param2, Param3, Param4, Param5, Param6)>
          *pEventItem = GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }

      bRet = pEventItem->UnSubscribeEvent(subscriber);
    } while (FALSE);
    return bRet;
  }

  void FireEvent(const DWORD dwEventID, Param1 param1, Param2 param2,
                 Param3 param3, Param4 param4, Param5 param5, Param6 param6) {
    do {
      CEventItem<R(Param1, Param2, Param3, Param4, Param5, Param6)>
          *pEventItem = GetEventObject(dwEventID);
      if (!pEventItem) {
        break;
      }
      pEventItem->operator()(param1, param2, param3, param4, param5, param6);
    } while (FALSE);
  }
};
