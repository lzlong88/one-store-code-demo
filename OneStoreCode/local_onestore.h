#pragma once
#include <string>
#include "CRegMgr.h"
#include "IPC/ProcMsgQueueMgr.h"


class AEngineImpl;

class OST_Request_JSDataBase;

class service_info_reply;

class update_seedapp_ReqData;
class update_seedapp_reply;

class product_detail_ReqData;
class product_detail_reply;

class show_error_ReqData;
class show_error_reply;

class auth_token_reply;
class client_id_reply;
class show_login_view_reply;
class bye_gameloop_reply;

class COnestoreCodeDlgDemoDlg;

class local_onestore : public IMsgQueueHandler
{
public:
  local_onestore(COnestoreCodeDlgDemoDlg* pdlg);
  ~local_onestore();

  void RegistProtocol();
  void UnRegistProtocol();

  BOOL get_service_info(const OST_Request_JSDataBase& req, service_info_reply& reply);
  BOOL get_auth_token(const OST_Request_JSDataBase& req, auth_token_reply& reply);
  BOOL get_client_id(const OST_Request_JSDataBase& req, client_id_reply& reply);
  BOOL update_seedapp(const update_seedapp_ReqData& req, update_seedapp_reply& reply);
  BOOL show_product_detail(const product_detail_ReqData& req, product_detail_reply& reply);
  BOOL show_error_dialog(const show_error_ReqData& req, show_error_reply& reply);
  BOOL show_login_view(const OST_Request_JSDataBase& req, show_login_view_reply& reply);
  BOOL bye_gameloop(const OST_Request_JSDataBase& req,  bye_gameloop_reply& reply);

  void send_notify_auth_cmd(bool bLogin = true);

  bool write(LPCSTR, int);
  void ProcessOnestoreData(LPCSTR szBuff, int iLen);
protected:
  void on_connected(LPCSTR, int);
  

  void HandleMsgQueue(ByteBuffer& bytebuffer, DWORD dwMsgType, DWORD clientId) override;
private:
  bool m_bExitCurCon;

  CRegMgr       m_RegMgr;

  CProcMsgQueueMgr  m_ProcMsgQueueMgr;

  COnestoreCodeDlgDemoDlg* m_pDlg;
};
