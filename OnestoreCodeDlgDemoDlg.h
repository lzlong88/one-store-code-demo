
// OnestoreCodeDlgDemoDlg.h : header file
//

#pragma once

#include "OneStoreCode/local_onestore.h"

// COnestoreCodeDlgDemoDlg dialog
class COnestoreCodeDlgDemoDlg : public CDialogEx
{
// Construction
public:
	COnestoreCodeDlgDemoDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ONESTORECODEDLGDEMO_DIALOG };
#endif
  CString getdlgproductdetail();

  void writeSendBuf(const std::string& str);
  void writeSendReceive(const std::string& str);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

  afx_msg void OnBnClickedButton1();
  afx_msg void OnBnClickedButton2();
  afx_msg void OnBnClickedButton3();
  afx_msg void OnBnClickedButton4();
  afx_msg void OnBnClickedButton5();
  afx_msg void OnBnClickedButton6();
  afx_msg void OnBnClickedButton7();
  afx_msg void OnBnClickedButton8();
  afx_msg void OnBnClickedButton9();

  local_onestore m_onestore;

// Implementation
protected:
	HICON m_hIcon;

  int memberfun0();
  int memberfun1(int i);
  int memberfun2(int i, double db);
  int memberfun3(int i, double db, char f);
  int memberfun4(int i, double db, char f, int ij);
  int memberfun5(int i, double db, char f, int ij, double di);
  int memberfun6(int i, double db, char f, int ij, double di, int ik);

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
};
